###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from pathlib import Path

from LHCbTesting import LHCbExeTest


class Test(LHCbExeTest):
    command = ["gaudirun.py", "../options/error_monitoring.py"]
    reference = "../refs/error_monitoring.yaml"
    timeout = 600

    def test_validate_json(self, cwd: Path):
        self.validate_json_with_reference(
            cwd / "error_monitoring.json", "../refs/error_monitoring.json"
        )
