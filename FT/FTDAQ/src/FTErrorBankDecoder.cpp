/*****************************************************************************
 * (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
 *****************************************************************************/

#include "Detector/FT/FTConstants.h"
#include "Detector/FT/FTSourceID.h"
#include "Event/FTErrorCounter.h"
#include "Event/RawBank.h"
#include "FTDAQ/FTDAQHelper.h"
#include "FTDAQ/FTReadoutMap.h"
#include "Gaudi/Accumulators/Histogram.h"
#include "LHCbAlgs/Transformer.h"
#include <Gaudi/Property.h>
#include <cstdint>

using FTErrors        = LHCb::FTError::FTErrors;
namespace FTConstants = LHCb::Detector::FT;
namespace FTRawBank   = LHCb::Detector::FT::RawBank;

namespace {
  using namespace std::string_literals;
  // Histogram nine errors from RawBank.h
  const std::vector<std::string> ErrorBankLabels = { "FTError",
                                                     "DaqErrorFragmentThrottled",
                                                     "DaqErrorBXIDCorrupted",
                                                     "DaqErrorSyncBXIDCorrupted",
                                                     "DaqErrorFragmentMissing",
                                                     "DaqErrorFragmentTruncated",
                                                     "DaqErrorIdleBXIDCorrupted",
                                                     "DaqErrorFragmentMalformed",
                                                     "DaqErrorEVIDJumped",
                                                     "DaqErrorAlignFifoFull",
                                                     "DaqErrorFEfragSizeWrong" };

  const std::vector<std::string> BankLocationLabels_Y = { "D1P0", "D1P1", "D2P0", "D2P1", "D3P0", "D3P1" };

  const std::vector<std::string> BankLocationLabels_X() {
    std::vector<std::string> xLabels;
    for ( const auto stationID : LHCb::Detector::FTChannelID::allStationIDs ) {
      for ( const auto layerID : LHCb::Detector::FTChannelID::allLayerIDs ) {
        for ( const auto quarterID : LHCb::Detector::FTChannelID::allQuarterIDs ) {
          xLabels.push_back(
              fmt::format( "{}{}{}", to_string( stationID ), to_string( layerID ), to_string( quarterID ) ) );
        }
      }
    }
    return xLabels;
  }
} // namespace

class FTErrorBankDecoder : public LHCb::Algorithm::Transformer<FTErrors( const LHCb::RawBank::View& )> {
public:
  FTErrorBankDecoder( const std::string& name, ISvcLocator* pSvcLocator );
  FTErrors operator()( const LHCb::RawBank::View& banks ) const override;

private:
  mutable Gaudi::Accumulators::Histogram<1, Gaudi::Accumulators::atomicity::full, float> m_errorBankTypeHist = {
      this,
      "ErrorsPerErrorBankType",
      "Errors",
      { static_cast<unsigned int>( ErrorBankLabels.size() ), -0.5f, static_cast<float>( ErrorBankLabels.size() ) - 0.5f,
        "Error Bank", ErrorBankLabels } };

  mutable Gaudi::Accumulators::Histogram<2> m_errorsBankLocationHist2D = {
      this,
      "errorsPerBankLocation",
      "Errors per Bank Location",
      { { FTConstants::nQuartersTotal, -0.5, FTConstants::nQuartersTotal - 0.5, "Quarter", BankLocationLabels_X() },
        { 6, -0.5, 6 - 0.5, "Port", BankLocationLabels_Y } } }; //---LoH: This 6 is nPorts * nDataLinks

  mutable Gaudi::Accumulators::StatCounter<> m_decodererrors_D1_0{ this, "# D1_0 decoder errors" };
  mutable Gaudi::Accumulators::StatCounter<> m_decodererrors_D2_0{ this, "# D2_0 decoder errors" };
  mutable Gaudi::Accumulators::StatCounter<> m_decodererrors_D2_1{ this, "# D2_1 decoder errors" };
  mutable Gaudi::Accumulators::StatCounter<> m_decodererrors_D3_0{ this, "# D3_0 decoder errors" };
  mutable Gaudi::Accumulators::StatCounter<> m_decodererrors_D3_1{ this, "# D3_1 decoder errors" };
  mutable Gaudi::Accumulators::StatCounter<> m_decodererrors_Unknown{ this, "# Unknown decoder errors" };
};

DECLARE_COMPONENT( FTErrorBankDecoder )

FTErrorBankDecoder::FTErrorBankDecoder( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer( name, pSvcLocator, { KeyValue{ "ErrorRawBanks", {} } },
                   KeyValue{ "OutputLocation", LHCb::FTErrorLocation::Default } ) {}

FTErrors FTErrorBankDecoder::operator()( const LHCb::RawBank::View& banks ) const {
  if ( banks.empty() ) return {};

  FTErrors errorCounts;

  for ( const auto* bank : banks ) {
    if ( !bank ) continue;
    auto sourceID = LHCb::Detector::FTSourceID( bank->sourceID() );

    // only care about SciFi error banks (added in lhcb/Detector!429), skip the rest
    if ( !sourceID.isIndeedFT() ) continue;

    if ( LHCb::RawBank::MagicPattern != bank->magic() )
      error() << "Bad MagicPattern for sourceID " << bank->sourceID() << endmsg;

    auto bankType      = bank->type();
    auto bankName      = LHCb::RawBank::typeName( bankType );
    auto bankCounter   = static_cast<unsigned int>( bankType );
    auto board         = static_cast<unsigned int>( sourceID.dataLink() ); //[0-2] convention; excel/ECS is [1-3]
    auto port          = static_cast<unsigned int>( sourceID.port() );     //[0-1]
    auto uniqueQuarter = sourceID.globalQuarterIdx();
    auto uniquePort    = board * 2 + port;

    if ( board == 0 && port == 0 )
      ++m_decodererrors_D1_0;
    else if ( board == 1 && port == 0 )
      ++m_decodererrors_D2_0;
    else if ( board == 2 && port == 0 )
      ++m_decodererrors_D3_0;
    else if ( board == 1 && port == 1 )
      ++m_decodererrors_D2_1;
    else if ( board == 2 && port == 1 )
      ++m_decodererrors_D3_1;
    else {
      warning() << "Unknown board/post: board=" << board << ", port=" << port << endmsg;
      ++m_decodererrors_Unknown;
    }

    if ( msgLevel( MSG::DEBUG ) )
      debug() << "  Bank: " << bankName << "  sourceID: " << sourceID << " " << sourceID.sourceID() << " "
              << uniqueQuarter << " " << uniquePort << endmsg;
    // only relevant FT or DAQ banks passed
    if ( bankCounter >= static_cast<unsigned int>( LHCb::RawBank::FTError ) &&
         bankCounter <= static_cast<unsigned int>( LHCb::RawBank::DaqErrorEVIDJumped ) ) {
      ++m_errorBankTypeHist[bankCounter - static_cast<unsigned int>( LHCb::RawBank::FTError )];
      ++m_errorsBankLocationHist2D[{ uniqueQuarter, uniquePort }];
    } else {
      error() << "Bank: " << bankName << "is not an FT or DAQ error bank" << endmsg;
    }
  }

  return errorCounts;
}
