###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from IOExample.Configuration import runIOTest

from PyConf.application import ApplicationOptions

options = ApplicationOptions(_enabled=False)
options.evt_max = 5
options.data_type = "2012"
options.simulation = False
options.dddb_tag = "dddb-20130929-1"
options.conddb_tag = "cond-20141107"
options.input_files = ["PFN:Reco14-Stripping21.mdst"]
options.input_type = "ROOT"
options.output_type = "ROOT"
options.output_file = "PFN:Reco14-Stripping21-copy.mdst"
options.input_stream = "Trigger"

runIOTest(options, LoadAll=True, DataContent="MDST")
