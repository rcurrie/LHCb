/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file RichHistoBase.h
 *
 *  Header file for RICH base class : Rich::HistoBase
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   2009-07-27
 */
//-----------------------------------------------------------------------------

#pragma once

// Local
#include "RichFutureKernel/RichCommonBase.h"

// Rich
#include "Detector/Rich/Types.h"
#include "Kernel/RichDetectorType.h"
#include "Kernel/RichParticleIDType.h"
#include "Kernel/RichRadiatorType.h"
#include "Kernel/RichSide.h"
#include "RichFutureUtils/RichQuadrants.h"
#include "RichUtils/RichException.h"
#include "RichUtils/RichHashMap.h"
#include "RichUtils/RichHistoID.h"
#include "RichUtils/RichMap.h"

// Gaudi
#include "Gaudi/Accumulators/StaticHistogram.h"
#include "Gaudi/Property.h"

// STL
#include <array>
#include <cassert>
#include <cstdint>
#include <mutex>
#include <optional>
#include <string>
#include <tuple>
#include <type_traits>
#include <utility>
#include <vector>

namespace Rich::Future {

  namespace Hist {

    namespace details {
      /// Check if a given type is one of a given list.
      template <typename T, typename... Types>
      using is_one_of = std::disjunction<std::is_base_of<std::remove_cvref_t<T>, std::remove_cvref_t<Types>>...>;
      template <typename T, typename... Types>
      constexpr bool is_one_of_v = is_one_of<T, Types...>::value;
      ///  Check if it is 'OK' to cast a parameter From -> To
      template <typename From, typename To>
      constexpr bool is_safe_cast() {
        // eventually would like to add proper 'narrowing' type traits here but for now do a few checks
        constexpr bool is_fp_to_int    = std::is_floating_point_v<From> && std::is_integral_v<To>;
        constexpr bool is_big_to_small = ( sizeof( From ) > sizeof( To ) );
        constexpr bool is_signed_int_mismatch =
            std::is_integral_v<From> && std::is_integral_v<To> && ( std::is_signed_v<From> != std::is_signed_v<To> );
        constexpr bool from_is_int_or_enum = std::is_arithmetic_v<From> || std::is_enum_v<From>;
        return ( !is_fp_to_int && !is_big_to_small && !is_signed_int_mismatch && from_is_int_or_enum &&
                 std::is_arithmetic_v<To> && std::is_convertible_v<From, To> );
      }
      /// Check histogram dimensions
      template <typename T, unsigned int ND, typename I = typename T::value_type::NumberDimensions::value_type>
      constexpr bool is_dim_v = std::is_same_v<typename T::value_type::NumberDimensions, std::integral_constant<I, ND>>;
      /// Check histogram types for various arthemtic emplate types
      template <typename T, template <typename, Gaudi::Accumulators::atomicity> typename H,
                Gaudi::Accumulators::atomicity ATOMICITY>
      constexpr bool is_hist_type_v = is_one_of_v<T, H<float, ATOMICITY>, H<double, ATOMICITY>,            //
                                                  H<std::int64_t, ATOMICITY>, H<std::uint64_t, ATOMICITY>, //
                                                  H<std::int32_t, ATOMICITY>, H<std::uint32_t, ATOMICITY>, //
                                                  H<std::int16_t, ATOMICITY>, H<std::uint16_t, ATOMICITY>, //
                                                  H<std::int8_t, ATOMICITY>, H<std::uint8_t, ATOMICITY>>;
      template <typename T, template <typename, Gaudi::Accumulators::atomicity> typename... Hs>
      constexpr bool is_one_of_hist_types_v = ( (... || is_hist_type_v<T, Hs, Gaudi::Accumulators::atomicity::full>) ||
                                                (... || is_hist_type_v<T, Hs, Gaudi::Accumulators::atomicity::none>));
      /// Base class wrapper for all histograms, making them optional.
      template <typename H>
      struct BaseH : std::optional<H> {
        using std::optional<H>::value;
        using std::optional<H>::has_value;
        using std::optional<H>::emplace;
        using std::optional<H>::operator->;
        using ArgType = typename H::AxisTupleArithmeticType;

        /// Forward operator call to underlying histogram
        [[nodiscard]] auto operator[]( ArgType&& arg ) { return this->value()[std::forward<ArgType>( arg )]; }

        /// Axis access
        template <unsigned int N>
        [[nodiscard]] auto& axis() const {
          return this->value().template axis<N>();
        }

        /// Wrapper class for histogram buffers
        template <typename B>
        struct Buffer : std::optional<B> {
          [[nodiscard]] auto operator[]( ArgType&& arg ) { return this->value()[std::forward<ArgType>( arg )]; }
        };

        /// Create a buffer object for this histogram
        [[nodiscard]] auto buffer() {
          using BuffT = decltype( this->value().buffer() );
          static_assert( std::is_move_constructible_v<BuffT> );
          return ( this->has_value() ? Buffer<BuffT>{ ( *this )->buffer() } : Buffer<BuffT>{} );
        }
      };
    } // namespace details

    using DefaultArithmeticType = double;

    // 1D histogram types
    template <typename TYPE                            = DefaultArithmeticType,
              Gaudi::Accumulators::atomicity ATOMICITY = Gaudi::Accumulators::atomicity::full>
    using H1D = details::BaseH<Gaudi::Accumulators::StaticHistogram<1, ATOMICITY, TYPE>>;
    template <typename TYPE                            = DefaultArithmeticType,
              Gaudi::Accumulators::atomicity ATOMICITY = Gaudi::Accumulators::atomicity::full>
    using WH1D = details::BaseH<Gaudi::Accumulators::StaticWeightedHistogram<1, ATOMICITY, TYPE>>;
    template <typename TYPE                            = DefaultArithmeticType,
              Gaudi::Accumulators::atomicity ATOMICITY = Gaudi::Accumulators::atomicity::full>
    using P1D = details::BaseH<Gaudi::Accumulators::StaticProfileHistogram<1, ATOMICITY, TYPE>>;
    template <typename TYPE                            = DefaultArithmeticType,
              Gaudi::Accumulators::atomicity ATOMICITY = Gaudi::Accumulators::atomicity::full>
    using WP1D = details::BaseH<Gaudi::Accumulators::StaticWeightedProfileHistogram<1, ATOMICITY, TYPE>>;

    // 2D histogram types
    template <typename TYPE                            = DefaultArithmeticType,
              Gaudi::Accumulators::atomicity ATOMICITY = Gaudi::Accumulators::atomicity::full>
    using H2D = details::BaseH<Gaudi::Accumulators::StaticHistogram<2, ATOMICITY, TYPE>>;
    template <typename TYPE                            = DefaultArithmeticType,
              Gaudi::Accumulators::atomicity ATOMICITY = Gaudi::Accumulators::atomicity::full>
    using WH2D = details::BaseH<Gaudi::Accumulators::StaticWeightedHistogram<2, ATOMICITY, TYPE>>;
    template <typename TYPE                            = DefaultArithmeticType,
              Gaudi::Accumulators::atomicity ATOMICITY = Gaudi::Accumulators::atomicity::full>
    using P2D = details::BaseH<Gaudi::Accumulators::StaticProfileHistogram<2, ATOMICITY, TYPE>>;
    template <typename TYPE                            = DefaultArithmeticType,
              Gaudi::Accumulators::atomicity ATOMICITY = Gaudi::Accumulators::atomicity::full>
    using WP2D = details::BaseH<Gaudi::Accumulators::StaticWeightedProfileHistogram<2, ATOMICITY, TYPE>>;

    // some type traits

    // Histogram Dimensionality
    template <typename T>
    constexpr bool is_1d_v = details::is_dim_v<T, 1>;
    template <typename T>
    constexpr bool is_2d_v = details::is_dim_v<T, 2>;

    // Profile versus regular histogram
    template <typename T>
    constexpr bool is_profile_v = details::is_one_of_hist_types_v<T, WP1D, WP2D, P1D, P2D>;
    template <typename T>
    constexpr bool is_hist_v = details::is_one_of_hist_types_v<T, WH1D, WH2D, H1D, H2D>;

    // Weighted versus non-weighted types
    template <typename T>
    constexpr bool is_weighted_v = details::is_one_of_hist_types_v<T, WH1D, WH2D, WP1D, WP2D>;
    template <typename T>
    constexpr bool is_not_weighted_v = details::is_one_of_hist_types_v<T, H1D, H2D, P1D, P2D>;

    /// Get histogram arithemtic type
    template <typename T>
    using param_t = typename T::value_type::AxisArithmeticType;

    template <typename PARAM, typename Z>
    constexpr bool param_check_v = details::is_safe_cast<PARAM, Z>();

    /// Array storage for histograms with buffer support
    template <typename T, std::size_t N, typename INDEX = std::size_t>
    struct Array : public Rich::TypedIndexArray<T, N, INDEX> {
      // create buffer object for contained histograms
      inline auto buffer() {
        auto buff_array = std::apply( []( auto&... i ) { return std::array{ i.buffer()... }; },
                                      static_cast<std::array<T, N>&>( *this ) );
        using BUFFER    = typename decltype( buff_array )::value_type;
        return Rich::TypedIndexArray<BUFFER, N, INDEX>( std::move( buff_array ) );
      }
    };

    template <typename HIST>
    using RadArray = Array<HIST, Rich::NRadiatorTypes, Rich::RadiatorType>;
    template <typename HIST>
    using DetArray = Array<HIST, Rich::NRiches, Rich::DetectorType>;
    template <typename HIST>
    using PanelArray = Array<HIST, Rich::NPDPanelsPerRICH, Rich::Side>;
    template <typename HIST>
    using PartArray = Array<HIST, Rich::NParticleTypes, Rich::ParticleIDType>;

    template <typename T>
    using QuadArray = Array<T, Rich::Utils::NQuadrants, Rich::Utils::Quadrant>;

  } // namespace Hist

  //-----------------------------------------------------------------------------
  /** @class HistoBase RichFutureKernel/RichHistoBase.h
   *
   *  Common base class for all RICH histogramming tools and algorithms.
   *
   *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *  @date   2009-07-27
   */
  //-----------------------------------------------------------------------------

  template <class PBASE>
  class HistoBase : public CommonBase<PBASE> {

  protected:
    // definitions

    /// short name for bin labels
    using BinLabels = std::vector<std::string>;
    /// short name for axis label
    using AxisLabel = std::string;

  public:
    // inherit constructors
    using CommonBase<PBASE>::CommonBase;

  protected:
    /// Histogram Constructor initisalisations
    inline void initRichHistoConstructor() {
      // Nothing at the moment ...
      // keep for now in case needed, but if not eventually should just remove.
    }

    /// Tuple Constructor initisalisations
    inline void initRichTupleConstructor() {
      // Place all tuples under RICH/ sub-dir
      const auto sc = ( this->setProperty( "NTupleTopDir", "RICH/" ) && //
                        this->setProperty( "NTupleLUN", "RICHTUPLE1" ) );
      if ( !sc ) { throw Rich::Exception( "Failed to set ntupling properties" ); }
    }

  public:
    /// System initialize
    virtual StatusCode sysInitialize() override;

  protected:
    /// Number of bins for 1D histograms
    inline auto nBins1D() const noexcept { return m_nBins1D.value(); }

    /// Number of bins for 2D histograms
    inline auto nBins2D() const noexcept { return m_nBins2D.value(); }

  private:
    /// Number of bins for 1D histograms
    Gaudi::Property<unsigned int> m_nBins1D{ this, "NBins1DHistos", 100 };

    /// Number of bins for 2D histograms
    Gaudi::Property<unsigned int> m_nBins2D{ this, "NBins2DHistos", 50 };

  protected:
    /** @brief Place to book all histograms which must be present after initialisation
     *
     *  Useful for online monitoring where booking on-demand does not play well with
     *  the online histogram systems
     *
     *  This method is automatically called during initialisation. It is not neccessary
     *  to call it by hand. Simply implement this method in any monitoring code which
     *  needs to pre-book histograms.
     *
     *  @return StatusCode indicating if booking was successful
     */
    virtual StatusCode prebookHistograms();

  protected:
    // Histogram initialisation

    template <typename H, typename... Args>
    bool initHist( H& h, Args&&... args ) const {
      if constexpr ( Hist::is_1d_v<H> ) { return init1D( h, std::forward<Args>( args )... ); }
      if constexpr ( Hist::is_2d_v<H> ) { return init2D( h, std::forward<Args>( args )... ); }
    }

    /// 1D histogram
    template <typename H1D>
    bool init1D( H1D&                     h,               //
                 const Rich::HistogramID& id,              //
                 std::string              title,           //
                 const Hist::param_t<H1D> low,             //
                 const Hist::param_t<H1D> high,            //
                 const unsigned int       bins,            //
                 AxisLabel                xAxisLabel = "", //
                 AxisLabel                yAxisLabel = "", //
                 BinLabels                binLabels  = {} ) const {
      using Axis = Gaudi::Accumulators::Axis<Hist::param_t<H1D>>;
      title      = id.fullTitle( title );
      if ( !xAxisLabel.empty() ) {
        title += ";" + xAxisLabel;
        // Only allow y Axis label if we also have an X axis label
        if ( !yAxisLabel.empty() ) { title += ";" + yAxisLabel; }
      }
      h.emplace( this, "/RICH/" + this->name() + "/" + id.fullid(), std::move( title ),
                 Axis( bins, low, high, std::move( xAxisLabel ), std::move( binLabels ) ) );
      return h.has_value();
    }

    /// 2D histogram
    template <typename H2D>
    bool init2D( H2D&                     h,               //
                 const Rich::HistogramID& id,              //
                 std::string              title,           //
                 const Hist::param_t<H2D> lowX,            //
                 const Hist::param_t<H2D> highX,           //
                 const unsigned int       binsX,           //
                 const Hist::param_t<H2D> lowY,            //
                 const Hist::param_t<H2D> highY,           //
                 const unsigned int       binsY,           //
                 AxisLabel                xAxisLabel = "", //
                 AxisLabel                yAxisLabel = "", //
                 AxisLabel                zAxisLabel = "", //
                 BinLabels                xBinLabels = {}, //
                 BinLabels                yBinLabels = {} ) const {
      using Axis = Gaudi::Accumulators::Axis<Hist::param_t<H2D>>;
      title      = id.fullTitle( title );
      if ( !xAxisLabel.empty() ) {
        title += ";" + xAxisLabel;
        // Only allow y Axis label if we also have an X axis label
        if ( !yAxisLabel.empty() ) {
          title += ";" + yAxisLabel;
          // Only allow z Axis label if we also have an X and Y axis labels
          if ( !zAxisLabel.empty() ) { title += ";" + zAxisLabel; }
        }
      }
      h.emplace( this, "/RICH/" + this->name() + "/" + id.fullid(), std::move( title ),
                 Axis( binsX, lowX, highX, std::move( xAxisLabel ), std::move( xBinLabels ) ),
                 Axis( binsY, lowY, highY, std::move( yAxisLabel ), std::move( yBinLabels ) ) );
      return h.has_value();
    }

  private:
    /// Flag to indicate if histograms have been booked or not
    bool m_histosAreBooked{ false };
  };

} // namespace Rich::Future
