/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file RichTrackSegment.cpp
 *
 *  Implementation file for class : LHCb::RichTrackSegment
 *
 *  @author  Chris Jones  Christopher.Rob.Jones@cern.ch
 *  @author  Antonis Papanestis
 *  @date    2002-06-10
 */
//-----------------------------------------------------------------------------

// local
#include "RichUtils/RichTrackSegment.h"

#include "GaudiKernel/Bootstrap.h"
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/SystemOfUnits.h"

#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"

#include <cmath>
#include <iostream>
#include <limits>
#include <mutex>

namespace {

  // access the mass^2 for a given hypothesis, using the Particle Property Svc
  auto pidMass2( const Rich::ParticleIDType pid ) {
    static Rich::ParticleArray<double> masses2;
    static std::once_flag              run_once;
    std::call_once( run_once, [&]() {
      const auto ppSvc = Gaudi::svcLocator()->service<LHCb::IParticlePropertySvc>( "LHCb::ParticlePropertySvc", true );
      assert( ppSvc );
      masses2[Rich::Electron]       = std::pow( ppSvc->find( "e+" )->mass() / Gaudi::Units::MeV, 2 );
      masses2[Rich::Muon]           = std::pow( ppSvc->find( "mu+" )->mass() / Gaudi::Units::MeV, 2 );
      masses2[Rich::Pion]           = std::pow( ppSvc->find( "pi+" )->mass() / Gaudi::Units::MeV, 2 );
      masses2[Rich::Kaon]           = std::pow( ppSvc->find( "K+" )->mass() / Gaudi::Units::MeV, 2 );
      masses2[Rich::Proton]         = std::pow( ppSvc->find( "p+" )->mass() / Gaudi::Units::MeV, 2 );
      masses2[Rich::Deuteron]       = std::pow( ppSvc->find( "deuteron" )->mass() / Gaudi::Units::MeV, 2 );
      masses2[Rich::BelowThreshold] = std::numeric_limits<double>::max();
    } );
    return masses2[pid];
  }

} // namespace

double LHCb::RichTrackSegment::velocity( const Rich::ParticleIDType pid ) const {
  // Velocity assuming given PID type
  const auto m2 = pidMass2( pid );
  return ( bestMomentumMag() * Gaudi::Units::c_light ) / std::sqrt( bestMomentum().Mag2() + m2 );
}

void LHCb::RichTrackSegment::updateState( const Gaudi::XYZPoint& rotPnt, const Gaudi::Transform3D& trans ) {
  // Entry point
  const auto toEntry = entryPoint() - rotPnt;
  // Middle point
  const auto toMid = middlePoint() - rotPnt;
  // exit point
  const auto toExit = exitPoint() - rotPnt;
  // set the states
  setStates( rotPnt + trans( toEntry ), trans( entryMomentum() ), //
             rotPnt + trans( toMid ), trans( middleMomentum() ),  //
             rotPnt + trans( toExit ), trans( exitMomentum() ) );
}

void LHCb::RichTrackSegment::updateTimeCache() {
  // sanity check state info makes sense, to avoid FPEs
  if ( entryPoint().Z() > 0 && fabs( entryMomentum().Z() ) > 0 ) {

    // Path length from origin point to radiator entry
    auto pathLenToEntry = std::sqrt( std::pow( entryPoint().X() - originVertex().X(), 2 ) +
                                     std::pow( entryPoint().Y() - originVertex().Y(), 2 ) +
                                     std::pow( entryPoint().Z() - originVertex().Z(), 2 ) );

    if ( radiator() == Rich::Rich2Gas ) {
      // Curved path correction following example in OTMeasurementProvider
      // Correction following the kick formula (to 2nd order in dtx)
      const auto zmagnet = 5140.0 * Gaudi::Units::mm;
      const auto zEntInv = 1.0 / entryPoint().Z();
      const auto f       = zmagnet * zEntInv;
      const auto dtx     = ( entryMomentum().X() / entryMomentum().Z() ) - ( entryPoint().X() * zEntInv );
      const auto dL      = 0.5 * ( ( 1.0 - f ) / f ) * dtx * dtx * pathLenToEntry;
      // The kick correction is actually too long. Until we find something
      // better, we simply scale with a factor derived in MC
      pathLenToEntry += ( 0.575 * dL );
    }

    // Loop over all mass hypos
    for ( const auto pid : Rich::particles() ) {
      // velocity for this hypo
      const auto v = velocity( pid );
      // finally set transit time
      m_timeToRadEntry[pid] = ( v > 0 ? originTime() + ( pathLenToEntry / v ) : std::numeric_limits<double>::max() );
    }
  }
}

void LHCb::RichTrackSegment::updateStateCache() {
  // compute the cached rotation matrices for the angle calculations
  const auto z = bestMomentum().Unit();
  auto       y = z.Cross( Gaudi::XYZVector( 1, 0, 0 ) );
  y *= vdt::fast_isqrtf( y.Mag2() ); // maybe not needed ?
  const auto x = y.Cross( z );
  m_rotation2  = Gaudi::Rotation3D( x.X(), y.X(), z.X(), x.Y(), y.Y(), z.Y(), x.Z(), y.Z(), z.Z() );
  m_rotation   = Gaudi::Rotation3D( m_rotation2.Inverse() );

  // Vector from entry to exit point
  const auto entryExitV( exitPoint() - entryPoint() );

  // mag^2 for entry to exit vector
  const auto entryExitVMag2 = entryExitV.mag2();

  // update entry to middle point vector
  m_midEntryV = ( middlePoint() - entryPoint() );

  // update middle to exit point vector
  m_exitMidV = ( exitPoint() - middlePoint() );

  // update factors
  m_invMidFrac1 = std::sqrt( entryExitVMag2 / m_midEntryV.mag2() );
  m_midFrac2    = std::sqrt( m_exitMidV.mag2() / entryExitVMag2 );

  // update the path length
  m_pathLength = std::sqrt( m_midEntryV.mag2() ) + std::sqrt( m_exitMidV.mag2() );

  // SIMD data caches
  m_entryPointSIMD  = entryPoint();
  m_middlePointSIMD = middlePoint();
  m_exitPointSIMD   = exitPoint();
  m_midEntryVSIMD   = m_midEntryV;
  m_exitMidVSIMD    = m_exitMidV;
  m_invMidFrac1SIMD = FP( m_invMidFrac1 );
  m_midFrac2SIMD    = FP( m_midFrac2 );
  m_rotationSIMD    = SIMDRotation3D( m_rotation );
  m_rotation2SIMD   = SIMDRotation3D( m_rotation2 );
}

Gaudi::XYZVector LHCb::RichTrackSegment::bestMomentum( const double fractDist ) const {
  // return the best momentum vector
  if ( zCoordAt( fractDist ) < middlePoint().z() ) {
    const auto midFrac = fractDist * m_invMidFrac1;
    return ( entryMomentum() * ( 1 - midFrac ) ) + ( middleMomentum() * midFrac );
  } else {
    const auto midFrac = ( fractDist / m_midFrac2 ) - 1.0;
    return ( middleMomentum() * ( 1 - midFrac ) ) + ( exitMomentum() * midFrac );
  }
}

std::ostream& LHCb::RichTrackSegment::fillStream( std::ostream& s ) const {
  s << "{ " << std::endl
    << " entryPoint:\t" << entryPoint() << std::endl
    << " middlePoint:\t" << middlePoint() << std::endl
    << " exitPoint:\t" << exitPoint() << std::endl
    << " entryMomentum:\t" << entryMomentum() << std::endl
    << " middleMomentum:\t" << middleMomentum() << std::endl
    << " exitMomentum:\t" << exitMomentum() << std::endl
    << " radiator:\t" << Rich::text( radiator() ) << std::endl
    << " rich:\t" << Rich::text( rich() ) << std::endl
    << " entryErrors:\t" << entryErrors() << std::endl
    << " middleErrors:\t" << middleErrors() << std::endl
    << " exitErrors:\t" << exitErrors() << std::endl
    << " originVtx:\t" << originVertex() << std::endl
    << " } ";
  return s;
}
