/*****************************************************************************\
* (c) Copyright 2000-2025 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

#include <array>
#include <cstdint>
#include <string>

namespace Rich::Utils {

  inline constexpr std::uint16_t NQuadrants = 4u;

  enum Quadrant : std::uint16_t { Q0 = 0, Q1, Q2, Q3 };

  inline auto quadrants() { return std::array{ Quadrant::Q0, Quadrant::Q1, Quadrant::Q2, Quadrant::Q3 }; }

  inline std::string quadString( const Quadrant q ) {
    switch ( q ) {
    case Quadrant::Q0:
      return " | Quadrant x>0 y>0";
    case Quadrant::Q1:
      return " | Quadrant x<0 y>0";
    case Quadrant::Q2:
      return " | Quadrant x>0 y<0";
    case Quadrant::Q3:
      return " | Quadrant x<0 y<0";
    default:
      return " | Quadrant UNDEFINED";
    }
  }

  template <typename T>
  inline auto quadrant( const T x, const T y ) {
    return ( x > 0 ? ( y > 0 ? Quadrant::Q0 : Quadrant::Q2 ) //
                   : ( y > 0 ? Quadrant::Q1 : Quadrant::Q3 ) );
  }

} // namespace Rich::Utils
