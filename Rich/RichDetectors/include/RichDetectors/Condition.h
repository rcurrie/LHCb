/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

#ifdef USE_DD4HEP
#  include <nlohmann/json.hpp>
#else
#  include "DetDesc/ParamValidDataObject.h"
#endif

#include <cassert>
#include <string>
#include <utility>

namespace Rich::Detector {

#ifdef USE_DD4HEP
  using Condition = nlohmann::json;
#else
  using Condition = ParamValidDataObject;
#endif

  inline auto condition_param_exists( const Condition& cond, const std::string& paramName ) {
#ifdef USE_DD4HEP
    return cond.find( paramName ) != cond.end();
#else
    return cond.exists( paramName );
#endif
  }
  inline auto condition_param_exists( const Condition* cond, const std::string& paramName ) {
    assert( cond );
    return condition_param_exists( *cond, paramName );
  }

  template <typename T>
  inline auto condition_param( const Condition& cond, const std::string& paramName ) {
#ifdef USE_DD4HEP
    return cond[paramName].get<T>();
#else
    return cond.param<T>( paramName );
#endif
  }
  template <typename T>
  inline auto condition_param( const Condition* cond, const std::string& paramName ) {
    assert( cond );
    return condition_param<T>( *cond, paramName );
  }

  template <typename T>
  inline auto condition_param( const Condition& cond, const std::string& paramName, const T def ) {
#ifdef USE_DD4HEP
    const auto c = cond.find( paramName );
    return ( cond.end() != c ? c->get<T>() : def );
#else
    return ( condition_param_exists( cond, paramName ) ? cond.param<T>( paramName ) : def );
#endif
  }
  template <typename T>
  inline auto condition_param( const Condition* cond, const std::string& paramName, const T def ) {
    assert( cond );
    return condition_param<T>( *cond, paramName, std::move( def ) );
  }

} // namespace Rich::Detector
