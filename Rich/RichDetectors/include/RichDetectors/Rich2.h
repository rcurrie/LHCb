/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// Local
#include "RichDetectors/Rich2Gas.h"
#include "RichDetectors/RichX.h"

// Detector description
#ifdef USE_DD4HEP
#  include "Detector/Rich2/DetElemAccess/DeRich2.h"
#else
#  include "RichDet/DeRich2.h"
#endif

namespace Rich::Detector {

#ifdef USE_DD4HEP
  using Rich2 = details::RichX<Rich::Rich2, LHCb::Detector::DeRich2, LHCb::Detector::DeRich, Rich::Detector::Rich2Gas>;
#else
  using Rich2 = details::RichX<Rich::Rich2, DeRich2, DeRich, Rich::Detector::Rich2Gas>;
#endif

} // namespace Rich::Detector
