/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// Detector description
#ifdef USE_DD4HEP
#  include "Detector/Rich/DeRichMapmt.h"
#else
#  include "RichDet/DeRichPD.h"
#endif

// eventually should be moved elsewhere
#include "RichDet/Rich1DTabProperty.h"

// LHCbKernel
#include "Kernel/RichSmartID.h"

// local
#include "RichDetectors/Utilities.h"

// LHCbMath
#include "LHCbMath/FastMaths.h"
#include "LHCbMath/SIMDTypes.h"

// RichUtils
#include "RichUtils/RichDAQDefinitions.h"
#include "RichUtils/RichSIMDRayTracing.h"

// STL
#include <algorithm>
#include <cstdint>
#include <ostream>
#include <type_traits>

namespace Rich::Detector {

  //-----------------------------------------------------------------------------
  /** @class PD RichPD.h
   *
   *  RICH photon detector helper class
   *
   *  @author Chris Jones
   *  @date   2020-10-05
   */
  //-----------------------------------------------------------------------------

  class PD final : public LHCb::SIMD::AlignedBase<LHCb::SIMD::VectorAlignment> {

  public:
    // types

    /// Tabulated function type
    using TabFunc = const Rich::TabulatedFunction1D;
    /// type for SIMD ray tracing result
    using SIMDRayTResult = Rich::RayTracingUtils::SIMDResult;
    /// scalar FP type for SIMD objects
    using FP = Rich::SIMD::DefaultScalarFP;
    /// SIMD float type
    using SIMDFP = Rich::SIMD::FP<FP>;
    /// SIMD Int32 type
    using SIMDINT32 = Rich::SIMD::Int32;
    /// SIMD UInt32 type
    using SIMDUINT32 = Rich::SIMD::UInt32;
    /// SIMD Point
    using SIMDPoint = Rich::SIMD::Point<FP>;
    /// SIMD Vector
    using SIMDVector = Rich::SIMD::Vector<FP>;
    /// Array of PD pointers
    using SIMDPDs = Rich::SIMD::STDArray<const PD*>;
    /// Array of SmartIDs
    using SIMDSmartIDs = SIMDRayTResult::SmartIDs;
    /// Scalar detection point
    using DetPtn = Gaudi::XYZPoint;

  public:
    // Accessors

    /// PD ID
    auto pdSmartID() const noexcept { return m_pdSmartID; }

    /// PD QE Curve
    auto pdQuantumEff() const noexcept { return m_pdQuantumEff.get(); }

    /// Effective pixel area
    auto effectivePixelArea() const noexcept { return m_effPixelArea; }

    /// effective number of pixels per PD
    auto effectiveNumActivePixels() const noexcept { return m_numPixels; }

    /// Check if this is an H-Type (large) PD
    bool isHType() const noexcept { return m_isHType; }

    /// Access the local to global transform
    inline const auto& localToGlobal() const noexcept { return m_locToGloM; }

    /// Access the local to global transform (SIMD)
    inline const auto& localToGlobalSIMD() const noexcept { return m_locToGloMSIMD; }

    /// Compute the scalar detection point for a given RichSmartID
    inline auto detectionPoint( const LHCb::RichSmartID id ) const noexcept {
      // ID should always represent a pixel in this PD
      assert( id.pdID() == pdSmartID() );
      // compute point in PMT local frame
      const FP   fPixCol = id.pixelCol();
      const FP   fPixRow = id.pixelRow();
      const auto xh      = ( fPixCol - scalar( m_NumPixColFrac ) ) * scalar( m_EffectivePixelXSize );
      const auto yh      = ( fPixRow - scalar( m_NumPixRowFrac ) ) * scalar( m_EffectivePixelYSize );
      // transform to global frame and return
      return localToGlobal() * DetPtn{ xh, yh, scalar( m_localZcoord ) };
    }

    /// Converts RichSmartIDs to an SIMD point in global coordinates.
    inline auto detectionPoint( const SIMDSmartIDs& IDs ) const noexcept {
      // All IDs should always represent a pixel in this PD, or be invalid
      assert( std::all_of( IDs.begin(), IDs.end(),               //
                           [pdID = pdSmartID()]( const auto id ) //
                           { return ( !id.isValid() || id.pdID() == pdID ); } ) );
      // Extract the row, col numbers
      SIMDUINT32 row{}, col{};
      GAUDI_LOOP_UNROLL( SIMDFP::Size )
      for ( std::size_t i = 0; i < SIMDFP::Size; ++i ) {
        // ID valid test not needed as invalid IDs will anyway return 0
        col[i] = IDs[i].pixelCol();
        row[i] = IDs[i].pixelRow();
      }
      // compute point in PMT local frame
      using namespace LHCb::SIMD;
      const auto xh = ( simd_cast<SIMDFP>( col ) - m_NumPixColFrac ) * m_EffectivePixelXSize;
      const auto yh = ( simd_cast<SIMDFP>( row ) - m_NumPixRowFrac ) * m_EffectivePixelYSize;
      // convert to global frame and rturn
      return localToGlobalSIMD() * SIMDPoint{ xh, yh, m_localZcoord };
    }

    /// Returns the centre point of the PD in the global frame
    inline auto centrePointGlobal() const noexcept { return localToGlobal() * DetPtn{ 0, 0, 0 }; }

    /// Returns the centre point of the PD in the panel frame
    inline const auto& centrePointPanel() const noexcept { return m_zeroInPanelFrame; }

    /// Checks if an X,Y pair (in panel local coordinates) is in the acceptance of this PD
    template <typename TYPE>
    inline auto isInAcceptance( const TYPE x, const TYPE y ) const noexcept {
      if constexpr ( std::is_arithmetic<TYPE>::value ) {
        return ( fabs( x - centrePointPanel().X() ) < m_xAcceptance ) &&
               ( fabs( y - centrePointPanel().Y() ) < m_yAcceptance );
      } else {
        // TODO if ever required... For now compilation failure...
      }
    }

    /// Checks if an point (in panel local coordinates) is in the acceptance of this PD
    template <typename POINT>
    inline bool isInAcceptance( const POINT& ptn ) const noexcept {
      return isInAcceptance( ptn.X(), ptn.Y() );
    }

  public:
#ifndef USE_DD4HEP
    /// DetDesc Constructor
    template <typename PANEL, typename DEPD>
    PD( const PANEL& panel, const DEPD& pd )
        : m_pdSmartID( pd.pdSmartID() )
        , m_isHType( pd.pdSmartID().isHTypePMT() )
        , m_pdQuantumEff( pd.pdQuantumEff() )
        , m_effPixelArea( pd.effectivePixelArea() )
        , m_numPixels( pd.effectiveNumActivePixels() )
        , m_locToGloM( pd.toGlobalMatrix() ) {
      // form SIMD local -> global transform
      toSIMDTrans( m_locToGloM, m_locToGloMSIMD );
      // parameters for SmartID -> global position
      m_NumPixColFrac = pd.pmtData().PmtNumPixColFrac;
      m_NumPixRowFrac = pd.pmtData().PmtNumPixRowFrac;
      if ( isHType() ) {
        m_localZcoord         = pd.pmtData().zShift + pd.pmtData().PmtQwZSize + pd.pmtData().GrandPmtAnodeHalfThickness;
        m_EffectivePixelXSize = pd.pmtData().GrandPmtEffectivePixelXSize;
        m_EffectivePixelYSize = pd.pmtData().GrandPmtEffectivePixelYSize;
        m_xAcceptance         = FP( 0.5 ) * pd.pmtData().RichGrandPmtAnodeXSize;
        m_yAcceptance         = FP( 0.5 ) * pd.pmtData().RichGrandPmtAnodeYSize;
      } else {
        m_localZcoord         = pd.pmtData().zShift + pd.pmtData().PmtQwZSize + pd.pmtData().PmtAnodeHalfThickness;
        m_EffectivePixelXSize = pd.pmtData().PmtEffectivePixelXSize;
        m_EffectivePixelYSize = pd.pmtData().PmtEffectivePixelYSize;
        m_xAcceptance         = FP( 0.5 ) * pd.pmtData().RichPmtAnodeXSize;
        m_yAcceptance         = FP( 0.5 ) * pd.pmtData().RichPmtAnodeYSize;
      }
      // Set PMT center point in owning panel frame
      const auto zInGlobal = m_locToGloM * Gaudi::XYZPoint{ 0, 0, 0 };
      m_zeroInPanelFrame   = panel.globalToLocal() * zInGlobal;
    }
#else
    /// DD4HEP constructor
    template <typename PANEL, typename DEPD>
    PD( const PANEL&            panel, //
        const LHCb::RichSmartID id,    //
        const DEPD&             pd )
        : m_pdSmartID( id )
        , m_isHType( id.isHTypePMT() )
        , m_pdQuantumEff( std::make_shared<TabFunc>( pd.QE() ) )
        , m_effPixelArea( pd.effPixelArea() )
        , m_numPixels( pd.numPixels() )
        , m_locToGloM( pd.toGlobalMatrix() ) {
      // form SIMD local -> global transform
      toSIMDTrans( m_locToGloM, m_locToGloMSIMD );
      // parameters for SmartID -> global position
      using namespace LHCb::Detector::detail;
      m_localZcoord = SIMDFP( dd4hep_param<FP>( "RhPMTPhCathodeZPos" ) + dd4hep_param<FP>( "RhPMTQuartzThickness" ) +
                              ( FP( 0.5 ) * dd4hep_param<FP>( "RhPMTPhCathodeZThickness" ) ) );
      if ( isHType() ) {
        m_NumPixColFrac       = SIMDFP( FP( 0.5 ) * FP( dd4hep_param<int>( "RhGrandPMTNumPixelsInCol" ) - 1 ) );
        m_NumPixRowFrac       = SIMDFP( FP( 0.5 ) * FP( dd4hep_param<int>( "RhGrandPMTNumPixelsInRow" ) - 1 ) );
        m_EffectivePixelXSize = SIMDFP( dd4hep_param<FP>( "RhGrandPMTPixelXSize" ) );
        m_EffectivePixelYSize = SIMDFP( dd4hep_param<FP>( "RhGrandPMTPixelYSize" ) );
        m_xAcceptance         = FP( 0.5 ) * dd4hep_param<FP>( "RhGrandPMTAnodeXSize" );
        m_yAcceptance         = FP( 0.5 ) * dd4hep_param<FP>( "RhGrandPMTAnodeYSize" );
      } else {
        m_NumPixColFrac       = SIMDFP( FP( 0.5 ) * FP( dd4hep_param<int>( "RhPMTNumPixelsInCol" ) - 1 ) );
        m_NumPixRowFrac       = SIMDFP( FP( 0.5 ) * FP( dd4hep_param<int>( "RhPMTNumPixelsInRow" ) - 1 ) );
        m_EffectivePixelXSize = SIMDFP( dd4hep_param<FP>( "RhPMTPixelXSize" ) );
        m_EffectivePixelYSize = SIMDFP( dd4hep_param<FP>( "RhPMTPixelYSize" ) );
        m_xAcceptance         = FP( 0.5 ) * dd4hep_param<FP>( "RhPMTAnodeXSize" );
        m_yAcceptance         = FP( 0.5 ) * dd4hep_param<FP>( "RhPMTAnodeYSize" );
      }
      // Set PMT center point in owning panel frame
      const auto zInGlobal = m_locToGloM * Gaudi::XYZPoint{ 0, 0, 0 };
      m_zeroInPanelFrame   = panel.globalToLocal() * zInGlobal;
    }
#endif

  private:
    /// Computes the rotation of the PMT reference frame, around the PMT local frame X axis, in the global Frame
    inline auto xAxisRotationInGlobal() const noexcept {
      const auto refP = DetPtn{ 0, 1, 0 };
      const auto p0   = localToGlobal() * DetPtn{ 0, 0, 0 };
      const auto p1   = localToGlobal() * refP;
      auto       vG   = p1 - p0;
      vG.SetX( 0 ); // only use (y,z) projection to compute rotation about x axis
      const auto vGMag2 = vG.Mag2();
      return ( vGMag2 > 0 ? LHCb::Math::fast_acos( vG.Dot( refP ) / std::sqrt( vGMag2 ) ) : 0.0 );
    }
    /// Computes the rotation of the PMT reference frame, around the PMT local frame Y axis, in the global Frame
    inline auto yAxisRotationInGlobal() const noexcept {
      const auto refP = DetPtn{ 1, 0, 0 };
      const auto p0   = localToGlobal() * DetPtn{ 0, 0, 0 };
      const auto p1   = localToGlobal() * refP;
      auto       vG   = p1 - p0;
      vG.SetY( 0 ); // only use (x,z) projection to compute rotation about y axis
      const auto vGMag2 = vG.Mag2();
      return ( vGMag2 > 0 ? LHCb::Math::fast_acos( vG.Dot( refP ) / std::sqrt( vGMag2 ) ) : 0.0 );
    }
    /// Computes the rotation of the PMT reference frame, around the PMT local frame Z axis, in the global Frame
    inline auto zAxisRotationInGlobal() const noexcept {
      const auto refP = DetPtn{ 1, 0, 0 };
      const auto p0   = localToGlobal() * DetPtn{ 0, 0, 0 };
      const auto p1   = localToGlobal() * refP;
      auto       vG   = p1 - p0;
      vG.SetZ( 0 ); // only use (x,y) projection to compute rotation about z axis
      const auto vGMag2 = vG.Mag2();
      return ( vGMag2 > 0 ? LHCb::Math::fast_acos( vG.Dot( refP ) / std::sqrt( vGMag2 ) ) : 0.0 );
    }

    /// Overload ostream operator
    template <typename STREAM>
    STREAM& fillStream( STREAM& s ) const {
      return s << "[ PD " << pdSmartID()                                           //
               << " EffPixArea=" << effectivePixelArea()                           //
               << " EffNumPixs=" << effectiveNumActivePixels()                     //
               << " NumPixColFrac=" << scalar( m_NumPixColFrac )                   //
               << " NumPixRowFrac=" << scalar( m_NumPixRowFrac )                   //
               << " LocalZcoord=" << scalar( m_localZcoord )                       //
               << " CenPtnInGlo=" << centrePointGlobal()                           //
               << " CenPtnInPanel=" << centrePointPanel()                          //
               << " (x,y,z)RotnInGlo=(" << xAxisRotationInGlobal()                 //
               << "," << yAxisRotationInGlobal()                                   //
               << "," << zAxisRotationInGlobal() << ")"                            //
               << " (x,y)Acpt=+-(" << m_xAcceptance << "," << m_yAcceptance << ")" //
               << " QE=" << *pdQuantumEff()                                        //
               << " ]";
    }

  public:
    // messaging

    /// Overload MsgStream operator
    friend inline auto& operator<<( MsgStream& s, const PD& pd ) { return pd.fillStream( s ); }
    /// Overload ostream operator
    friend inline auto& operator<<( std::ostream& s, const PD& pd ) { return pd.fillStream( s ); }

  private:
    // data

    /// PD Smart ID
    LHCb::RichSmartID m_pdSmartID;

    /// cache locally H-Type PMT flag
    bool m_isHType{ false };

    /// QE Curve
    std::shared_ptr<TabFunc> m_pdQuantumEff;

    /// The effective pixel area (in mm^2) including any demagnification factors
    float m_effPixelArea{ 0 };

    /// (effective) Number of pixels
    float m_numPixels{ 0 };

    /// cache local->global transform
    ROOT::Math::Transform3D m_locToGloM;

    /// cache SIMD local->global transform
    alignas( LHCb::SIMD::VectorAlignment ) Rich::SIMD::Transform3D<Rich::SIMD::DefaultScalarFP> m_locToGloMSIMD;

    /// centre point in owning panel frame
    Gaudi::XYZPoint m_zeroInPanelFrame;

    // parameters for computing smartID detection points
    alignas( LHCb::SIMD::VectorAlignment ) SIMDFP m_localZcoord         = SIMDFP::Zero();
    alignas( LHCb::SIMD::VectorAlignment ) SIMDFP m_NumPixColFrac       = SIMDFP::Zero();
    alignas( LHCb::SIMD::VectorAlignment ) SIMDFP m_NumPixRowFrac       = SIMDFP::Zero();
    alignas( LHCb::SIMD::VectorAlignment ) SIMDFP m_EffectivePixelXSize = SIMDFP::Zero();
    alignas( LHCb::SIMD::VectorAlignment ) SIMDFP m_EffectivePixelYSize = SIMDFP::Zero();
    FP m_xAcceptance                                                    = 0;
    FP m_yAcceptance                                                    = 0;
  };

} // namespace Rich::Detector
