/*****************************************************************************\
 * * (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
 * *                                                                             *
 * * This software is distributed under the terms of the GNU General Public      *
 * * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 * *                                                                             *
 * * In applying this licence, CERN does not waive the privileges and immunities *
 * * granted to it by virtue of its status as an Intergovernmental Organization  *
 * * or submit itself to any jurisdiction.                                       *
 * \*****************************************************************************/

// Icludes
#include "Gaudi/BaseSink.h"
#include <GAUDI_VERSION.h>

#include <algorithm>
#include <deque>
#include <fstream>

#include "Phoenix/Store.h"

/**
 * Receive and collect multiple event data as input produced from different algorithms (in Rec/Vis) outputing in json
 * format, and output them into a single .json file in the particular format that Phoenix web event display supports
 * Phoenix Documentation: https://github.com/HSF/phoenix/blob/master/guides/users.md#using-phoenix-with-your-own-data
 *
 * @author Andreas PAPPAS
 * @date 22-07-2021
 */

namespace LHCb::Phoenix {

  class Sink : public Gaudi::Monitoring::BaseSink {

  public:
    Sink( std::string name, ISvcLocator* svcloc ) : BaseSink( name, svcloc ) {
      // only deal with Phoenix related entities
      setProperty( "TypesToSave", std::vector<std::string>{ "Phoenix:.*" } )
          .orThrow( "Unable to set typesToSaveProperty", "LHCb::Phoenix::Sink" );
    }

    void flush( bool ) override {
      std::map<std::pair<int, int>, nlohmann::json> json_data;
      applyToAllSortedEntities( [&json_data]( std::string const&, std::string const&, nlohmann::json const& j ) {
        for ( auto& entry : j ) {
          std::pair<int, int> index{ entry["run number"], entry["event number"] };

          // push run, event, gps number per unique event if they exist
          if ( json_data.find( index ) == json_data.end() ) {
            json_data[index]["run number"]          = entry["run number"];
            json_data[index]["event number"]        = entry["event number"];
            json_data[index]["gps time"]            = entry["gps time"];
            json_data[index]["bunch crossing type"] = entry["bunch crossing type"];
            // compute GMT human readable time, will be displayed in Phoenix
            // maybe UTC would be better ? To be reviewed anyway with C++20 where utc_clock is available
            const std::chrono::duration<uint64_t, std::micro>        dur{ entry["gps time"] };
            const std::chrono::time_point<std::chrono::system_clock> time{ dur };
            std::time_t       ctime = std::chrono::system_clock::to_time_t( time );
            std::tm           tm    = *std::gmtime( &ctime );
            std::stringstream ss;
            ss << std::put_time( &tm, "%Y-%m-%d %H:%M:%S GMT" );
            json_data[index]["time"] = ss.str();
          }

          // push only the core event data (by merging it)
          for ( auto& [k, content] : entry["Content"].items() ) {
            for ( auto& [k2, v] : content.items() ) { json_data[index][k][k2] = v; }
          }
        }
      } );
      /// initializing the file stream
      m_ostrm = std::ofstream{ m_fileName };
      // finally dump the data into the file
      nlohmann::json j;
      for ( auto& [p, v] : json_data ) {
        j[std::string( "EVENT-" ) + std::to_string( p.first ) + '-' + std::to_string( p.second )] = v;
      }
      m_ostrm << std::setw( m_indentation.value() ) << j << '\n';
      m_ostrm.flush();
    }

  private:
    // used to output to file
    std::ofstream m_ostrm;

    Gaudi::Property<std::string> m_fileName{ this, "FileName", "LHCb_EventData.json",
                                             "Name of file where to save event data in json format" };
    Gaudi::Property<int>         m_indentation{ this, "IndentationLevel", 2,
                                        "Indentation level for nlohmann library. -1 is compact format, 0 only line "
                                                "breaks, >0 selects actual indentation" };
  };
  DECLARE_COMPONENT( Sink )

} // namespace LHCb::Phoenix
