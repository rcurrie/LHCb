###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Locations (reconstruction) for configuring packing and unpacking."""

from collections import OrderedDict

reco_locations = OrderedDict(
    [
        (
            0.0,
            {
                "ChargedProtos": ("/Event/Rec/ProtoP/Charged", "ProtoParticles"),
                "NeutralProtos": ("/Event/Rec/ProtoP/Neutrals", "ProtoParticles"),
                "Tracks": ("/Event/Rec/Track/Best", "Tracks"),
                "MuonPIDTracks": ("/Event/Rec/Muon/MuonTracks", "Tracks"),
                "PVs": ("/Event/Rec/Vertex/Primary", "PVs"),
                "CaloElectrons": ("/Event/Rec/Calo/Electrons", "CaloHypos"),
                "CaloPhotons": ("/Event/Rec/Calo/Photons", "CaloHypos"),
                "CaloMergedPi0s": ("/Event/Rec/Calo/MergedPi0s", "CaloHypos"),
                "CaloSplitPhotons": ("/Event/Rec/Calo/SplitPhotons", "CaloHypos"),
                "MuonPIDs": ("/Event/Rec/Muon/MuonPID", "MuonPIDs"),
                "RichPIDs": ("/Event/Rec/Rich/PIDs", "RichPIDs"),
                "RecSummary": ("/Event/Rec/Summary", "RecSummary"),
            },
        ),
        (
            1.0,
            {
                "LongProtos": ("/Event/Rec/ProtoP/Long", "ProtoParticles"),
                "DownstreamProtos": ("/Event/Rec/ProtoP/Downstream", "ProtoParticles"),
                "UpstreamProtos": ("/Event/Rec/ProtoP/Upstream", "ProtoParticles"),
                "NeutralProtos": ("/Event/Rec/ProtoP/Neutrals", "ProtoParticles"),
                "LongTracks": ("/Event/Rec/Track/BestLong", "Tracks"),
                "DownstreamTracks": ("/Event/Rec/Track/BestDownstream", "Tracks"),
                "UpstreamTracks": ("/Event/Rec/Track/BestUpstream", "Tracks"),
                "Ttracks": ("/Event/Rec/Track/Ttrack", "Tracks"),
                "VeloTracks": ("/Event/Rec/Track/Velo", "Tracks"),
                "PVs": ("/Event/Rec/Vertex/Primary", "PVs"),
                "CaloElectrons": ("/Event/Rec/Calo/Electrons", "CaloHypos"),
                "CaloPhotons": ("/Event/Rec/Calo/Photons", "CaloHypos"),
                "CaloMergedPi0s": ("/Event/Rec/Calo/MergedPi0s", "CaloHypos"),
                "CaloSplitPhotons": ("/Event/Rec/Calo/SplitPhotons", "CaloHypos"),
                "RecSummary": ("/Event/Rec/Summary", "RecSummary"),
            },
        ),
        (
            1.1,
            {
                "LongProtos": ("/Event/Rec/ProtoP/Long", "ProtoParticles"),
                "DownstreamProtos": ("/Event/Rec/ProtoP/Downstream", "ProtoParticles"),
                "UpstreamProtos": ("/Event/Rec/ProtoP/Upstream", "ProtoParticles"),
                "NeutralProtos": ("/Event/Rec/ProtoP/Neutrals", "ProtoParticles"),
                "LongTracks": ("/Event/Rec/Track/BestLong", "Tracks"),
                "DownstreamTracks": ("/Event/Rec/Track/BestDownstream", "Tracks"),
                "UpstreamTracks": ("/Event/Rec/Track/BestUpstream", "Tracks"),
                "FittedVeloTracks": ("/Event/Rec/Track/BestVelo", "Tracks"),
                "PVs": ("/Event/Rec/Vertex/Primary", "PVs"),
                "CaloElectrons": ("/Event/Rec/Calo/Electrons", "CaloHypos"),
                "CaloPhotons": ("/Event/Rec/Calo/Photons", "CaloHypos"),
                "CaloMergedPi0s": ("/Event/Rec/Calo/MergedPi0s", "CaloHypos"),
                "CaloSplitPhotons": ("/Event/Rec/Calo/SplitPhotons", "CaloHypos"),
                "RecSummary": ("/Event/Rec/Summary", "RecSummary"),
            },
        ),
    ]
)

default_persistreco_version = 1.0

pp2mcp_locations = {
    "ChargedPP2MC": ("/Event/Relations/ChargedPP2MCP", "PP2MCPRelations"),
    "NeutralPP2MC": ("/Event/Relations/NeutralPP2MCP", "PP2MCPRelations"),
}
