###############################################################################
# (c) Copyright 2019-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Set of mock types for unit testing with PyConf
"""

from .dataflow import is_datahandle


class MockAlgorithm:
    """Mock equivalent of Algorithm class"""

    def __init__(self, name):
        self._name = name


class MockDataHandle:
    """Mock equivalent of DataHandle class"""

    def __init__(self, location, type, id=None):
        self._location = location
        self._type = type
        self._id = id or location
        self._producer = MockAlgorithm("DummyAlgo")

    @property
    def location(self):
        return self._location

    @property
    def type(self):
        return self._type

    @property
    def producer(self):
        return self._producer


def mock_is_datahandle(arg):
    """Returns True if arg is of type DataHandle or MockDataHandle"""
    return isinstance(arg, MockDataHandle) or is_datahandle(arg)
