PyConf
======

PyConf makes Gaudi application configuration safer, cleaner, and simpler to debug.

More details on the components PyConf provides is given in the
[`PyConf/__init__.py` file][init]. An example configuration is given in
[`options/example.py`][example].

[Moore][moore] is a complex application that uses PyConf.

[init]: python/PyConf/__init__.py
[example]: options/example.py
[moore]: https://gitlab.cern.ch/lhcb/Moore


Transition from LHCbApp
=======================

Look at an example of transition. A simple one is [hlt2_passthrough_persistreco_check.py](https://gitlab.cern.ch/lhcb/Moore/-/merge_requests/927/diffs#af0f8c6aa7e1f6063b6d5057f296e006e1b7810c)

Here are the main changes :

  - drop includes from ApplicationMgr, LHCbApp, IODataManager, IOHelper, configured_ann_svc, ...
  - add includes for CompositeNode, NodeLogic, configure_input, configure, options :

    ```
    from PyConf.control_flow import CompositeNode, NodeLogic
    from PyConf.application import configure_input, configure
    from Moore import options
    ```
  - replace the LHCbApp call with the adequate set of options (typically data_type, simulation, dddb_tag, conddb_tag)
    - note that TestFileDB is supported and you should use it in tests
    - you can (and should) set data_type, simulation, dddb_tag/geometry_version, conddb_tag/conditions_version, input_files and input_type in one go through :

    ```
    options.set_input_and_conds_from_testfiledb( entryInTestFileDB )
    ```
  - drop creation and configuration of ApplicationMgr and any use of IOHelper, IODataManager
  - replace them with adequate options (typically input_files, input_type)
  - call confligure_input :

    ```
    config = configure_input(options)
    ```
  - create a CompositeNode with the algorithms to run (algs list in the next lines) and call configure :

    ```
    cf_node = CompositeNode(
        'whatever_name_you_want_to_see_here',
        combine_logic=NodeLogic.NONLAZY_OR,  # Or other NodeLogic
        children=algs)
    config.update(configure(options, cf_node))
    ```

Some advanced features of RootIOAlg in PyConf
=============================================

How to use GaudiPython in Pyconf
--------------------------------
  simply add a line in your options :
  ```
  options.gaudipython_mode = True
  ```

How to read everything from input file into TES
-----------------------------------------------
  simply add a line in your options :
  ```
  options.root_ioalg_name = "RootIOAlgExt"
  ```
  note that this won't allow you to use the loaded data in any functional algorithm as no DataHandle is provided
  this is only a workaround in case you're using CopyInputStream and should be avoided

How to deal with overwrites of items in the TES
-----------------------------------------------
  this is simply forbidden now, but may happen if you use the previous workaround and you populate the TES with something in the original file
  in such case, simply ads a line in your options :
  ```
  options.root_ioalg_opts["IgnorePaths"] = ["PathToIgnore", "OtherPathToignore"]
  ```
