###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from LHCbTesting import LHCbExeTest
from LHCbTesting.preprocessors import RegexpReplacer


class Test(LHCbExeTest):
    command = ["Gaudi.exe", "../options/Aida2Root.opts"]
    reference = "refs/Aida2Root.yaml"
    preprocessor = LHCbExeTest.preprocessor + RegexpReplacer(
        when="^Aida2Root",
        orig=r"(INFO.*'(skewness|kurtosis)(Err)?'.*)\|([0-9.e+\- ]*\|){2}",
        repl=r"\1| ### | ### |",
    )
