###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiTesting import NO_ERROR_MESSAGES
from LHCbTesting import LHCbExeTest


class Test(LHCbExeTest):
    command = ["gaudirun.py", "-v"]
    timeout = 1200
    reference = {"messages_count": NO_ERROR_MESSAGES}

    def options(self):
        # from Gaudi.Configuration import *
        from DetDescChecks.Options import LoadDDDBTest

        LoadDDDBTest("Upgrade", sim=True)
