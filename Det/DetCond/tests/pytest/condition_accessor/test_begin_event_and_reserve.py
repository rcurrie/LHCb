###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from LHCbTesting import LHCbExeTest


class Test(LHCbExeTest):
    command = [
        "gaudirun.py",
        "../../options/begin_event_no_reserve.py",
        "../../options/fake_event_time.py",
    ]
    reference = "../../refs/begin_event_no_reserve.yaml"

    def options(self):
        # Make UpdateManagerSvc not use BeginEvent incident
        # (similar to multi-threading jobs, requires ReserveIOV)
        from Configurables import UpdateManagerSvc

        UpdateManagerSvc(IOVLockLocation="")
