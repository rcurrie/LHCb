/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#define MUONDET_MUONREADOUTCOND_CPP

// Include files
#include "MuonDet/MuonReadoutCond.h"
#include <cmath>
#include <iostream>

/** @file MuonReadoutCond.cpp
 *
 * Implementation of class : MuonReadoutCond
 * This is the Muon readout information, with electronic and chamber
 * effects required to convert GEANT information into fired channels
 *
 * @author David Hutchcroft, David.Hutchcroft@cern.ch
 *
 */

namespace {
  /// Utility routine to find the largest posible size of cluster
  template <typename Container>
  int maxCluster( const Container& c ) {
    return std::accumulate( c.begin(), c.end(), 0, []( int mx, const auto& i ) { return std::max( mx, i.clSize ); } );
  }

  // protect for FPE check
  double safe_exponential( double arg ) {
    if ( ( arg ) < -100. ) return 0.0;
    return exp( arg );
  }
} // namespace

// Standard Constructors
MuonReadoutCond::MuonReadoutCond() = default;

/// Copy constructor
MuonReadoutCond::MuonReadoutCond( MuonReadoutCond& obj ) : IValidity(), Condition( obj ), m_RList( obj.getRList() ) {}

/// update constructor, do a deep copy of all
/// except for the properties of a generic DataObject
void MuonReadoutCond::update( MuonReadoutCond& obj ) {
  Condition::update( (Condition&)obj );
  m_RList = obj.getRList();
}

/// Standard Destructor
MuonReadoutCond::~MuonReadoutCond() {}

StatusCode MuonReadoutCond::initialize() {
  /// Set CumProbX and CumProbY from cluserX and clusterY
  /// and make the randomnumber generators
  for ( auto& iRout : m_RList ) {

    if ( iRout.ClusterX.size() == 0 || iRout.ClusterY.size() == 0 ) { return StatusCode::FAILURE; }

    // start with X

    // remove old probabilties and make enough entries in the vector
    iRout.CumProbX.assign( maxCluster( iRout.ClusterX ), 0. );

    // fill CumProbX in position i with value p from cluster
    for ( const auto& probIt : iRout.ClusterX ) iRout.CumProbX[probIt.clSize - 1] = probIt.clProb;

    std::partial_sum( iRout.CumProbX.begin(), iRout.CumProbX.end(), iRout.CumProbX.begin() );

    // alright paranoia here, normalise the cumlative sum
    std::transform( iRout.CumProbX.begin(), iRout.CumProbX.end(), iRout.CumProbX.begin(),
                    [&iRout]( auto& i ) { return i / iRout.CumProbX.back(); } );

    // same for Y
    // remove old probabilties and make enough entries in the vector
    iRout.CumProbY.assign( maxCluster( iRout.ClusterY ), 0. );

    // fill CumProbY in position i with value p from cluster
    for ( const auto& probIt : iRout.ClusterY ) iRout.CumProbY[probIt.clSize - 1] = probIt.clProb;

    std::partial_sum( iRout.CumProbY.begin(), iRout.CumProbY.end(), iRout.CumProbY.begin() );

    // alright paranoia here, normalise the cumlative sum
    std::transform( iRout.CumProbY.begin(), iRout.CumProbY.end(), iRout.CumProbY.begin(),
                    [&iRout]( auto& i ) { return i / iRout.CumProbY.back(); } );
  }

  return StatusCode::SUCCESS;
}

/// Used by XML to set readout type from std::string to int
StatusCode MuonReadoutCond::addReadout( const std::string& rType, int& index ) {

  _readoutParameter tmpParameters;

  if ( rType == "Anode" ) {
    tmpParameters.ReadoutType = 0;
  } else if ( rType == "Cathode" ) {
    tmpParameters.ReadoutType = 1;
  } else {
    index = -1;
    return StatusCode::FAILURE;
  }
  tmpParameters.Efficiency       = 0.;
  tmpParameters.SyncDrift        = 0.;
  tmpParameters.ChamberNoise     = 0.;
  tmpParameters.ElectronicsNoise = 0.;
  tmpParameters.MeanDeadTime     = 0.;
  tmpParameters.RMSDeadTime      = 0.;
  tmpParameters.TimeGateStart    = 0.;
  tmpParameters.PadEdgeSizeX     = 0.;
  tmpParameters.PadEdgeSigmaX    = 0.;
  tmpParameters.PadEdgeSizeY     = 0.;
  tmpParameters.PadEdgeSigmaY    = 0.;
  tmpParameters.JitterMin        = 0;
  tmpParameters.JitterMax        = 0;

  m_RList.push_back( tmpParameters );
  index = m_RList.size() - 1;
  return StatusCode::SUCCESS;
}

/// Return the size of cluster corresponding to a random number (0->1) in X
int MuonReadoutCond::singleGapClusterX( double randomNumber, double xDistPadEdge, int i ) const {
  return this->singleGapCluster( 0, randomNumber, xDistPadEdge, i );
}

/// Return the size of cluster corresponding to a random number (0->1) in Y
int MuonReadoutCond::singleGapClusterY( double randomNumber, double yDistPadEdge, int i ) const {
  return this->singleGapCluster( 1, randomNumber, yDistPadEdge, i );
}

/// Used by singleGapClusterX and  singleGapClusterY to calculate value
int MuonReadoutCond::singleGapCluster( int xy, double randomNumber, double xpos, int i ) const {

  // Apologies for the high level of indirection, I decided that
  // having one routine that picked from X or Y was perferable to
  // two routines.

  if ( randomNumber > 1. || randomNumber < 0. ) { return 1; }

  const std::vector<double>* cumProb;
  std::vector<_clus>         local_cluster;
  std::vector<double>        local_cumProb;
  const double*              padEdgeSize;
  const double*              padEdgeSigma;
  if ( 0 == xy ) {
    cumProb      = &( m_RList[i].CumProbX );
    padEdgeSize  = &( m_RList[i].PadEdgeSizeX );
    padEdgeSigma = &( m_RList[i].PadEdgeSigmaX );
    for ( int size = 0; size < (int)m_RList[i].CumProbX.size(); size++ ) {
      local_cluster.push_back( m_RList[i].ClusterX[size] );
      local_cumProb.push_back( m_RList[i].CumProbX[size] );
    }
  } else {
    cumProb      = &( m_RList[i].CumProbY );
    padEdgeSize  = &( m_RList[i].PadEdgeSizeY );
    padEdgeSigma = &( m_RList[i].PadEdgeSigmaY );
    for ( int size = 0; size < (int)m_RList[i].CumProbY.size(); size++ ) {
      local_cluster.push_back( m_RList[i].ClusterY[size] );
      local_cumProb.push_back( m_RList[i].CumProbY[size] );
    }
  }

  // now should have a cumlative sum in *cumProb
  int icsize;
  if ( ( *cumProb ).size() >= 3 && randomNumber > ( *cumProb )[1] ) {
    // do _not_ do the correction for the pad edge effect
    // (cluster size at least 3 anyway
    icsize = 1;
    while ( ( *cumProb )[icsize] < randomNumber ) { ++icsize; }
  } else {
    // must correct for pad edge effect
    if ( ( *padEdgeSigma ) > 0. && ( *padEdgeSize ) < 0. ) {
      // tested that there _is_ an effect
      double edgeEffect =
          ( *padEdgeSize ) * safe_exponential( -0.5 * ( xpos * xpos ) / ( ( *padEdgeSigma ) * ( *padEdgeSigma ) ) );
      local_cumProb[0] += edgeEffect;
      local_cumProb[1] -= edgeEffect;
    }
    icsize = 0;
    while ( ( local_cumProb )[icsize] < randomNumber ) { ++icsize; }
  }
  return icsize + 1;
}

/// takes pairs: eg 3,0.2 and adds them to the X cluster size/prob list
void MuonReadoutCond::addClusterX( int size, double prob, int i ) {
  m_RList[i].ClusterX.push_back( _clus{ size, prob } );
}

/// takes pairs: eg 3,0.2 and adds them to the Y cluster size/prob list
void MuonReadoutCond::addClusterY( int size, double prob, int i ) {
  m_RList[i].ClusterY.push_back( _clus{ size, prob } );
}

void MuonReadoutCond::setTimeJitter( const std::vector<double>& jitterVec, double min, double max, int i ) {
  m_RList[i].JitterVector = jitterVec;
  m_RList[i].JitterMin    = min;
  m_RList[i].JitterMax    = max;
}
