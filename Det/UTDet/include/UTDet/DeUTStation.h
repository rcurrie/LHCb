/*****************************************************************************\
  * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
  *                                                                             *
  * This software is distributed under the terms of the GNU General Public      *
  * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
  *                                                                             *
  * In applying this licence, CERN does not waive the privileges and immunities *
  * granted to it by virtue of its status as an Intergovernmental Organization  *
  * or submit itself to any jurisdiction.                                       *
  \*****************************************************************************/
#pragma once

#include "Detector/UT/ChannelID.h"

#ifdef USE_DD4HEP

#  include "Detector/UT/DeUTStation.h"
using DeUTStation = LHCb::Detector::UT::DeUTSide;

#else

#  include "GaudiKernel/MsgStream.h"
#  include "UTDet/DeUTBaseElement.h"
#  include "UTDet/DeUTSide.h"
#  include <string>
#  include <vector>

/** @class DeUTModule DeUTModule.h UTDet/DeUTModule.h
 *
 *  Class representing a UT module (13 or 7 sensors)
 *
 *  @author Xuhao Yuan (based on code by Jianchun Wang, Matt Needham, Andy Beiter)
 *  @date   2021-04-02
 *
 */

struct DeUTStation : DeUTSide {
  using DeUTSide::DeUTSide;
};

#endif
