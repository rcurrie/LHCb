/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Detector/UT/ChannelID.h"

#ifdef USE_DD4HEP

#  include "Detector/UT/DeUTSensor.h"
using DeUTSensor = LHCb::Detector::UT::DeUTSensor;

#else

#  include "DetDesc/Condition.h"
#  include "GaudiAlg/GaudiTool.h"
#  include "GaudiKernel/Plane3DTypes.h"
#  include "GaudiKernel/SystemOfUnits.h"
#  include "Kernel/LineTraj.h"
#  include "LHCbMath/LineTypes.h"
#  include "UTDet/DeUTBaseElement.h"
#  include <memory>
#  include <string>

/** @class DeUTSensor DeUTSensor.h UTDet/DeUTSensor.h
 *
 *  Class representing a UT Sensor
 *
 *  @author Xuhao Yuan (based on code by Jianchun Wang, Matt Needham, Andy Beiter)
 *  @date   2021-04-02
 *
 */

class DeUTSector;

static const CLID CLID_DeUTSensor = 9330;

class DeUTSensor : public DeUTBaseElement {

public:
  /** parent type */
  using parent_type = DeUTSector;

  /** Constructor */
  using DeUTBaseElement::DeUTBaseElement;

  /**
   * Retrieves reference to class identifier
   * @return the class identifier for this class
   */
  static const CLID& classID() { return CLID_DeUTSensor; }

  /**
   * another reference to class identifier
   * @return the class identifier for this class
   */
  const CLID& clID() const override;

  /** initialization method
   * @return Status of initialisation
   */
  StatusCode initialize() override;

  /** check whether contains
   *  @param  aChannel channel
   *  @return bool
   */
  [[nodiscard]] bool contains( const LHCb::Detector::UT::ChannelID aChannel ) const override {
    return aChannel.uniqueSector() == elementID().uniqueSector();
  }

  /** sector identfier
   * @return id
   */
  [[nodiscard]] unsigned int id() const { return m_id; }
  char                       moduletype() const { return m_moduletype; }
  char                       sensorType() const { return this->moduletype(); }

  /** set sector id */
  DeUTSensor& setID( unsigned int id ) {
    m_id = id;
    return *this;
  }

  /** detector pitch
   * @return pitch
   */
  [[nodiscard]] float pitch() const { return m_pitch; }

  /** number of strips
   * @return number of strips
   */
  [[nodiscard]] unsigned int nStrip() const { return m_nStrip; }

  /**
   * check if valid strip number
   *
   */
  [[nodiscard]] bool isStrip( const int strip ) const {
    return ( strip >= static_cast<int>( m_firstStrip ) && strip < static_cast<int>( m_firstStrip + m_nStrip ) );
  }

  [[nodiscard]] bool getStripflip() const { return m_stripflip; }

  /** convert a local u to a strip
   * @param  u local u
   * @return bool strip
   **/
  [[nodiscard]] int localUToStrip( double u ) const;

  unsigned int firstStrip() const { return m_firstStrip; };

  auto version() const { return m_version; };

  /** convert strip to local U
   * @param strip
   * @param offset
   * @return local u
   */
  [[nodiscard]] double localU( unsigned int strip, double offset = 0. ) const;

  /** trajectory
   * @return trajectory for the fit
   */
  [[nodiscard]] LHCb::LineTraj<double> trajectory( unsigned int strip, double offset ) const;

  /** plane corresponding to the sector
   * @return the plane
   */
  [[nodiscard]] Gaudi::Plane3D const& plane() const { return m_plane; }

  /** plane corresponding to the stave entrance
   * @return the plane
   */
  [[nodiscard]] Gaudi::Plane3D const& entryPlane() const { return m_entryPlane; }

  /** plane corresponding to the stave exit
   * @return the plane
   */
  [[nodiscard]] Gaudi::Plane3D const& exitPlane() const { return m_exitPlane; }

  /** localInActive
   * @param  point point in local frame
   * @param  tol   tolerance
   * @return in active region
   */
  [[nodiscard]] bool localInActive( const Gaudi::XYZPoint& point,
                                    Gaudi::XYZPoint        tol = Gaudi::XYZPoint( 0., 0., 0. ) ) const;

  /** globalInActive
   * @param  point point in global frame
   * @param  tol   tolerance
   * @return bool in active region
   */
  [[nodiscard]] bool globalInActive( const Gaudi::XYZPoint& point,
                                     Gaudi::XYZPoint        tol = Gaudi::XYZPoint( 0., 0., 0. ) ) const {
    return localInActive( toLocal( point ), tol );
  }

  /** localInBondGap
   * @param  v     coordinate in local frame
   * @param  tol   tolerance
   * @return bool if in bond gap
   */
  [[nodiscard]] bool localInBondGap( float v, float tol ) const {
    return ( ( v + tol > m_uMinLocal ) && ( v - tol < m_uMaxLocal ) );
  }

  /** globalInActive
   * @param  point point in global frame
   * @param  tol   tolerance
   * @return bool in bondgap
   */
  [[nodiscard]] bool globalInBondGap( const Gaudi::XYZPoint& point, float tol = 0 ) const {
    return localInBondGap( toLocal( point ).Y(), tol );
  }

  /** thickness
   * @return float thickness
   */
  [[nodiscard]] float thickness() const { return m_thickness; }

  /** active height
   * @return float activeHeight
   **/
  [[nodiscard]] float activeHeight() const { return std::abs( m_vMinLocal - m_vMaxLocal ); }

  /** active height
   * @return float activeWidth
   **/
  [[nodiscard]] float activeWidth() const { return std::abs( m_uMinLocal - m_uMaxLocal ); }

  /**  sensor capacitance **/
  [[nodiscard]] double capacitance() const {
    // by chance the CMS and LHCb sensors have same capacitance
    constexpr float rho = 1.4 * Gaudi::Units::picofarad / Gaudi::Units::cm;
    return rho * activeHeight();
  }

  /** x sense of local frame relative to global */
  [[nodiscard]] bool xInverted() const { return m_xInverted; }

  /** y sense of local frame relative to global */
  [[nodiscard]] bool yInverted() const { return m_yInverted; }

  /** print to stream */
  std::ostream& printOut( std::ostream& os ) const override;

  /** print to msgstream */
  MsgStream& printOut( MsgStream& os ) const override;

  /** ouput operator for class DeUTSensor
   *  @see DeUTSensor
   *  @see MsgStream
   *  @param os      reference to STL output stream
   *  @param aSensor reference to DeUTSensor object
   */
  friend std::ostream& operator<<( std::ostream& os, const DeUTSensor& aSensor ) { return aSensor.printOut( os ); }

  /** ouput operator for class DeUTSensor
   *  @see DeUTSensor
   *  @see MsgStream
   *  @param os      reference to MsgStream output stream
   *  @param aSensor reference to DeUTSensor object
   */
  friend MsgStream& operator<<( MsgStream& os, const DeUTSensor& aSensor ) { return aSensor.printOut( os ); }

private:
  void       cacheParentProperties( const DeUTSector& parent );
  StatusCode initGeometryInfo();

private:
  parent_type* m_parent = nullptr;

  char       m_moduletype;
  void       clear();
  void       determineSense();
  StatusCode cacheInfo();
  StatusCode registerConditionsCallbacks();

  Gaudi::Plane3D m_plane;
  Gaudi::Plane3D m_entryPlane;
  Gaudi::Plane3D m_exitPlane;

  Gaudi::XYZVector          m_direction;
  std::pair<double, double> m_range;

  unsigned int m_firstStrip = 1u;
  GeoVersion   m_version    = GeoVersion::ErrorVersion;
  unsigned int m_id         = 0u;
  float        m_pitch      = 0.0;
  unsigned int m_nStrip     = 0u;
  std::string  m_versionString;

public: // To be removed later, should be private:
  float m_thickness = 0.0;
  float m_uMinLocal = 0.0;
  float m_uMaxLocal = 0.0;
  float m_vMinLocal = 0.0;
  float m_vMaxLocal = 0.0;

  std::optional<LHCb::LineTraj<double>> m_midTraj;

  bool m_xInverted = false;
  bool m_yInverted = false;

  float m_deadWidth   = 0.0;
  float m_stripLength = 0.0;

  bool m_stripflip = true;
};

[[deprecated( "please deref first" )]] std::ostream& operator<<( std::ostream& os, const DeUTSensor* aSensor ) = delete;
[[deprecated( "please deref first" )]] MsgStream&    operator<<( MsgStream& os, const DeUTSensor* aSensor )    = delete;

#endif
