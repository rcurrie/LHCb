/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "UTDet/DeUTStave.h"
#include "DetDesc/IGeometryInfo.h"
#include "Kernel/UTNames.h"
#include "UTDet/DeUTFace.h"
#include "UTDet/DeUTLayer.h"
#include "UTDet/DeUTModule.h"
#include "UTDet/DeUTSector.h"
#include "UTDet/DeUTSide.h"
#include <algorithm>
#include <numeric>

using namespace LHCb;

/** @file DeUTStave.cpp
 *
 *  Implementation of class :  DeUTStave
 *
 *  @author Xuhao Yuan (based on code by Andy Beiter, Jianchun Wang, Matt Needham)
 *  @date   2021-03-29
 *
 */

const CLID& DeUTStave::clID() const { return DeUTStave::classID(); }

StatusCode DeUTStave::initialize() {
  // initialize method
  StatusCode sc = DeUTBaseElement::initialize();

  if ( sc.isFailure() ) {
    MsgStream msg( msgSvc(), name() );
    msg << MSG::ERROR << "Failed to initialize detector element" << endmsg;
  } else {
    m_detRegion   = param<int>( "detRegion" );
    m_firstSector = param<int>( "firstReadoutSector" );
    m_column      = param<int>( "column" );
    m_numSectors  = param<int>( "numSectors" );

    if ( exists( "staveType" ) ) {
      m_staveRotZ = param<std::string>( "staveRotZ" );
      m_type      = param<std::string>( "staveType" );
    } else if ( exists( "moduleType" ) ) {
      m_staveRotZ = param<std::string>( "moduleRotZ" );
      m_type      = param<std::string>( "moduleType" );
    }

    m_parent                               = getParent<DeUTLayer>();
    const Detector::UT::ChannelID parentID = m_parent->elementID();

    if ( exists( "staveID" ) ) { // if the left-right split geommetry applied
      m_staveID = param<int>( "staveID" );
      Detector::UT::ChannelID chan( Detector::UT::ChannelID::detType::typeUT, parentID.side(), parentID.layer(),
                                    m_staveID, 0, 0, 0, 0 );
      setElementID( chan );
      m_faces = getChildren<DeUTFace>();
      for ( auto ptrface : m_faces ) {
        for ( auto ptrmodule : ptrface->modules() ) {
          m_modules.push_back( ptrmodule );
          const auto& vecptrsectors = ptrmodule->sectors();
          m_sectors.insert( m_sectors.end(), vecptrsectors.begin(), vecptrsectors.end() );
        }
      }
      m_firstSector = 0;
    } else {
      m_version              = GeoVersion::v0;
      unsigned int t_layerID = parentID.layer();
      unsigned int t_sideID  = 0u;
      if ( m_detRegion == 3 || ( m_detRegion == 2 && m_firstSector >= 41 ) ) t_sideID = 1u;
      unsigned int t_staveID = m_column - ( t_layerID < 2 ? 9 : 10 );
      if ( t_sideID == 0 ) t_staveID = ( t_layerID < 2 ? 8 : 9 ) - m_column;
      Detector::UT::ChannelID chan( Detector::UT::ChannelID::detType::typeUT, t_sideID, t_layerID, t_staveID, 0, 0, 0,
                                    0 );
      setElementID( chan );
      m_sectors = getChildren<DeUTSector>();
    }
  }

  m_sideID   = elementID().side();
  m_nickname = UTNames().UniqueStaveToString( this->elementID() );

  if ( exists( "version" ) ) m_versionString = param<std::string>( "version" );

  sc = registerCondition( this, m_prodIDString, &DeUTStave::updateProdIDCondition, true );
  if ( sc.isFailure() ) {
    MsgStream msg( msgSvc(), name() );
    msg << MSG::ERROR << "Failed to register prodID conditions" << endmsg;
    return sc;
  }

  return sc;
}

StatusCode DeUTStave::updateProdIDCondition() {
  const Condition* aCon = condition( m_prodIDString );
  if ( !aCon ) {
    MsgStream msg( msgSvc(), name() );
    msg << MSG::ERROR << "Failed to find condition" << endmsg;
    return StatusCode::FAILURE;
  }
  m_prodID = aCon->param<int>( "ProdID" );
  return StatusCode::SUCCESS;
}

std::ostream& DeUTStave::printOut( std::ostream& os ) const {
  return os << " Stave : " << name() << " type " << m_type << " Det region " << m_detRegion << " Column " << m_column
            << " side " << m_sideID << " staveID " << m_staveID << std::endl;
}

MsgStream& DeUTStave::printOut( MsgStream& os ) const {
  return os << " Stave : " << name() << " type " << m_type << " Det region " << m_detRegion << " Column " << m_column
            << " side " << m_sideID << " staveID " << m_staveID << std::endl;
}

const DeUTSector* DeUTStave::findSector( const Detector::UT::ChannelID aChannel ) const {
  auto iter = std::find_if( m_sectors.begin(), m_sectors.end(),
                            [&]( const DeUTSector* s ) { return s->contains( aChannel ); } );

  return iter != m_sectors.end() ? *iter : nullptr;
}

const DeUTSector* DeUTStave::findSector( const Gaudi::XYZPoint& point ) const {
  auto iter =
      std::find_if( m_sectors.begin(), m_sectors.end(), [&]( const DeUTSector* s ) { return s->isInside( point ); } );
  return iter != m_sectors.end() ? *iter : nullptr;
}

const DeUTFace* DeUTStave::findFace( const Detector::UT::ChannelID aChannel ) const {
  auto iter =
      std::find_if( m_faces.begin(), m_faces.end(), [&]( const DeUTFace* s ) { return s->contains( aChannel ); } );

  return iter != m_faces.end() ? *iter : nullptr;
}

const DeUTFace* DeUTStave::findFace( const Gaudi::XYZPoint& point ) const {
  auto iter = std::find_if( m_faces.begin(), m_faces.end(), [&]( const DeUTFace* s ) { return s->isInside( point ); } );
  return iter != m_faces.end() ? *iter : nullptr;
}

const DeUTModule* DeUTStave::findModule( const Detector::UT::ChannelID aChannel ) const {
  auto iter = std::find_if( m_modules.begin(), m_modules.end(),
                            [&]( const DeUTModule* s ) { return s->contains( aChannel ); } );

  return iter != m_modules.end() ? *iter : nullptr;
}

const DeUTModule* DeUTStave::findModule( const Gaudi::XYZPoint& point ) const {
  auto iter =
      std::find_if( m_modules.begin(), m_modules.end(), [&]( const DeUTModule* s ) { return s->isInside( point ); } );
  return iter != m_modules.end() ? *iter : nullptr;
}

double DeUTStave::fractionActive() const {
  return std::accumulate( m_sectors.begin(), m_sectors.end(), 0.0,
                          []( double f, const DeUTSector* s ) { return f + s->fractionActive(); } ) /
         double( m_sectors.size() );
}
