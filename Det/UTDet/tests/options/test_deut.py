#####################################################################################
# (c) Copyright 1998-2019 CERN for the benefit of the LHCb and ATLAS collaborations #
#                                                                                   #
# This software is distributed under the terms of the Apache version 2 licence,     #
# copied verbatim in the file "LICENSE".                                            #
#                                                                                   #
# In applying this licence, CERN does not waive the privileges and immunities       #
# granted to it by virtue of its status as an Intergovernmental Organization        #
# or submit itself to any jurisdiction.                                             #
#####################################################################################
from PyConf.application import ApplicationOptions, configure, configure_input
from PyConf.control_flow import CompositeNode

options = ApplicationOptions(_enabled=False)
options.data_type = "Upgrade"
options.evt_max = 1
options.input_type = "None"
options.simulation = True
options.geometry_version = "run3/trunk"
options.conditions_version = "master"
config = configure_input(options)

# Prepare detector description
dd4hepsvc = config["LHCb::Det::LbDD4hep::DD4hepSvc/LHCb::Det::LbDD4hep::DD4hepSvc"]
dd4hepsvc.VerboseLevel = 1
dd4hepsvc.GeometryLocation = "${DETECTOR_PROJECT_ROOT}/compact"
dd4hepsvc.GeometryMain = "LHCb.xml"
dd4hepsvc.DetectorList = ["/world", "UT"]
dd4hepsvc.ConditionsLocation = (
    "/cvmfs/lhcb.cern.ch/lib/lhcb/git-conddb/lhcb-conditions-database.git"
)

# Configure fake run number
from PyConf.Algorithms import DeUTTester
from PyConf.Algorithms import LHCb__Tests__FakeRunNumberProducer as FET

algs = [FET(name="FakeRunNumber", Start=42, Step=20), DeUTTester(name="DeUTTester")]

config.update(configure(options, CompositeNode("TopSeq", algs)))
