/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/Service.h"
#include "GaudiKernel/Vector3DTypes.h"
#include "Kernel/ILHCbMagnetSvc.h"
#include <cstdlib>
#include <numeric>
#include <vector>

/** @file MultipleMagneticFieldSvc.cpp
 *  Implementation of MultipleMagneticFieldSvc class
 *
 *  @author Magnus Lieng
 *  @date   2008-07-03
 */

/** @class MultipleMagneticFieldSvc MultipleMagneticFieldSvc.h
 *  A service for wrapping multiple MagneticFieldSvc
 *
 *  @author Magnus Lieng
 *  @date   2008-07-03
 */

class MultipleMagneticFieldSvc : public extends<Service, ILHCbMagnetSvc> {

public:
  /// Standard Constructor.
  using extends::extends;

  /// Initialise the service (Inherited Service overrides)
  StatusCode initialize() override;

  /// Finalise the service (Inherited Service overrides)
  StatusCode finalize() override;

  using ILHCbMagnetSvc::fieldVector; // avoid hiding compiler  warning...

  /** Implementation of ILHCbMagneticFieldSvc interface.
   * @param[in]  xyz Point at which magnetic field vector will be given
   * @return fvec Magnectic field vector.
   */
  ROOT::Math::XYZVector fieldVector( const Gaudi::XYZPoint& xyz ) const override;

  // These should be incorporated in a better way...
  bool   useRealMap() const override;            ///< True is using real map
  double scaleFactor() const;                    ///< accessor to m_scaleFactor
  int    polarity() const;                       ///< Polarity (only if they are all the same!)
  bool   isDown() const override;                ///< sign of the polarity
  double signedRelativeCurrent() const override; ///< includes scale factor for polarity and current
  const LHCb::Magnet::MagneticFieldGrid* fieldGrid() const override {
    throw GaudiException( "MultipleMagneticFieldSvc::fieldGrid() cannot be used", name(), StatusCode::FAILURE );
  }

private:
  Gaudi::Property<std::vector<std::string>> m_names{ this, "MagneticFieldServices" };
  std::vector<SmartIF<ILHCbMagnetSvc>>      m_magneticFieldSvcs; ///< Magnetic field services.
};

DECLARE_COMPONENT( MultipleMagneticFieldSvc )

//=============================================================================
// Initialize
//=============================================================================
StatusCode MultipleMagneticFieldSvc::initialize() {
  StatusCode sc = extends::initialize();
  if ( sc.isFailure() ) return sc;

  /// Getting the member magnetic field services
  for ( const auto& name : m_names ) {
    m_magneticFieldSvcs.emplace_back( service( name, true ) );
    if ( !m_magneticFieldSvcs.back() ) {
      warning() << " Member " << name << " not found" << endmsg;
      m_magneticFieldSvcs.pop_back();
    }
  }

  /// Warn if no fields are set up
  if ( m_magneticFieldSvcs.empty() ) { warning() << " No member fields configured" << endmsg; }
  return sc;
}

//=============================================================================
// Finalize
//=============================================================================
StatusCode MultipleMagneticFieldSvc::finalize() {
  // release the used fields
  m_magneticFieldSvcs.clear();
  return extends::finalize();
}

//=============================================================================
// FieldVector: find the magnetic field value at a given point in space
//=============================================================================
Gaudi::XYZVector MultipleMagneticFieldSvc::fieldVector( const Gaudi::XYZPoint& r ) const {
  /// sum field strength from member fields
  return std::accumulate(
      m_magneticFieldSvcs.begin(), m_magneticFieldSvcs.end(), ROOT::Math::XYZVector{ 0., 0., 0. },
      [&]( ROOT::Math::XYZVector bf, const ILHCbMagnetSvc* fs ) { return bf + fs->fieldVector( r ); } );
}

//=============================================================================
// UseRealMap: Check if any member fields use real maps
//=============================================================================
bool MultipleMagneticFieldSvc::useRealMap() const {
  return std::any_of( m_magneticFieldSvcs.begin(), m_magneticFieldSvcs.end(),
                      []( const ILHCbMagnetSvc* fs ) { return fs->useRealMap(); } );
}

//=============================================================================
// polarity: meaningless for multiple fields unless they are all the same
//=============================================================================
bool MultipleMagneticFieldSvc::isDown() const {

  auto neq = std::adjacent_find(
      m_magneticFieldSvcs.begin(), m_magneticFieldSvcs.end(),
      []( const ILHCbMagnetSvc* first, const ILHCbMagnetSvc* second ) { return first->isDown() != second->isDown(); } );
  if ( neq != m_magneticFieldSvcs.end() ) {
    throw GaudiException( "Invalid call to MultipleMagneticFieldSvc::isDown()", "INCONSISTENT POLARITIES",
                          StatusCode::FAILURE );
  }
  // if empty, we're neither down nor up... so we anser 'isDown' with 'no'
  return !m_magneticFieldSvcs.empty() && m_magneticFieldSvcs.front()->isDown();
}

//=============================================================================
// signedRelativeCurrent: meaningless for multiple fields unless they
// are all the same polarity
//=============================================================================

double MultipleMagneticFieldSvc::signedRelativeCurrent() const {
  /// Attaining scale factor of member fields (The average)
  auto scale = std::accumulate( m_magneticFieldSvcs.begin(), m_magneticFieldSvcs.end(), 0.,
                                []( double s, const ILHCbMagnetSvc* fs ) { return s + fs->signedRelativeCurrent(); } );
  return !m_magneticFieldSvcs.empty() ? scale / m_magneticFieldSvcs.size() : 0.;
}
