/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "initOutputLevel.h"
#include <DetDesc/Condition.h>
#include <GaudiKernel/IUpdateManagerSvc.h>
#include <GaudiKernel/SystemOfUnits.h>
#include <VPDet/DeVP.h>
#include <algorithm>

namespace {
  inline const std::string beamSpotCond = "/dd/Conditions/Online/Velo/MotionSystem";
}

//============================================================================
// Object identification
//============================================================================
const CLID& DeVP::clID() const { return DeVP::classID(); }

//============================================================================
// Initialisation method
//============================================================================

namespace {
  template <class CallerClass, class ObjectClass>
  void registerRecursively( IUpdateManagerSvc* updMgrSvc, ObjectClass& element, CallerClass& callbackelement,
                            typename ObjectMemberFunction<CallerClass>::MemberFunctionType mf ) {
    updMgrSvc->registerCondition( &callbackelement, element.geometry(), mf );
    for ( auto* it : element.childIDetectorElements() ) registerRecursively( updMgrSvc, *it, callbackelement, mf );
  }
} // namespace

StatusCode DeVP::initialize() {

  auto sc = initOutputLevel( msgSvc(), "DeVP" );
  if ( !sc ) return sc;

  const auto lvl = msgSvc()->outputLevel( "DeVP" );
  m_debug        = lvl <= MSG::DEBUG;

  // Initialise the base class.
  sc = DetectorElementPlus::initialize();
  if ( sc.isFailure() ) {
    msg() << MSG::ERROR << "Cannot initialize DetectorElement" << endmsg;
    return sc;
  }

  // Get all daughter detector elements.
  findSensors( this, m_sensors );
  std::sort( m_sensors.begin(), m_sensors.end(), less_SensorNumber() );

  if ( m_debug ) {
    msg() << MSG::DEBUG << "Found " << m_sensors.size() << " sensors" << endmsg;
    unsigned int nLeftSensors =
        std::count_if( m_sensors.begin(), m_sensors.end(), []( const auto* s ) { return s->isLeft(); } );
    unsigned int nRightSensors = m_sensors.size() - nLeftSensors;
    msg() << MSG::DEBUG << "There are " << m_sensors.size() << " sensors "
          << "(left: " << nLeftSensors << ", right: " << nRightSensors << ")" << endmsg;
  }
  // Register update of the local cach on geometry changes
  // updMgrSvc()->registerCondition( this, geometry(), &DeVP::updateCache );
  registerRecursively( updMgrSvc(), *this, *this, &DeVP::updateCache );

  // Retrieve and register the beamspot condition
  SmartDataPtr<Condition> cond{ dataSvc(), beamSpotCond };
  if ( !cond ) {
    msg() << MSG::ERROR << "Failed to retrieve resolver condition" << endmsg;
    return StatusCode::FAILURE;
  } else {
    m_beamSpotCond = cond.ptr();
    updMgrSvc()->registerCondition( this, beamSpotCond, &DeVP::updateBeamSpot );
    return StatusCode::SUCCESS;
  }
}

//============================================================================
// Get sensitive volume identifier for a given point in the global frame
//============================================================================
int DeVP::sensitiveVolumeID( const Gaudi::XYZPoint& point ) const {

  const double     z   = point.z();
  constexpr double tol = 10 * Gaudi::Units::mm;
  auto             i   = std::find_if( m_sensors.begin(), m_sensors.end(), [&]( const DeVPSensor* s ) {
    // Quickly skip sensors which are far away in z, only
    // then check whether point is inside this sensor
    return ( fabs( z - s->z() ) < tol ) && s->isInside( point );
  } );
  if ( i != m_sensors.end() ) return to_unsigned( ( *i )->sensorNumber() );
  msg() << MSG::ERROR << "No sensitive volume at (" << point.x() << ", " << point.y() << ", " << point.z() << ")"
        << endmsg;
  return -999;
}

//============================================================================
// Scan detector element tree for VP sensors
//============================================================================
void DeVP::findSensors( IDetectorElementPlus* det, std::vector<DeVPSensor*>& sensors ) {

  // Get the daughter detector elements.
  for ( const auto& element : det->childIDetectorElements() ) {
    if ( m_debug ) {
      msg() << MSG::DEBUG << std::setw( 12 ) << std::setiosflags( std::ios::left ) << element->name() << endmsg;
    }
    // Check if this is a VP sensor.
    DeVPSensor* p = dynamic_cast<DeVPSensor*>( element );
    if ( p ) {
      // Add the sensor to the list.
      sensors.push_back( p );
      if ( m_debug ) { msg() << MSG::DEBUG << "Storing detector " << element->name() << endmsg; }
    } else {
      // Recurse.
      findSensors( element, sensors );
    }
  }
}

StatusCode DeVP::updateCache() {
  const auto& s = sensor( LHCb::Detector::VPChannelID::SensorID{ 0 } );
  std::copy( s.xLocal().begin(), s.xLocal().end(), m_local_x.begin() );
  std::copy( s.xPitch().begin(), s.xPitch().end(), m_x_pitch.begin() );
  m_pixel_size = s.pixelSize( { LHCb::Detector::VPChannelID::SensorID{ 0 }, LHCb::Detector::VPChannelID::ChipID{ 0 },
                                LHCb::Detector::VPChannelID::ColumnID{ 0 }, LHCb::Detector::VPChannelID::RowID{ 0 } } )
                     .second;
  runOnAllSensors( [this]( const DeVPSensor& sensor ) {
    // get the local to global transformation matrix and
    // store it in a flat float array of sixe 12.
    // Take care of the different convention between ROOT and LHCb for the reference point in volumes !
    Gaudi::Rotation3D     ltg_rot;
    Gaudi::TranslationXYZ ltg_trans;
    sensor.getGlobalMatrixDecomposition( ltg_rot, ltg_trans );
    assert( to_unsigned( sensor.sensorNumber() ) < m_ltg.size() );
    auto& ltg = m_ltg[to_unsigned( sensor.sensorNumber() )];
    ltg_rot.GetComponents( begin( ltg ) );
    ltg[9]  = ltg_trans.X();
    ltg[10] = ltg_trans.Y();
    ltg[11] = ltg_trans.Z();
    assert( to_unsigned( sensor.sensorNumber() ) < m_c2.size() );
    assert( to_unsigned( sensor.sensorNumber() ) < m_s2.size() );
    const auto vg                              = sensor.localToGlobal( Gaudi::XYZVector{ 1, 0, 0 } );
    m_c2[to_unsigned( sensor.sensorNumber() )] = vg.x() * vg.x(); // cos^2
    m_s2[to_unsigned( sensor.sensorNumber() )] = vg.y() * vg.y(); // sin^2
  } );
  return StatusCode::SUCCESS;
}

StatusCode DeVP::updateBeamSpot() {
  const double xRC = m_beamSpotCond->param<double>( "ResolPosRC" );
  const double xLA = m_beamSpotCond->param<double>( "ResolPosLA" );
  m_beamSpot.SetX( ( xRC + xLA ) / 2. );
  m_beamSpot.SetY( m_beamSpotCond->param<double>( "ResolPosY" ) );
  m_isVeloClosed = ( std::abs( xRC - m_beamSpot.x() ) < 5 * Gaudi::Units::mm &&
                     std::abs( xLA - m_beamSpot.x() ) < 5 * Gaudi::Units::mm );
  return StatusCode::SUCCESS;
}
