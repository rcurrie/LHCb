###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import pytest
from GaudiTesting import platform_matches
from LHCbTesting import LHCbExeTest
from LHCbTesting.preprocessors import LineSkipper


@pytest.mark.skipif(platform_matches(["dd4hep"]), reason="Unsupported platform")
class Test(LHCbExeTest):
    command = ["gaudirun.py", "../../scripts/velo_motion_from_yaml.py"]
    reference = "../../refs/velo_motion_from_yaml_pyconf.yaml"
    preprocessor = LHCbExeTest.preprocessor + LineSkipper(["|-velo_motion_system_yaml"])
