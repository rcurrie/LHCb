/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/** @file DeRichSystem.h
 *
 *  Header file for detector description class : DeRichSystem
 *
 *  @author Antonis Papanestis a.papanestis@rl.ac.uk
 *  @date   2006-01-26
 */

#pragma once

// STL
#include <algorithm>
#include <map>
#include <unordered_map>

// base class
#include "RichDet/DeRichBase.h"

// LHCbKernel
#include "Kernel/RichDetectorType.h"
#include "Kernel/RichSide.h"
#include "Kernel/RichSmartID.h"

// RichUtils
#include "RichUtils/RichDAQDefinitions.h"
#include "RichUtils/RichMap.h"

// local
#include "RichDet/DeRich.h"
#include "RichDet/DeRichPDPanel.h"
#include "RichDet/RichDetConfigType.h"
class DeRichPD;

// External declarations
extern const CLID CLID_DERichSystem;

/** @class DeRichSystem RichDet/DeRichSystem.h
 *
 * Class for generic info about the Rich system. In particular
 * to provide conversions and mappings between the various
 * RICH numbering schemes and to provide data on which PDs, Level0 and Level1
 * boards are currently active.
 *
 * @author Antonis Papanestis a.papanestis@rl.ac.uk
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date   27/01/2006
 */
class DeRichSystem : public DeRichBase {

public:
  /// Constructor for this class
  DeRichSystem( const std::string& name = "" ) : DeRichBase( name ) {}

  /// Default destructor
  virtual ~DeRichSystem() = default;

  /** Retrieves reference to class identifier
   *  @return the class identifier for this class
   */
  const CLID& clID() const override { return classID(); }

  /** Retrieves reference to class identifier
   *  @return the class identifier for this class
   */
  static const CLID& classID();

  /** This is where most of the geometry is read and variables initialised
   *  @return Status of initialisation
   *  @retval StatusCode::FAILURE Initialisation failed, program should
   *  terminate
   */
  StatusCode initialize() override;

public:
  /** Convert an PD RichSmartID into the corresponding PD hardware number
   *  @param smartID The RichSmartID for the PD
   *  @return The corresponding PD hardware ID
   */
  const Rich::DAQ::PDHardwareID hardwareID( const LHCb::RichSmartID& smartID ) const;

  /** Convert a RICH PD hardware number into the corresponding PD RichSmartID
   *  @param hID The hardware ID for the PD
   *  @return The corresponding PD RichSmartID
   */
  const LHCb::RichSmartID richSmartID( const Rich::DAQ::PDHardwareID& hID ) const;

  /** Convert a RICH Copy Number into the corresponding PD RichSmartID
   *  @param copyNumber The PD Copy Number
   *  @return The corresponding PD RichSmartID
   */
  const LHCb::RichSmartID richSmartID( const Rich::DAQ::PDCopyNumber& copyNumber ) const;

  /** Convert a RICH smartID to the corresponding PD Copy Number
   *  @param smartID The PD RichSmartID
   *  @return The corresponding PD Copy Number
   */
  const Rich::DAQ::PDCopyNumber copyNumber( const LHCb::RichSmartID& smartID ) const;

  /// Returns a list of all active PDs identified by their RichSmartID
  const LHCb::RichSmartID::Vector& activePDRichSmartIDs() const noexcept;

  /// Returns a list of all inactive PDs identified by their RichSmartID
  const LHCb::RichSmartID::Vector& inactivePDRichSmartIDs() const noexcept;

  /// Returns a list of all (active and inactive) PDs identified by their RichSmartID
  const LHCb::RichSmartID::Vector& allPDRichSmartIDs() const noexcept;

  /// Returns a list of all active PDs identified by their hardware IDs
  const Rich::DAQ::PDHardwareIDs& activePDHardwareIDs() const noexcept;

  /// Returns a list of all inactive PDs identified by their hardware IDs
  const Rich::DAQ::PDHardwareIDs& inactivePDHardwareIDs() const noexcept;

  /// Returns a list of all (active and inactive) PDs identified by their hardware IDs
  const Rich::DAQ::PDHardwareIDs& allPDHardwareIDs() const noexcept;

  /** Ask whether a given PD is currently active or dead
   *  @param id The RichSmartID for the PD
   *  @return boolean indicating if the given PD is active or not
   *  @attention For speed, this method does NOT check if the given PD id is a valid one
   *             or not. Invalid PD ids will return true
   */
  bool pdIsActive( const LHCb::RichSmartID& id ) const;

  /** Ask whether a given PD is currently active or dead
   *  @param id The hardware id for the PD
   *  @return boolean indicating if the given PD is active or not
   *  @attention For speed, this method does NOT check if the given PD id is a valid one
   *             or not. Invalid PD ids will return true
   */
  bool pdIsActive( const Rich::DAQ::PDHardwareID& id ) const;

  /// Returns the number of PDs in the given RICH detector
  unsigned int nPDs( const Rich::DetectorType rich ) const;

  /// Returns the number of PDs in the given RICH detector and side
  unsigned int nPDs( const Rich::DetectorType rich, const Rich::Side side ) const;

  /// Returns the total number of PDs
  unsigned int nPDs() const noexcept;

  /// Access the DeRichPanel for the given RICH and Panel
  inline const DeRichPDPanel* dePDPanel( const Rich::DetectorType rich, const Rich::Side side ) const {
    const auto depanel = deRich( rich )->pdPanel( side );
    assert( depanel );
    return depanel;
  }

  /// Access the DeRichPanel for the given PD ID
  inline const DeRichPDPanel* dePDPanel( const LHCb::RichSmartID pdID ) const {
    return dePDPanel( pdID.rich(), pdID.panel() );
  }

  /** Get the correct DeRichPD object for the given RichSmartID
   *  @param[in] hpdID The RichSmartID for the PD
   *  @return Pointer to the associated DeRichPD object
   */
  inline const DeRichPD* dePD( const LHCb::RichSmartID pdID ) const { return dePDPanel( pdID )->dePD( pdID ); }

  /// Is a 'large' PD
  inline bool isLargePD( const LHCb::RichSmartID pdID ) const { return dePDPanel( pdID )->isLargePD( pdID ); }

  /**
   * Retrieves the location of the PD/PMT in the detector store, so it can be
   * loaded using the getDet<DeRichPD> method.
   * @return The location of the PD in the detector store
   */
  std::string getDePDLocation( const LHCb::RichSmartID& smartID ) const;

  /// The photon detector type
  inline Rich::RichPhDetConfigType RichPhotoDetConfig() const noexcept { return m_photDetConf; }

  /// Access on the Detector Elements for Rich1 and Rich2
  inline DeRich* deRich( const Rich::DetectorType rich ) const {
    const auto derich = m_deRich[rich];
    assert( derich );
    return derich;
  }

  /// Access on the RICH Detector Elements for given radiator
  inline DeRich* deRich( const Rich::RadiatorType rad ) const noexcept {
    return ( Rich::Rich2Gas == rad ? deRich( Rich::Rich2 ) : deRich( Rich::Rich1 ) );
  }

private:
  // definitions

  /// Map type to use.
  template <typename TO, typename FROM>
  using MyMap = std::unordered_map<TO, FROM>;

private:
  // methods

  /// Update methods for PD mappings
  StatusCode buildPDMappings();

  /// Fill the maps for the given RICH detector
  StatusCode fillMaps( const Rich::DetectorType rich );

  /// Save information to a map, checking first it is not already set
  template <class SOURCE, class TARGET, class MAP>
  inline bool safeMapFill( const SOURCE& source, const TARGET& target, MAP& map ) {
    auto p = map.insert( typename MAP::value_type( source, target ) );
    if ( !p.second ) {
      error() << "Error filling map '" << System::typeinfoName( typeid( map ) ) << "' source "
              << System::typeinfoName( typeid( source ) ) << "=" << source << " already has an entry for target "
              << System::typeinfoName( typeid( target ) ) << " OLD=" << map[source] << " NEW=" << target << endmsg;
    }
    return p.second;
  }

  /// setup photon detector configuration
  void setupPhotDetConf() noexcept;

  /// The version of RichSystem
  inline int systemVersion() const noexcept {
    return ( exists( "systemVersion" ) ? param<int>( "systemVersion" ) : 0 );
  }

private:
  // data

  /// RICH PhotoDetector Configuration (assume HPD by default)
  Rich::RichPhDetConfigType m_photDetConf = Rich::HPDConfig;

  /// mapping from RichSmartID to Rich::DAQ::PDHardwareID
  MyMap<const LHCb::RichSmartID, Rich::DAQ::PDHardwareID> m_soft2hard;

  /// mapping from Rich::DAQ::PDHardwareID to RichSmartID
  MyMap<const Rich::DAQ::PDHardwareID, LHCb::RichSmartID> m_hard2soft;

  /// List of all active PD RichSmartIDs
  LHCb::RichSmartID::Vector m_activePDSmartIDs;

  /// List of all inactive PD RichSmartIDs
  LHCb::RichSmartID::Vector m_inactivePDSmartIDs;

  /// List of all PD RichSmartIDs
  LHCb::RichSmartID::Vector m_allPDSmartIDs;

  /// List of all active PD hardware IDs
  Rich::DAQ::PDHardwareIDs m_activePDHardIDs;

  /// List of all inactive PD hardware IDs
  Rich::DAQ::PDHardwareIDs m_inactivePDHardIDs;

  /// List of all PD hardware IDs
  Rich::DAQ::PDHardwareIDs m_allPDHardIDs;

  /// smartID to copy number map
  MyMap<const LHCb::RichSmartID, Rich::DAQ::PDCopyNumber> m_smartid2copyNumber;

  /// copy number to smartID map
  MyMap<const Rich::DAQ::PDCopyNumber, LHCb::RichSmartID> m_copyNumber2smartid;

  /// Rich1 & Rich2 detector elements
  Rich::DetectorArray<DeRich*> m_deRich = { {} };
};

//=========================================================================
// activePDRichSmartIDs
//=========================================================================
inline const LHCb::RichSmartID::Vector& DeRichSystem::activePDRichSmartIDs() const noexcept {
  return m_activePDSmartIDs;
}

//=========================================================================
// inactivePDRichSmartIDs
//=========================================================================
inline const LHCb::RichSmartID::Vector& DeRichSystem::inactivePDRichSmartIDs() const noexcept {
  return m_inactivePDSmartIDs;
}

//=========================================================================
// allPDRichSmartIDs
//=========================================================================
inline const LHCb::RichSmartID::Vector& DeRichSystem::allPDRichSmartIDs() const noexcept { return m_allPDSmartIDs; }

//=========================================================================
// activePDHardwareIDs
//=========================================================================
inline const Rich::DAQ::PDHardwareIDs& DeRichSystem::activePDHardwareIDs() const noexcept { return m_activePDHardIDs; }

//=========================================================================
// activePDHardwareIDs
//=========================================================================
inline const Rich::DAQ::PDHardwareIDs& DeRichSystem::inactivePDHardwareIDs() const noexcept {
  return m_inactivePDHardIDs;
}

//=========================================================================
// allPDHardwareIDs
//=========================================================================
inline const Rich::DAQ::PDHardwareIDs& DeRichSystem::allPDHardwareIDs() const noexcept { return m_allPDHardIDs; }

//=========================================================================
// pdIsActive
//=========================================================================
inline bool DeRichSystem::pdIsActive( const LHCb::RichSmartID& id ) const {
  const auto& c = m_inactivePDSmartIDs;
  return ( c.empty() || std::find( c.begin(), c.end(), id.pdID() ) == c.end() );
}

//=========================================================================
// pdIsActive
//=========================================================================
inline bool DeRichSystem::pdIsActive( const Rich::DAQ::PDHardwareID& id ) const {
  const auto& c = m_inactivePDHardIDs;
  return ( c.empty() || std::find( c.begin(), c.end(), id ) == c.end() );
}

//=========================================================================
// Number HPDs
//=========================================================================
inline unsigned int DeRichSystem::nPDs() const noexcept { return allPDRichSmartIDs().size(); }

//=========================================================================
// PD per RICH
//=========================================================================
inline unsigned int DeRichSystem::nPDs( const Rich::DetectorType rich ) const {
  return std::count_if( allPDRichSmartIDs().begin(), allPDRichSmartIDs().end(),
                        [&rich]( const auto& PD ) { return PD.rich() == rich; } );
}

//=========================================================================
// PD per RICH and side
//=========================================================================
inline unsigned int DeRichSystem::nPDs( const Rich::DetectorType rich, const Rich::Side side ) const {
  return std::count_if( allPDRichSmartIDs().begin(), allPDRichSmartIDs().end(),
                        [&rich, &side]( const auto& PD ) { return ( PD.rich() == rich && PD.panel() == side ); } );
}
