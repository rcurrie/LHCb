2022-12-19 LHCb v54r3
===

This version uses
Detector [v1r7](../../../../Detector/-/tags/v1r7),
Gaudi [v36r9](../../../../Gaudi/-/tags/v36r9) and
LCG [101a_LHCB_7](http://lcginfo.cern.ch/release/101a_LHCB_7/) with ROOT 6.24.08.

This version is released on `master` branch.
Built relative to LHCb [v54r2](/../../tags/v54r2), with the following changes:

### New features ~"new feature"


### Fixes ~"bug fix" ~workaround

- ~VP | Fix DeVP::module to return module DetElem instead of ladder, !3900 (@tlatham)
- ~Core ~Conditions | Fix use of DD4hep condition derivation to correctly propagate IOVs, !3901 (@clemenci)
- Remove fixed bank location name, !3896 (@sesen)


### Enhancements ~enhancement



### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Calo | Fix calo index bookkeeping, !3884 (@mveghel)
- Fixes needed to compile with gcc 12, !3875 (@clemenci)


### Documentation ~Documentation


### Other

- ~Configuration | Propagate conddbtag as conditionsversion to DDDBConf in pyconf, !3653 (@nnolte) [#234]
- ~Configuration | Do not require dddb_tag to be set in dd4hep builds, !3895 (@jonrob)
- ~RICH | RICH - Add current refractive index scale factor accessors, !3889 (@jonrob)
- ~RICH ~"MC checking" ~Monitoring | RichHistoBase - Expand size of ID and title fields in histogram printout, !3881 (@jonrob)
- ~RICH ~Conditions | RichDetectors - Simplify derived condition configuration, !3899 (@jonrob)
- ~Persistency | Make all writers and decoders to use RawBank::View, !3873 (@sesen) [#179]
- Workaround for Moore#501, !3906 (@rmatev) [Moore#501]
- Add missing include needed when building for centos9, !3905 (@clemenci)
- Add support for registering lumi summary schemata with the metadata repo, !3904 (@dcraik)
- Small improvements on Phoenix Sink, !3897 (@sponce)
- Default_raw_event location, !3893 (@sesen)
- Improved error reporting in HLTScheduler when an algo fails, !3892 (@sponce)
- Fix bug in lumi summary decoding (sizes and offsets of counters were swapped), !3887 (@dcraik)
- Remove unused header file, !3880 (@dcraik)
- Set ApproxEventsPerBasket to 1 for FSRs, !3871 (@cburr)
- Add odin producer unconditionally to list of producers, !3861 (@sstahl)
- Add more generic forwarding functions, !3885 (@graven)
