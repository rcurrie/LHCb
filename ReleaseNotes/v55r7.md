2024-04-19 LHCb v55r7
===

This version uses
Gaudi [v38r1](../../../../Gaudi/-/tags/v38r1),
Detector [v1r31](../../../../Detector/-/tags/v1r31) and
LCG [105a](http://lcginfo.cern.ch/release/105a/) with ROOT 6.30.04.

This version is released on the `2024-patches` branch.
Built relative to LHCb [v55r6](/../../tags/v55r6), with the following changes:

### New features ~"new feature"

- ~Core ~Utilities | Error event handling, !3883 (@rmatev)


### Fixes ~"bug fix" ~workaround

- Protect FT decoding from misordered links, !4513 (@lohenry)


### Enhancements ~enhancement

- ~Persistency | TAE handling for EvtStoreSvc, !4530 (@rmatev)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- Fix ODIN::CalibrationTypes and clean up, !4514 (@rmatev)
- Remove unused headers from Particle.h, !4502 (@graven)


### Documentation ~Documentation


### Other

- Follows https://gitlab.cern.ch/lhcb/Detector/-/merge_requests/418 and related, !4517 (@samarian)
- Packer/unpacker cleanup / consolidation, !4463 (@graven)
