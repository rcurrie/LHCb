/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloFutureUtils/CaloMomentum.h"
#include "CaloFutureUtils/Kinematics.h"
#include "Event/CaloCluster.h"
#include "Event/CaloHypo.h"
#include "Event/ProtoParticle.h"
#include "Event/VertexBase.h"
#include <utility>
#include <vector>
// ============================================================================
/** @file
 *  Implementation file for class LHCb::CaloMomentum
 *  @date 2006-08-08
 *  @author Olivier Deschamps
 */
// ============================================================================

// ============================================================================
// From CaloPosition + reference point (null covariance)
// ============================================================================
LHCb::Calo::Momentum::Momentum( const LHCb::CaloPosition* calo, LHCb::Calo::Momentum::Point point )
    : m_point( std::move( point ) ) {
  addCaloPosition( calo );
  addToFlag( LHCb::Calo::Momentum::NewReferencePoint );
}
// ============================================================================
// From CaloPosition + reference point + covariance
// ============================================================================
LHCb::Calo::Momentum::Momentum( const LHCb::CaloPosition* calo, LHCb::Calo::Momentum::Point point,
                                const LHCb::Calo::Momentum::PointCovariance& cov )
    : m_point( std::move( point ) ), m_pointCovMatrix( cov ) {
  addCaloPosition( calo );
  addToFlag( LHCb::Calo::Momentum::NewReferencePoint );
  addToFlag( LHCb::Calo::Momentum::NewPointCovariance );
}
// ============================================================================
// From ProtoParticle +  XYZPoint
// ============================================================================
LHCb::Calo::Momentum::Momentum( const LHCb::ProtoParticle* proto, LHCb::Calo::Momentum::Point point )
    : m_point( std::move( point ) ) {
  addCaloPosition( proto );
  addToFlag( LHCb::Calo::Momentum::NewReferencePoint );
}
// ============================================================================
// From (ProtoParticle, XYZPoint, SymMatrix3x3)
// ============================================================================
LHCb::Calo::Momentum::Momentum( const LHCb::ProtoParticle* proto, LHCb::Calo::Momentum::Point point,
                                const LHCb::Calo::Momentum::PointCovariance& cov )
    : m_point( std::move( point ) ), m_pointCovMatrix( cov ) {
  addCaloPosition( proto );
  addToFlag( LHCb::Calo::Momentum::NewReferencePoint );
  addToFlag( LHCb::Calo::Momentum::NewPointCovariance );
}

// vector of CaloHypos (useful to manage bremList from BremAdder tool)
LHCb::Calo::Momentum::Momentum( const std::vector<const LHCb::CaloHypo*> hypos ) {
  std::for_each( hypos.begin(), hypos.end(), [&]( const CaloHypo* h ) { addCaloPosition( h ); } );
}

// ============================================================================
// From (CaloHypo, XYZPoint)
// ============================================================================
LHCb::Calo::Momentum::Momentum( const LHCb::CaloHypo* hypo, LHCb::Calo::Momentum::Point point )
    : m_point( std::move( point ) ) {
  addCaloPosition( hypo );
  addToFlag( LHCb::Calo::Momentum::NewReferencePoint );
}
// ============================================================================
// From (CaloHypo,XYZPoint, SymMatrix3x3)
// ============================================================================
LHCb::Calo::Momentum::Momentum( const LHCb::CaloHypo* hypo, LHCb::Calo::Momentum::Point point,
                                const LHCb::Calo::Momentum::PointCovariance& cov )
    : m_point( std::move( point ) ), m_pointCovMatrix( cov ) {
  addCaloPosition( hypo );
  addToFlag( LHCb::Calo::Momentum::NewReferencePoint );
  addToFlag( LHCb::Calo::Momentum::NewPointCovariance );
}
// ============================================================================
// CaloPosition
// ============================================================================
void LHCb::Calo::Momentum::addCaloPosition( const LHCb::CaloPosition* calopos ) {
  m_flag |= LHCb::Calo::Momentum::FromCaloPos;
  if ( calopos ) {
    m_caloPositions.push_back( *calopos );
  } else {
    m_status |= LHCb::Calo::Momentum::NullCaloPos;
  }
}
// ============================================================================
// CaloCluster
// ============================================================================
void LHCb::Calo::Momentum::addCaloPosition( const LHCb::CaloCluster* cluster ) {
  m_flag |= LHCb::Calo::Momentum::FromCaloCluster;
  if ( cluster ) {
    m_caloPositions.push_back( cluster->position() );
  } else {
    m_status |= LHCb::Calo::Momentum::NullCaloCluster;
  }
}
// ============================================================================
// CaloHypo
// ============================================================================
void LHCb::Calo::Momentum::addCaloPosition( const LHCb::CaloHypo* hypo ) {
  m_flag |= LHCb::Calo::Momentum::FromCaloHypo;

  if ( !hypo ) {
    m_status |= LHCb::Calo::Momentum::NullCaloHypo;
    return;
  }

  // Check if the hypo has referenced hypos
  const SmartRefVector<LHCb::CaloHypo>& hypos = hypo->hypos();

  if ( hypos.empty() ) {
    // if no : photon, photonFromMerged, ...
    const LHCb::CaloPosition* calopos = hypo->position();
    addCaloPosition( calopos );
    m_caloHypos.push_back( hypo );
  } else {
    // if yes : mergedPi0
    for ( const auto& hypo : hypos ) {
      m_flag |= LHCb::Calo::Momentum::FromCaloHypo;
      const LHCb::CaloPosition* calopos = hypo->position();
      addCaloPosition( calopos );
      m_caloHypos.push_back( hypo );
    }
  }
}
// ============================================================================
// ProtoParticle
// ============================================================================
void LHCb::Calo::Momentum::addCaloPosition( const LHCb::ProtoParticle* proto ) {
  m_flag |= LHCb::Calo::Momentum::FromProtoPart;

  if ( !proto ) {
    m_status |= LHCb::Calo::Momentum::NullProtoPart;
    return;
  }

  if ( 0 != proto->charge() ) {
    m_status |= LHCb::Calo::Momentum::ChargedProtoPart;
    return;
  }
  const SmartRefVector<LHCb::CaloHypo>& hypos = proto->calo();

  if ( hypos.empty() ) {
    m_status |= LHCb::Calo::Momentum::EmptyHypoRef;
  } else if ( 1 == hypos.size() ) {
    // Each ProtoPart derive from a single CaloHypo so far
    auto ihypo = hypos.begin();
    addCaloPosition( *ihypo );
  } else {
    // If not the case the special configuration should be implemented here
    m_status |= LHCb::Calo::Momentum::MultipleHypoRef;
  }
}
// ============================================================================
/// evaluate the parameters
// ============================================================================
bool LHCb::Calo::Momentum::evaluate( int param ) {
  if ( LHCb::Calo::Momentum::OK != m_status ) { return false; }

  // adapted from PhotonParam.h (thanks to Vanya Belyaev)

  Gaudi::LorentzVector p4;

  LHCb::Calo::Momentum::MomCovariance      C11;                    // 4-moment 4x4 covariance matrix
  LHCb::Calo::Momentum::PointCovariance    C22 = m_pointCovMatrix; // 3-point  3x3 covariance matrix
  LHCb::Calo::Momentum::MomPointCovariance C12;                    // momentum-point 4x3 off-diagonal block

  for ( LHCb::CaloPosition calo : m_caloPositions ) {

    // Evaluate the 4-momentum
    if ( 0 != ( static_cast<unsigned>( LHCb::Calo::Momentum::Parameter::Momentum ) & param ) ) {
      if ( calo.e() >= 0. ) p4 += CaloFuture::Kinematics::momentum( calo, m_point );
    }

    // Evaluate everithing the covariance
    if ( 0 != ( static_cast<unsigned>( LHCb::Calo::Momentum::Parameter::Covariance ) & param ) ) {

      //  vector of "old" parameters
      //    vold( 1 ) = pos   -> x () ;   // x-position in Calorimeter
      //    vold( 2 ) = pos   -> y () ;   // y-position in Calorimeter
      //    vold( 3 ) = pos   -> e () ;   // energy     in Calorimeter
      //    vold( 4 ) = point .  x () ;   // x-position at production vertex
      //    vold( 5 ) = point .  y () ;   // y-position at production vertex
      //    vold( 6 ) = point .  z () ;   // z-position at production vertex

      // "old" covariance matrix has the block structure
      //     --------------
      //     |  S11 | S12 |
      //     --------------
      //     |  S21 | S22 |
      //     --------------
      // Where S11 is 3x3 covariance matrix of E  , xc and yc
      //   and S22 is 3x3 covariance matrix of xv , yv and zv
      //   matrices S12 and S21 both are equal to null matrix
      //
      const LHCb::CaloPosition::Covariance&        S11 = calo.covariance(); // (X,Y,E)
      const LHCb::Calo::Momentum::PointCovariance& S22 = C22;               // (x,y,z)

      //    vnew ( 1 ) = px ;   // x-component of photon momentum
      //    vnew ( 2 ) = py ;   // y-component of photon momentum
      //    vnew ( 3 ) = pz ;   // z-component of photon momentum
      //    vnew ( 4 ) = e  ;   // photon energy
      //    vnew ( 5 ) = x0 ;   // x-component of photon production point
      //    vnew ( 6 ) = y0 ;   // y-component of photon production point
      //    vnew ( 7 ) = z0 ;   // z-component of photon production point

      //  evaluate the "transport matrix"

      //
      //  The transport matrix F = d(vnew) / d(vold)    (7x6)
      //
      //   has block structure
      //        --------------
      //        | F11 |  F12 |
      //        --------------
      //        | F21 |  F22 |
      //        --------------
      //  Where F11 = d( px , py , pz , e) / d ( xc , yc , e  )   4x3 matrix
      //        F12 = d( px , py , pz , e) / d ( xv , yv , zv )   4x3 matrix
      //        F21 = d( x0 , y0 , z0    ) / d ( xc , yc , e  )   3x3 matrix
      //        F22 = d( x0 , y0 , z0    ) / d ( xv , yv , zv )   3x3 matrix
      //
      //  From definition of parameters it is obvious that
      //  F21 is null matrix ans F22 is unit matrix

      LHCb::Calo::Momentum::MomPointCovariance F11;
      LHCb::Calo::Momentum::MomPointCovariance F12;

      // ok, now all blocks of transport matrix are
      // known and we could evaluate the covariance matrix
      // of new parameters

      // The covariance matrix of new parameters has block structure:
      //     -------------
      //     | C11 | C12 |
      //     -------------
      //     | C21 | C22 |
      //     -------------
      //
      // C = F * S * F^T
      //
      // Where:
      //
      // C11 = F11*S11*F11^T + F12*S21*F11^T + F11*S12*F12^T + F12*S22*F12^T
      // C12 = F11*S11*F21^T + F12*S21*F21^T + F11*S12*F22^T + F12*S22*F22^T
      // C21 = F21*S11*F11^T + F22*S21*F11^T + F21*S12*F12^T + F22*S22*F12^T
      // C22 = F21*S11*F21^T + F22*S21*F21^T + F21*S12*F22^T + F22*S22*F22^T
      //
      // taking into account that S12 = 0 , S21 = 0 , F21 = 0 , F22 = 1 on gets
      //
      // C11 = F11 * S11 * F11^T + F12 * S22 * F12^T
      // C12 = F12 * S22 * F22^T                     = F12 * S22
      // C21 = F22 * S22 * F12^T                     = S22 * F12^T
      // C22 = F22 * S22 * F22^T                     = S22

      // C11 += S11.similarity( F11 ) + S22.similarity( F12 ) ;

      // OK, get all jacobians:
      CaloFuture::Kinematics::jacobians( calo, m_point, F11, F12 );

      // -----------------------
      // Covariance are additive
      // Assume the various CaloPositions are NOT correlated
      // THIS IS NOT THE CASE FOR MERGED PI0
      // @ToDo : adapt to the case of correlated CaloPositions

      C11 += Similarity( F11, S11 ) + Similarity( F12, S22 );
      C12 += F12 * S22;
    }
  }

  if ( 0 != ( static_cast<unsigned>( LHCb::Calo::Momentum::Parameter::Momentum ) & param ) ) {
    m_momentum = p4;
    m_flag |= LHCb::Calo::Momentum::MomentumEvaluated;
  }

  if ( 0 != ( static_cast<unsigned>( LHCb::Calo::Momentum::Parameter::Covariance ) & param ) ) {
    m_momCovMatrix      = C11;
    m_momPointCovMatrix = C12;
    m_flag |= LHCb::Calo::Momentum::CovarianceEvaluated;
  }
  return true;
}
// ===========================================================================
// get the momentum
// ===========================================================================
const LHCb::Calo::Momentum::Vector& LHCb::Calo::Momentum::momentum() {
  evaluate( static_cast<int>( LHCb::Calo::Momentum::Parameter::Momentum ) );
  return m_momentum;
}
// ===========================================================================
// get the covariance
// ===========================================================================
const LHCb::Calo::Momentum::MomCovariance& LHCb::Calo::Momentum::momCovMatrix() {
  evaluate( static_cast<int>( LHCb::Calo::Momentum::Parameter::Covariance ) );
  return m_momCovMatrix;
}
// ===========================================================================
// get the covariance
// ===========================================================================
const LHCb::Calo::Momentum::MomPointCovariance& LHCb::Calo::Momentum::momPointCovMatrix() {
  evaluate( static_cast<int>( LHCb::Calo::Momentum::Parameter::Covariance ) );
  return m_momPointCovMatrix;
}
// =============================================================================
// reference point
// =============================================================================
void LHCb::Calo::Momentum::setReferencePoint( const LHCb::VertexBase* vertex ) {
  m_point          = vertex->position();
  m_pointCovMatrix = vertex->covMatrix();
  addToFlag( LHCb::Calo::Momentum::NewReferencePoint );
  addToFlag( LHCb::Calo::Momentum::NewPointCovariance );
}
