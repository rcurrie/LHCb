###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from LHCbTesting import LHCbExeTest


class Test(LHCbExeTest):
    command = ["echo", ""]

    def test_000_import(self):
        import GaudiConfUtils.ConfigurableGenerators
        from GaudiConfUtils.ConfigurableGenerators import ApplicationMgr

    def test_010_generate(self):
        from Gaudi.Configuration import allConfigurables

        from GaudiConfUtils.ConfigurableGenerators import MessageSvc

        assert "MessageSvc" not in allConfigurables

        gen = MessageSvc(OutputLevel=1)
        ms = gen.configurable("MessageSvc")

        assert "MessageSvc" in allConfigurables
        assert ms.OutputLevel == gen.OutputLevel

        from GaudiKernel.Configurable import purge

        purge()

    def test_020_properties(self):
        from Gaudi.Configuration import allConfigurables

        from GaudiConfUtils.ConfigurableGenerators import MessageSvc

        assert "MessageSvc" not in allConfigurables

        try:
            MessageSvc.NoPropName = "test"
            raise AssertionError("No exception raised assigning to class")
        except AttributeError:
            pass
        except AssertionError:
            raise
        except Exception as x:
            raise Exception("wrong exception raised assigning to class: %s" % x)

        try:
            MessageSvc.OutputLevel = 3
            raise AssertionError("No exception raised assigning to class")
        except AttributeError:
            pass
        except AssertionError:
            raise
        except Exception as x:
            raise Exception("wrong exception raised assigning to class: %s" % x)

        gen = MessageSvc()

        try:
            gen.NoPropName = "test"
            raise AssertionError("No exception raised assigning to wrong property")
        except AttributeError:
            pass
        except AssertionError:
            raise
        except Exception as x:
            raise Exception(
                "wrong exception raised assigning to wrong property: %s" % x
            )

        gen.OutputLevel = 5

        ms = gen("MessageSvc")

        assert "MessageSvc" in allConfigurables
        assert ms.OutputLevel == gen.OutputLevel

        from GaudiKernel.Configurable import purge

        purge()

    def test_020_repr(self):
        from GaudiConfUtils.ConfigurableGenerators import MessageSvc

        gen = MessageSvc(OutputLevel=7)
        assert gen == eval(repr(gen))
