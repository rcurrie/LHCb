###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import argparse
import os

from Configurables import (
    ApplicationMgr,
    ConfigCDBAccessSvc,
    DeterministicPrescaler,
    GaudiSequencer,
    HltGenConfig,
)
from DDDB.CheckDD4Hep import UseDD4Hep
from GaudiPython.Bindings import AppMgr

from PyConf.application import ApplicationOptions, configure_input

parser = argparse.ArgumentParser(usage="usage: %(prog)s prescale")

parser.add_argument("prescale", type=float, help="what prescale")
parser.add_argument(
    "--clear", action="store_true", help="remove TCK db before creating the TCK"
)

args = parser.parse_args()
scale = float(args.prescale)

# Location of TCK database
filename = os.path.join("TCKData", "config.cdb")
if not os.path.exists("TCKData"):
    os.makedirs("TCKData")
elif args.clear and os.path.exists(filename):
    # remove the target file if requested
    os.remove(filename)

options = ApplicationOptions(_enabled=False)
options.data_type = "2016"
options.simulation = False
options.dddb_tag = "dddb-20200424-3"
options.conddb_tag = "cond-20191004-1"
options.input_type = "None"
config = configure_input(options)

if not UseDD4Hep:
    from Configurables import CondDB

    CondDB().LatestGlobalTagByDataTypes = [options.data_type]

# TCK access service
accessSvc = ConfigCDBAccessSvc(File=filename, Mode="ReadWrite")

# Sequence, actually only a prescaler
seq = GaudiSequencer("TestSequence")
prescaler = DeterministicPrescaler(
    "TestScaler", SeedName="TestScaler", AcceptFraction=scale
)
seq.Members = [prescaler]

# Algorithm to generate the TCK
gen = HltGenConfig(
    ConfigTop=[seq.getName()],
    ConfigSvc=["ToolSvc"],
    ConfigAccessSvc=accessSvc.getName(),
    HltType="LHCb_Test",
    MooreRelease="v1r0",
    Label="Test",
)

# make sure gen is the very first Top algorithm
ApplicationMgr().TopAlg = [gen, seq]

# Instantiate AppMgr and run some events
gaudi = AppMgr()
TES = gaudi.evtSvc()

gaudi.initialize()
gaudi.start()
gaudi.stop()
gaudi.finalize()
gaudi.exit()

print("PASSED")
