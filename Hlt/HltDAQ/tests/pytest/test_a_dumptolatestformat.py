###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import pytest
from LHCbTesting import LHCbExeTest


@pytest.mark.ctest_fixture_setup("hltdaq.a_dumptolatestformat")
@pytest.mark.shared_cwd("HltDAQ")
class Test(LHCbExeTest):
    """
    Author: rlambert
    Purpose: to check if bank encoder changes are binary compatible
    """

    command = ["gaudirun.py", "../options/dump_decode_wipe_encode_split.py"]
    reference = {"messages_count": {"FATAL": 0, "ERROR": 4, "WARNING": 28}}
