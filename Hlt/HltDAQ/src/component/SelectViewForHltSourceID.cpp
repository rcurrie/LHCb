/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/RawEvent.h"
#include "HltSourceID.h"
#include "LHCbAlgs/Consumer.h"
#include "LHCbAlgs/Transformer.h"
#include <algorithm>
#include <vector>

namespace LHCb {

  class SelectViewForHltSourceID : public Algorithm::Transformer<RawBank::View( RawBank::View const& )> {
    Gaudi::Property<LHCb::Hlt::DAQ::SourceID>      m_source_id{ this, "SourceID", LHCb::Hlt::DAQ::SourceID::Hlt1 };
    mutable Gaudi::Accumulators::BinomialCounter<> m_selected_banks{ this, "Number of selected banks" };

  public:
    SelectViewForHltSourceID( const std::string& name, ISvcLocator* locator )
        : Transformer( name, locator, { "Input", {} }, { "Output", {} } ) {}
    RawBank::View operator()( RawBank::View const& input_view ) const override {
      int offset = -1;
      int count  = 0;
      for ( unsigned int i = 0; i < input_view.size(); i++ ) {
        auto hlt_source_id = LHCb::Hlt::DAQ::getSourceID( *input_view[i] );
        // We assume sorting here
        if ( hlt_source_id == m_source_id ) {
          if ( offset == -1 ) { offset = i; }
          ++count;
        }
      }
      m_selected_banks += count;
      auto selection = ( offset >= 0 ) ? input_view.subspan( offset, count ) : RawBank::View{};
      return selection;
    }
  };

  DECLARE_COMPONENT( SelectViewForHltSourceID )

} // namespace LHCb
