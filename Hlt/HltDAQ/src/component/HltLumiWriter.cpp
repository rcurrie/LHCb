/*****************************************************************************\
* (c) Copyright 2000-2024 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/HltLumiSummary.h"
#include "Event/RawEvent.h"
#include "HltSourceID.h"
#include "Kernel/IIndexedLumiSchemaSvc.h"
#include "Kernel/STLExtensions.h"
#include "LHCbAlgs/Transformer.h"

//-----------------------------------------------------------------------------
// Implementation file for class : HltLumiWriter
//
//-----------------------------------------------------------------------------

/** @class HltLumiWriter HltLumiWriter.h
 *  Fills the Raw Buffer banks for the LumiSummary
 */

namespace LHCb::Hlt::DAQ {

  class HltLumiWriter : public LHCb::Algorithm::MultiTransformer<std::tuple<LHCb::RawEvent, LHCb::RawBank::View>(
                                                                     const LHCb::HltLumiSummary& ),
                                                                 LHCb::Algorithm::Traits::writeOnly<LHCb::RawEvent>> {

    /// SourceID to insert in the bank header
    Gaudi::Property<SourceID> m_sourceID{ this, "SourceID", SourceID::Dummy };
    Gaudi::Property<unsigned> m_encodingKey{ this, "EncodingKey", 0,
                                             "if non-zero, ignore encodingKey in LumiSummary, and use this value" };

    mutable Gaudi::Accumulators::AveragingCounter<> m_totDataSize{ this, "Average event size / 32-bit words" };

  public:
    HltLumiWriter( const std::string& name, ISvcLocator* pSvcLocator )
        : MultiTransformer{ name,
                            pSvcLocator,
                            KeyValue{ "InputBank", LHCb::HltLumiSummaryLocation::Default },
                            { KeyValue{ "OutputRawEvent", "/Event/DAQ/HltLumiEvent" },
                              KeyValue{ "OutputView", "/Event/DAQ/HltLumi/View" } } } {};

    StatusCode initialize() override {
      return MultiTransformer::initialize().andThen( [&] {
        if ( m_sourceID != SourceID::Hlt1 && m_sourceID != SourceID::Hlt2 ) {
          fatal() << "SourceID must be either Hlt1 or Hlt2" << endmsg;
          return StatusCode::FAILURE;
        }
        return StatusCode::SUCCESS;
      } );
    };

    //=============================================================================
    // Main execution
    //  Fill the data bank, structure depends on encoding key
    //=============================================================================
    std::tuple<LHCb::RawEvent, LHCb::RawBank::View> operator()( const LHCb::HltLumiSummary& summary ) const override {
      unsigned int encodingKey = m_encodingKey;
      if ( auto it = summary.extraInfo().find( "encodingKey" ); it != summary.extraInfo().end() ) {
        if ( m_encodingKey == 0u ) {
          encodingKey = it->second;
        } else {
          warning() << "Existing encoding key " << it->second << " will be ignored" << std::endl;
        }
      } else {
        if ( m_encodingKey == 0u ) {
          throw GaudiException( "EncodingKey property must be specified", __PRETTY_FUNCTION__, StatusCode::FAILURE );
        }
      }

      auto lumi_schema = m_svc->lumiCounters( encodingKey, 0 );

      std::vector<unsigned int> bank;
      // size->bank.size: 0 -> 0, 1 -> 1, ..., 4 -> 1, 5 -> 2, 6 -> 2, 7 -> 2, 8 -> 2, 9 -> 3, ...
      bank.resize( ( lumi_schema.size + 3 ) / 4, 0 );
      for ( auto cntr : lumi_schema.counters ) {
        auto val = 0.;
        if ( cntr.name == "encodingKey" ) {
          val = encodingKey;
        } else if ( summary.extraInfo().find( cntr.name ) != summary.extraInfo().end() ) {
          val = summary.extraInfo().at( cntr.name );
        } else {
          warning() << "Field " << cntr.name << " missing but expected by lumiSummary schema " << encodingKey << endmsg;
          continue;
        }

        auto shift = cntr.shift;
        auto scale = cntr.scale;

        auto scaled_val = static_cast<unsigned>( std::round( shift + scale * val ) );
        writeCounter( cntr.offset, cntr.size, &bank.front(), scaled_val );
      }

      LHCb::RawEvent rawEvent;
      // set source, type, version
      uint16_t sourceIDCommon =
          shift<SourceIDMasks::OnlineReserved>( subSystemBits ) | shift<SourceIDMasks::Process>( m_sourceID.value() );
      rawEvent.addBank( sourceIDCommon, LHCb::RawBank::HltLumiSummary, 2, bank );

      m_totDataSize += bank.size();

      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << "Bank size: " << format( "%4d ", bank.size() ) << "Total Data bank size " << bank.size() << endmsg;
      }

      if ( msgLevel( MSG::VERBOSE ) ) {
        verbose() << "DATA bank : " << endmsg;
        for ( const auto& [k, w] : LHCb::range::enumerate( bank, 1 ) ) {
          verbose() << format( " %8x %11d   ", w, w );
          if ( k % 4 == 0 ) verbose() << endmsg;
        }
        verbose() << endmsg;
      }
      auto view = rawEvent.banks( LHCb::RawBank::HltLumiSummary );

      // without std::move here the RawEvent gets copied which would invalidate the view
      // View creation must be after RawEvent is made
      return { std::move( rawEvent ), std::move( view ) };
    }

    void writeCounter( unsigned offset, unsigned size, unsigned* target, unsigned value ) const {
      // Check value fits within size bits
      if ( size < ( 8 * sizeof( unsigned ) ) && value >= ( 1u << size ) ) { return; }

      // Separate offset into a word part and bit part
      unsigned word      = offset / ( 8 * sizeof( unsigned ) );
      unsigned bitoffset = offset % ( 8 * sizeof( unsigned ) );

      // Check size and offset line up with word boundaries
      if ( bitoffset + size > ( 8 * sizeof( unsigned ) ) ) { return; }

      // Apply the value to the matching bits
      unsigned mask = ( ( 1ul << size ) - 1 ) << bitoffset;
      target[word]  = ( target[word] & ~mask ) | ( ( value << bitoffset ) & mask );
    }

  private:
    ServiceHandle<IIndexedLumiSchemaSvc> m_svc{ this, "DecoderMapping", "HltANNSvc" };
  };

  DECLARE_COMPONENT_WITH_ID( HltLumiWriter, "HltLumiWriter" )

} // namespace LHCb::Hlt::DAQ
