/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <algorithm>
#include <array>
#include <stdexcept>
#include <string>
#include <string_view>
#include <vector>

// types of CompositeNodes, extendable if needed
enum class nodeType { LAZY_AND, NONLAZY_OR, NONLAZY_AND, LAZY_OR, NOT };

inline std::string_view toString( nodeType nt ) {
  using namespace std::literals::string_view_literals;
  switch ( nt ) {
  case nodeType::LAZY_AND:
    return "LAZY_AND"sv;
  case nodeType::NONLAZY_OR:
    return "NONLAZY_OR"sv;
  case nodeType::NONLAZY_AND:
    return "NONLAZY_AND"sv;
  case nodeType::LAZY_OR:
    return "LAZY_OR"sv;
  case nodeType::NOT:
    return "NOT"sv;
  }
  throw std::exception( std::out_of_range{ "bad nodeType" } );
}

inline nodeType toNodeType( std::string_view n ) {
  constexpr auto types =
      std::array{ nodeType::LAZY_AND, nodeType::NONLAZY_OR, nodeType::NONLAZY_AND, nodeType::LAZY_OR, nodeType::NOT };
  auto i = std::find_if( types.begin(), types.end(), [n]( nodeType nt ) { return n == toString( nt ); } );
  if ( i == types.end() ) throw std::exception( std::out_of_range( "bad nodeType" ) );
  return *i;
};

struct NodeDefinition final {
  std::string              name;
  std::string              type;
  std::vector<std::string> children;
  bool                     ordered;
};
