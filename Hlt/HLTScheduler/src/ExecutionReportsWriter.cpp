/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/HltDecReports.h"
#include "LHCbAlgs/Transformer.h"

#include "GaudiKernel/EventContext.h"
#include "GaudiKernel/StatusCode.h"
#include "Kernel/EventContextExt.h"
#include "Kernel/IIndexedANNSvc.h"
#include "Kernel/ISchedulerConfiguration.h"
#include <string>
#include <type_traits>
#include <vector>

namespace {
  bool ends_with( std::string_view s, std::string_view suffix ) {
    return s.size() >= suffix.size() && s.substr( s.size() - suffix.size() ).compare( suffix ) == 0;
  }

} // namespace

/** @brief Write DecReport objects based on the status of the execution nodes in the scheduler.
 *
 * Each execution node of the HltControlFlowMgr is converted to a DecReport
 * object, with the decision corresponding to the state of the node. The ID
 * used in the DecReport is taken from the ANNSvc.
 *
 * Note that historically, the names that we expect to see in the DecReports end in "Decision",
 * and we keep this behaviour, so the ANNSvc will look for trigger decisions called <node_name> +
 * "Decision" (see the decision_name variable).
 */
class ExecutionReportsWriter final : public LHCb::Algorithm::Transformer<LHCb::HltDecReports( EventContext const& )> {
public:
  ExecutionReportsWriter( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer{ name, pSvcLocator, { "DecReportsLocation", "/Event/DecReport" } } {}
  StatusCode          start() override;
  LHCb::HltDecReports operator()( EventContext const& evtCtx ) const override;

private:
  ServiceHandle<LHCb::Interfaces::ISchedulerConfiguration> m_scheduler{ this, "Scheduler", "HLTControlFlowMgr" };

  /// ANNSvc for translating selection names to int selection IDs
  ServiceHandle<IIndexedANNSvc> m_annsvc{ this, "IndexedANNSvc", "HltANNSvc", "Service to retrieve DecReport IDs" };

  /// Map from line name to index in the scheduler's execution node list and
  /// number assigned by the HltAnnSvc
  std::map<std::string, std::pair<int, int>> m_name_indices{};

  Gaudi::Property<std::vector<std::string>> m_line_names{
      this, "Persist", {}, "Specify the nodes to be written to TES" };
  Gaudi::Property<std::string> m_ann_key{
      this, "ANNSvcKey", "",
      [this]( const auto& ) {
        if ( m_ann_key.value().empty() || ends_with( m_ann_key.value(), "SelectionID" ) ) return;
        throw GaudiException( "ANNSvcKey " + m_ann_key.value() + " does not end in \"SelectionID\"",
                              __PRETTY_FUNCTION__, StatusCode::FAILURE );
      },
      "Key used to query the ANN service" };
  Gaudi::Property<unsigned int> m_key{ this, "TCK", 0u };
};

DECLARE_COMPONENT( ExecutionReportsWriter )

StatusCode ExecutionReportsWriter::start() {
  auto sc = Transformer::start();
  if ( !sc ) return sc;

  const auto& scheduler_items = m_scheduler->getNodeNamesWithIndices();
  for ( const auto& name : m_line_names ) {
    // The scheduler stores execution nodes as a flat list. We can cache the
    // index of the execution node for each line now, and retrieve the node from
    // the list in each event
    auto node_idx = std::find_if( scheduler_items.begin(), scheduler_items.end(),
                                  [&]( const auto& p ) { return std::get<0>( p ) == name; } );
    if ( node_idx == scheduler_items.end() ) {
      error() << "Line name not a known execution node: " << name << endmsg;
      return StatusCode::FAILURE;
    }

    // Historically names in DecReports end with decision, so we keep this.
    // Thus ANNSvc and is expected to contain (node name + "Decision") <-> int
    auto decision_name = name + "Decision";

    // Translate each decision name to an int, which will be written to the DecReport
    // If the decision name isn't known to the ANNSvc we can't translate it
    const auto& ann_items = m_annsvc->s2i( m_key.value(), m_ann_key.value() );
    auto        ann_idx   = std::find_if( ann_items.begin(), ann_items.end(),
                                          [&]( const auto& p ) { return std::get<0>( p ) == decision_name; } );
    if ( ann_idx == ann_items.end() ) {
      error() << "Decision name not known to ANNSvc: " << decision_name << endmsg;
      return StatusCode::FAILURE;
    }

    m_name_indices[decision_name] = { node_idx->second, ann_idx->second };
  }

  return sc;
}

LHCb::HltDecReports ExecutionReportsWriter::operator()( EventContext const& evtCtx ) const {
  auto const& lhcbExt = evtCtx.getExtension<LHCb::EventContextExtension>();
  auto const& state   = lhcbExt.getSchedulerExtension<LHCb::Interfaces::ISchedulerConfiguration::State>();

  LHCb::HltDecReports reports{};
  reports.setConfiguredTCK( m_key );
  reports.reserve( m_name_indices.size() );
  for ( const auto& [i, item] : LHCb::range::enumerate( m_name_indices ) ) {
    const auto& [decision_name, idx] = item;
    const auto& [node_idx, ann_idx]  = idx;
    const auto& node                 = state.node( node_idx );
    const bool  executed             = node.executionCtr == 0;
    const bool  passed = executed && node.passed; // Attention! node.passed is true when the node hasn't been executed
    reports.insert( decision_name, { passed, 0, 0, 0, ann_idx } ).ignore();
  }
  if ( msgLevel( MSG::DEBUG ) ) debug() << "Created HltDecReports:\n" << reports << endmsg;

  return reports; // write down something better?
}
