###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from LHCbTesting import LHCbExeTest


class Test(LHCbExeTest):
    command = ["gaudirun.py", "-v", "../options/testCF.py"]

    def test_control_flow(self, stdout: bytes):
        expected_strings = [
            b"""HLTControlFlowMgr VERBOSE StateTree: CFNode   executionCtr|passed
            NONLAZY_AND: moore     0|0
            LAZY_AND: line2       0|0
            A3                   0|0
            A4                   1|1
            NONLAZY_OR: decision  0|1
            LAZY_OR: line1       0|1
            A1                  0|1
            A2                  1|1
            A5                   0|1
            NOT: notA1           0|0
            A1                  0|1
            ExecReportsWriter     0|1
            """,
            b"""HLTControlFlowMgr VERBOSE AlgsWithStates: Algorithm   isExecuted|filterPassed
            B1                  1|1
            B2                  1|1
            A1                  1|1
            A2                  0|0
            A3                  1|0
            A4                  0|0
            A5                  1|1
            ExecReportsWriter   1|1
            """,
        ]
        for expected_string in expected_strings:
            assert stdout.count(expected_string) < 4, (
                f"control flow gone wrong for {expected_string}"
            )

    def test_node_order(self, stdout: bytes):
        nodeorder = b"ordered nodes: [A3, A4, A5, A1, A2, ExecReportsWriter]"
        assert nodeorder in stdout, "node order is wrong"
