###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from LHCbTesting import LHCbExeTest


class Test(LHCbExeTest):
    command = ["gaudirun.py", "-v", "../options/testNodeLogic.py"]

    def test_control_flow_logic(self, stdout: bytes):
        expected_strings = [
            "NONLAZY_OR: top  0|1",
            " NONLAZY_AND: 0  0|1",
            "  A1             0|1",
            "  A1             0|1",
            " NONLAZY_AND: 1  0|0",
            "  A1             0|1",
            "  A2             0|0",
            " NONLAZY_AND: 2  0|0",
            "  A2             0|0",
            "  A1             0|1",
            " NONLAZY_AND: 3  0|0",
            "  A2             0|0",
            "  A2             0|0",
            " NONLAZY_OR: 4   0|1",
            "  A1             0|1",
            "  A1             0|1",
            " NONLAZY_OR: 5   0|1",
            "  A1             0|1",
            "  A2             0|0",
            " NONLAZY_OR: 6   0|1",
            "  A2             0|0",
            "  A1             0|1",
            " NONLAZY_OR: 7   0|0",
            "  A2             0|0",
            "  A2             0|0",
            " LAZY_AND: 8     0|1",
            "  A1             0|1",
            "  A1             0|1",
            " LAZY_AND: 9     0|0",
            "  A1             0|1",
            "  A2             0|0",
            " LAZY_AND: 10    0|0",
            "  A2             0|0",
            "  A1             0|1",
            " LAZY_AND: 11    0|0",
            "  A2             0|0",
            "  A2             0|0",
            " LAZY_OR: 12     0|1",
            "  A1             0|1",
            "  A1             0|1",
            " LAZY_OR: 13     0|1",
            "  A1             0|1",
            "  A2             0|0",
            " LAZY_OR: 14     0|1",
            "  A2             0|0",
            "  A1             0|1",
            " LAZY_OR: 15     0|0",
            "  A2             0|0",
            "  A2             0|0",
            " NOT: notA1      0|0",
            "  A1             0|1",
            " NOT: notA2      0|1",
            "  A2             0|0",
        ]

        for expected_string in expected_strings:
            assert stdout.decode().count(expected_string) >= 4, (
                f"control flow gone wrong for {expected_string}"
            )
