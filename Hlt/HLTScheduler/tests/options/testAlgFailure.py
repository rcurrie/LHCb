###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import Gaudi__Examples__FloatDataConsumer as FloatDataConsumer
from Configurables import Gaudi__Examples__IntDataProducer as IntDataProducer
from Configurables import Gaudi__Examples__IntFloatToFloatData as IntFloatToFloatData
from Configurables import (
    HiveDataBrokerSvc,
    HiveWhiteBoard,
    HLTControlFlowMgr,
)
from Gaudi.Configuration import ApplicationMgr

producer = IntDataProducer("producer", OutputLocation="int")
transformer = IntFloatToFloatData(
    "transformer", InputLocation="int", OtherInput="int", OutputLocation="float"
)
consumer1 = FloatDataConsumer("consumer1", InputLocation="float")
consumer2 = FloatDataConsumer("consumer2", InputLocation="float")

whiteboard = HiveWhiteBoard("EventDataSvc", EventSlots=1)

HLTControlFlowMgr().CompositeCFNodes = [
    ("moore", "NONLAZY_OR", ["consumer1", "consumer2"], True),
]

HLTControlFlowMgr().ThreadPoolSize = 1

app = ApplicationMgr(
    EvtMax=100,
    EvtSel="NONE",
    ExtSvc=[whiteboard, "Gaudi::Monitoring::MessageSvcSink"],
    EventLoop=HLTControlFlowMgr(),
    TopAlg=[producer, transformer, consumer1, consumer2],
)

HiveDataBrokerSvc().DataProducers = app.TopAlg
