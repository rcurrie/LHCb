###############################################################################
# (c) Copyright 2019-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Consume a DST file"""

import os
from pathlib import Path

from Configurables import (
    ApplicationMgr,
    EventPersistencySvc,
    # EvtStoreSvc,
    EventSelector,
    HiveDataBrokerSvc,
    HiveWhiteBoard,
    # HLTControlFlowMgrRef as HLTControlFlowMgr,
    HLTControlFlowMgr,
    IODataManager,
    ReadHandleAlg,
)
from Configurables import (
    Gaudi__Hive__FetchDataFromFile as FetchDataFromFile,
)
from Configurables import (
    Gaudi__Monitoring__MessageSvcSink as MessageSvcSink,
)
from Configurables import (
    Gaudi__RootCnvSvc as RootCnvSvc,
)
from Gaudi.Configuration import FileCatalog

# TODO bump these numbers once we can read DSTs with multiple threads
n_slots = 1
n_threads = 1

input_filename = Path(os.getenv("PREREQUISITE_0", "")) / "data.dst"
event_selector = EventSelector(
    PrintFreq=1000,
    FirstEvent=11,  # FirstEvent is 1-based: 11 means skip 10 events
    Input=[
        f"DATAFILE='PFN:{input_filename}' " + "SVC='Gaudi::RootEvtSelector' OPT='READ'"
    ],
)

reader = ReadHandleAlg(Input="/Event/MyCollision")
fetch_data = FetchDataFromFile(DataKeys=[str(reader.Input)])
broker = HiveDataBrokerSvc(DataProducers=[fetch_data, reader])

all_algs = [reader]
scheduler = HLTControlFlowMgr(
    "HLTControlFlowMgr",
    CompositeCFNodes=[("top", "LAZY_AND", [a.getFullName() for a in all_algs], True)],
    ThreadPoolSize=n_threads,
)
event_store = HiveWhiteBoard(
    # event_store = EvtStoreSvc(  # TODO thread safety issue in FetchDataFromFile
    "EventDataSvc",
    DataLoader=EventPersistencySvc(CnvServices=[RootCnvSvc()]),
    ForceLeaves=True,
    RootCLID=1,
    EnableFaultHandler=True,
    EventSlots=n_slots,
)

ApplicationMgr(
    EvtMax=77,
    EvtSel=event_selector.getFullName(),
    ExtSvc=[
        event_store,
        broker,
        FileCatalog(Catalogs=["xmlcatalog_file:scheduler.xml"]),
        IODataManager(),
        MessageSvcSink(),
    ],
    EventLoop=scheduler,
)
