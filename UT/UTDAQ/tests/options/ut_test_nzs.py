###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import logging

from DDDB.CheckDD4Hep import UseDD4Hep
from Gaudi.Configuration import DEBUG, INFO, VERBOSE
from PRConfig.TestFileDB import test_file_db

from PyConf.Algorithms import UTRawBankToUTNZSDigitsAlg
from PyConf.application import (
    CompositeNode,
    configure,
    configure_input,
    default_raw_banks,
    make_odin,
)

# DD4HEP configuration
if UseDD4Hep:
    from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc

    dd4hepsvc = DD4hepSvc()
    dd4hepsvc.GeometryVersion = "run3/trunk"
    dd4hepsvc.GeometryMain = "LHCb.xml"
    dd4hepsvc.DetectorList = ["/world", "UT"]

# I/O configuration
options = test_file_db["ut-nzs-2024"].make_lbexec_options(
    simulation=False,
    python_logging_level=logging.INFO,
    evt_max=100,
    data_type="Upgrade",
    geometry_version="run3/trunk",
    conditions_version="master",
)

# Setting up algorithms pipeline
configure_input(options)
odin = make_odin()
decoder_nzs = UTRawBankToUTNZSDigitsAlg(
    name="UTRawToNZSDigits", UTBank=default_raw_banks("UTNZS"), OutputLevel=INFO
)
decoder_nzs_err = UTRawBankToUTNZSDigitsAlg(
    name="UTErrRawToNZSDigits", UTBank=default_raw_banks("UTError"), OutputLevel=INFO
)

top_node = CompositeNode("UT_NZSDecoding", [decoder_nzs, decoder_nzs_err])
configure(options, top_node)
