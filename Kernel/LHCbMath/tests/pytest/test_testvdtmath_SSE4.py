###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import pytest
from GaudiTesting import platform_matches
from LHCbTesting import LHCbExeTest
from LHCbTesting.preprocessors import RegexpReplacer


@pytest.mark.skipif(platform_matches(["arm"]), reason="Unsupported platform")
class Test(LHCbExeTest):
    command = ["TestVDTMathSSE4"]
    reference = "../refs/TestVDTMath.yaml"
    preprocessor = LHCbExeTest.preprocessor + RegexpReplacer(
        when="-0.633977997731447",
        orig=r"-0.633977997731447",
        repl=r"-0.633977997731446",
    )
