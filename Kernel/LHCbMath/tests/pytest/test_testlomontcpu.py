###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import pytest
from GaudiTesting import platform_matches
from LHCbTesting import LHCbExeTest


@pytest.mark.skipif(
    platform_matches(["asan", "lsan", "ubsan", "tsan"]), reason="Unsupported platform"
)
class Test(LHCbExeTest):
    command = ["TestLomont2"]
