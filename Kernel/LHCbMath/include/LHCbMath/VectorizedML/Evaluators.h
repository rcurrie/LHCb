/*****************************************************************************\
* (c) Copyright 2023-2024 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Feature.h"
#include "InferenceBase.h"
#include <GaudiKernel/GaudiException.h>
#include <LHCbMath/SIMDWrapper.h>
#include <LHCbMath/Utils.h>
#include <array>
#include <nlohmann/json.hpp>
#include <optional>
#include <string>
#include <vector>

namespace LHCb::VectorizedML {

  template <typename Model, typename Features>
  class EvaluatorOnRange {
  public:
    using ModelType = Model;

    StatusCode load( std::optional<std::string> const& buffer ) {
      // first check meta data
      if ( !buffer ) throw GaudiException( "Couldn't open weights file", "", StatusCode::FAILURE );
      auto weights_json  = nlohmann::json::parse( *buffer );
      bool feature_check = false;
      for ( auto const& [key, value] : weights_json.items() ) {
        // check metadata (features)
        if ( key.find( "metadata" ) != std::string::npos ) {
          for ( auto const& [subkey, subvalue] : value.items() ) {
            if ( subkey.find( "features" ) != std::string::npos ) {
              assert( subvalue.size() == Model::InputVec::size );
              feature_check = true;
              std::vector<std::string> names;
              names.insert( names.end(), subvalue.begin(), subvalue.end() );
              Utils::unwind<0, Model::InputVec::size>(
                  [&]( auto i ) { feature_check &= m_features.template get<i>().name() == names[i]; } );
            }
          }
        }
      }
      if ( !feature_check )
        throw GaudiException(
            "Feature of model are not the same as of the weights file, or info not present in metadata", "",
            StatusCode::FAILURE );
      // now load model parameters
      return m_model.load( buffer );
    }

    // evaluate while taking care of SIMD instructions for loop over non-SOA containers
    // for now have input container not const (e.g. needed for info erasure and saving in ProtoParticles)
    template <typename Input, typename FSelect, typename FOutput>
    void evaluate( Input& container, FSelect&& select, FOutput&& save_output ) const {
      // input object iteration
      auto obj_itr = container.begin();
      auto nObjs   = container.size();

      // conversions to simd
      using FType    = typename Model::FType;
      using InputVec = typename Model::InputVec;
      auto invec     = InputVec();

      std::array<std::array<float, simd::size>, Model::InputVec::size> feature_arrs;
      std::vector<size_t>                                              obj_indices;
      obj_indices.reserve( simd::size );

      // conversions from simd
      std::array<std::array<float, Model::OutputVec::size>, simd::size> output_arr;

      // loop in chunks of SIMD size
      for ( size_t i = 0; i < nObjs; i += simd::size ) {
        // check which objs need to be evaluated
        for ( size_t j = 0; j < simd::size; j++ ) {
          // make sure to continue only within container bounds
          if ( i + j >= nObjs ) break;
          auto obj = *( obj_itr + j );
          // further selection
          if ( !select( obj ) ) continue;
          // bookkeep selected obj indices
          obj_indices.push_back( j );
        }
        if ( obj_indices.empty() ) continue;

        // fill SIMD input
        for ( auto j : obj_indices ) {
          auto obj = *( obj_itr + j );
          Utils::unwind<0, Model::InputVec::size>(
              [&]( auto k ) { feature_arrs[k][j] = m_features.template get<k>()( obj ); } );
        }
        for ( auto k = 0; k < Model::InputVec::size; k++ ) invec( k ) = FType( feature_arrs[k] );

        // evaluate
        auto output = m_model.evaluate( invec );

        // convert from SIMD output and save
        for ( auto l = 0; l < Model::OutputVec::size; l++ ) {
          auto arr = SIMDWrapper::to_array( output( l ) );
          for ( auto j : obj_indices ) output_arr[j][l] = arr[j];
        }
        for ( auto j : obj_indices ) save_output( *( obj_itr + j ), output_arr[j] );

        // go to next objects
        obj_indices.clear();
        obj_itr += simd::size;
      }
    }

    Features const* features() const { return &m_features; }

    Model const* model() const { return &m_model; }

  private:
    Model    m_model;
    Features m_features;
  };

} // namespace LHCb::VectorizedML
