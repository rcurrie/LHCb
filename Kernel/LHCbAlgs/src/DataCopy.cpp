/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Gaudi/Accumulators.h"
#include "Gaudi/Algorithm.h"
#include "GaudiAlg/FixTESPath.h"
#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/SmartDataPtr.h"

/** @file
 *
 *  A pair of useful algorithms to copy/move/link data in TES
 *
 *  @see Gaudi::DataCopy
 *  @see Gaudi::DataLink
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@cern.ch
 *  @date 2011-10-05
 *
 */

namespace Gaudi {

  /**
   *  Trivial algorithm to "copy/move" data in TES
   *
   *  @code
   *  from Configurables import Gaudi__DataCopy
   *  rootInTes = '/Event/Leptonic/'
   *  rawEvt  = Gaudi_DataCopy ( 'CopyRawEvt' ,
   *                             What   =      '/Event/DAQ/RawEvent' ,
   *                             Target = rootInTes + 'DAQ/RawEvent' )
   *  @endcode
   *
   *  Alternatively one can use Gaudi__DataLink, correspoding to
   *  COPY == false as a value of the template parameter
   *
   *  @see Gaudi::DataLink
   *  @author Vanya BELYAEV Ivan.Belyaev@cern.ch
   *  @date 2011-10-05
   */
  template <bool COPY>
  class DataCopy : public FixTESPath<Gaudi::Algorithm> {
  public:
    using FixTESPath::FixTESPath;
    StatusCode initialize() override;
    StatusCode execute( const EventContext& ) const override;

  private:
    /// update handler for "What"
    void handler_1( Gaudi::Details::PropertyBase& p ); // update handler for "What"
    /// update handler for "Target"
    void handler_2( Gaudi::Details::PropertyBase& p ); // update handler for "Target"
    /// never fail
    Gaudi::Property<bool> m_never_fail{ this, "NeverFail", true, "Never fail" };
    /// what  to copy
    Gaudi::Property<std::string> m_what{ this, "What", "", &Gaudi::DataCopy<COPY>::handler_1,
                                         "Data to be copied/linked" };
    /// where to copy
    Gaudi::Property<std::string> m_target{ this, "Target", "", &Gaudi::DataCopy<COPY>::handler_2,
                                           "Target for copy/link" };

    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_noWhat{ this, "Nothing to be copied", 10 };
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_noTarget{ this, "Target is not specified", 10 };
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_whatIsTarget{ this, "No need to copy", 10 };
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_noData{ this, "No valid data found " };
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_unableToCopy{ this, "Unable copy/link" };

  }; // end of class DataCopy

} // end of namespace Gaudi

/// update handler for property "What"
template <bool COPY>
void Gaudi::DataCopy<COPY>::handler_1( Gaudi::Details::PropertyBase& /* p */ ) {
  if ( Gaudi::StateMachine::INITIALIZED > FSMState() ) { return; }
  m_what = fullTESLocation( m_what, true );
  info() << "Change property 'What'   :" << m_what << endmsg;
}

template <bool COPY>
void Gaudi::DataCopy<COPY>::handler_2( Gaudi::Details::PropertyBase& /* p */ ) {
  if ( Gaudi::StateMachine::INITIALIZED > FSMState() ) { return; }
  m_target = fullTESLocation( m_target, true );
  info() << "Change property 'Target' :" << m_target << endmsg;
}

template <bool COPY>
StatusCode Gaudi::DataCopy<COPY>::initialize() {
  return FixTESPath::initialize().andThen( [&]() -> StatusCode {
    m_what   = fullTESLocation( m_what, true );
    m_target = fullTESLocation( m_target, true );
    if ( m_what.empty() ) { ++m_noWhat; }
    if ( m_target.empty() ) { ++m_noTarget; }
    if ( m_what.value() == m_target.value() ) { ++m_whatIsTarget; }
    return StatusCode::SUCCESS;
  } );
}

template <bool COPY>
StatusCode Gaudi::DataCopy<COPY>::execute( const EventContext& evtCtx ) const {
  SmartDataPtr<DataObject> obj( evtSvc(), m_what );
  if ( !obj ) {
    execState( evtCtx ).setFilterPassed( false );
    ++m_noData;
    return m_never_fail.value() ? StatusCode::SUCCESS : StatusCode::FAILURE;
  }
  DataObject* object = obj;
  StatusCode  sc;
  if constexpr ( COPY ) {
    sc = evtSvc()->registerObject( m_target, object );
  } else {
    sc = evtSvc()->linkObject( m_target, object );
  }
  if ( sc.isFailure() ) {
    execState( evtCtx ).setFilterPassed( false );
    ++m_unableToCopy;
    return m_never_fail.value() ? StatusCode::SUCCESS : sc;
  }
  execState( evtCtx ).setFilterPassed( true );
  return StatusCode::SUCCESS;
}

DECLARE_COMPONENT_WITH_ID( Gaudi::DataCopy<true>, "Gaudi__DataCopy" )
DECLARE_COMPONENT_WITH_ID( Gaudi::DataCopy<false>, "Gaudi__DataLink" )
