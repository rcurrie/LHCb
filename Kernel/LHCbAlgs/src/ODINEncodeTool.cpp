/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "Event/ODIN.h"
#include "Event/RawBank.h"
#include "Event/RawEvent.h"
#include "Gaudi/Accumulators.h"
#include "GaudiAlg/IGenericTool.h"
#include "GaudiKernel/AlgTool.h"
#include "GaudiKernel/DataObjectHandle.h"
#include "ODINCodec.h"
#include <memory>
#include <vector>

/** @class ODINEncodeTool ODINEncodeTool.h
 *
 *  Tool to encode the ODIN object into a RawBank. The raw bank is then added to
 *  the RawEvent, which must exist.
 *
 *  @see ODINCodecBaseTool for the properties.
 *
 *  @author Marco Clemencic
 *  @date   2009-02-02
 */
class ODINEncodeTool final : public extends<AlgTool, IGenericTool> {

public:
  /// Standard constructor
  using extends::extends;

  /// Do the conversion
  void execute() override;

private:
  /// Decode the ODIN RawBank and fill the ODIN object
  /// @param odin reference to the ODIN object to convert
  /// @return     pointer to the newly created RawBank (ownership on the caller)
  LHCb::RawBank* i_encode( const LHCb::ODIN& odin, const int bank_version ) const;

  /// Location in the transient store of the ODIN object.
  DataObjectReadHandle<LHCb::ODIN> m_odinLocation{ this, "ODINLocation", LHCb::ODINLocation::Default,
                                                   "Location of the ODIN object in the transient store." };

  /// Location in the transient store of the RawEvent object
  DataObjectReadHandle<LHCb::RawEvent> m_rawEventLocation{ this, "RawEventLocation", LHCb::RawEventLocation::Default,
                                                           "Location of the RawEvent object in the transient store." };

  Gaudi::Property<int> m_bankVersion{ this, "BankVersion", 7, "version of the ODIN bank layout to produce" };

  /// If set to true, override the destination object.
  Gaudi::Property<bool> m_force{ this, "Force", false, "If already present, override the destination object." };

  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_noRaw{
      this, "Cannot find RawEvent object to store encoded ODIN bank" };
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_noODIN{ this, "Cannot find ODIN object to encode" };
  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_encoding_failure{ this, "Failed to encode ODIN into RawBank" };
};

//=============================================================================
// IMPLEMENTATION
//=============================================================================

// Declaration of the Tool Factory
DECLARE_COMPONENT( ODINEncodeTool )

//=============================================================================
// Main function
//=============================================================================
void ODINEncodeTool::execute() {
  if ( msgLevel( MSG::DEBUG ) ) debug() << "Getting " << m_rawEventLocation.objKey() << endmsg;

  // Check if there is an ODIN
  LHCb::ODIN const* odin = m_odinLocation.get();
  if ( !odin ) {
    ++m_noODIN;
    return;
  }

  // Check if there is a RawEvent
  LHCb::RawEvent* raw = m_rawEventLocation.getIfExists();
  if ( !raw ) {
    ++m_noRaw;
    return;
  }

  // pointer for a pre-existing bank
  const LHCb::RawBank* old_bank = nullptr;
  // Check if have an ODIN bank already
  if ( const auto& odinBanks = raw->banks( LHCb::RawBank::ODIN ); !odinBanks.empty() ) {
    if ( !m_force ) return; // keep the existing bank
    // we have to replace it... remember which it is, so we can do it if the
    // encoding is successful, just before adding the new bank
    old_bank = odinBanks.front();
  }

  // Encode the ODIN object -- note that `i_encode` returns the result of a call te `allocateBank`
  // and as a result, the returned value MUST BE PASSED TO RawEvent::adoptBank
  LHCb::RawBank* bank = i_encode( *odin, m_bankVersion );
  if ( !bank ) {
    // something went horribly wrong...
    ++m_encoding_failure;
    return;
  }

  // add the bank -- removing the previous one if we got to this point
  if ( old_bank ) raw->removeBank( old_bank );
  raw->adoptBank( bank, true );
}

//=============================================================================
// Encode
//=============================================================================
LHCb::RawBank* ODINEncodeTool::i_encode( const LHCb::ODIN& odin, const int bank_version ) const {

  // encode the ODIN instance
  std::array<std::uint32_t, LHCb::ODIN::BANK_SIZE / sizeof( std::uint32_t )> data{};
  LHCb::ODINCodec::encode( odin, bank_version, data );
  // Create the new bank
  // Note that we cannot delete it, so better that there is no failure after
  // this line.
  // ODIN source ID must be in the range 0x0001 - 0x000A included (see EDMS 2100937)
  return LHCb::RawEvent::createBank( 0x0001, LHCb::RawBank::ODIN, bank_version, LHCb::make_span( data ) );
}

//=============================================================================
