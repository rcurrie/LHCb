/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "ODINCodec.h"

#include "GaudiKernel/GaudiException.h"

using namespace LHCb;
using namespace LHCb::ODINCodec;

namespace {
  inline void Assert( const bool cond, const std::string& tag, const std::string& msg ) {
    if ( !( cond ) ) throw GaudiException( tag, msg, StatusCode::FAILURE );
  }
  inline void DecoderAssert( const bool cond, const std::string& msg ) {
    Assert( cond, "LHCb::ODINCodec::decode", msg );
  }
} // namespace

ODIN LHCb::ODINCodec::decode( const RawBank& bank, const bool ignoreBankVersion ) {
  // Check bank type
  DecoderAssert( bank.type() == LHCb::RawBank::ODIN, "Wrong ODIN raw bank type" );
  DecoderAssert( bank.magic() == LHCb::RawBank::MagicPattern, "Magic pattern mismatch in ODIN raw bank" );

  // Validate bank version
  const auto version = bank.version();
  if ( version == ODIN::BANK_VERSION || ignoreBankVersion ) {
    // Validate bank size
    DecoderAssert( bank.size() == ODIN::BANK_SIZE, "Wrong ODIN bank size " + std::to_string( bank.size() ) +
                                                       ", expected " + std::to_string( ODIN::BANK_SIZE ) );

    return ODIN::from_version<ODIN::BANK_VERSION>( bank.range<const unsigned int>() );
  } else if ( version == 6 ) {
    // Validate bank size
    constexpr auto expected_size = 10 * sizeof( std::uint32_t );
    DecoderAssert( bank.size() == expected_size, "Wrong ODIN bank size " + std::to_string( bank.size() ) +
                                                     ", expected " + std::to_string( expected_size ) );

    return ODIN::from_version<6>( bank.range<const unsigned int>() );
  } else {
    DecoderAssert( false, "Unknown ODIN bank version " + std::to_string( version ) +
                              ", supported ones are: " + std::to_string( ODIN::BANK_VERSION ) + ", 6" );
    return {}; // this is to avoid warnings as the compiler does not know that DecoderAssert throws an exception
  }
}

void LHCb::ODINCodec::encode( const LHCb::ODIN& odin, const int bank_version,
                              LHCb::span<std::uint32_t> output_buffer ) {
  if ( bank_version == ODIN::BANK_VERSION ) {
    odin.to_version<ODIN::BANK_VERSION>( output_buffer );
  } else if ( bank_version == 6 ) {
    odin.to_version<6>( output_buffer );
  } else {
    Assert( false, "LHCb::ODINCodec::encode",
            "Unsupported ODIN bank version for encoding: " + std::to_string( bank_version ) );
  }
}
