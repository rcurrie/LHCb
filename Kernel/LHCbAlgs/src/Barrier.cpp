/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// @author Niklas Nolte
#include "Event/Track.h"
#include "GaudiKernel/KeyedContainer.h"
#include "GaudiKernel/SharedObjectsContainer.h"
#include "LHCbAlgs/MergingTransformer.h"
#include "LHCbAlgs/SplittingTransformer.h"
#include "LHCbAlgs/Transformer.h"
#include <array>

/*  Barrier algorithms:

    - The Gatherer takes multiple optional input-containers (LHCb::Tracks for instance) (selections from
      different lines) and puts them into one big vector

    - The Merger creates the union of all those containers to have one container where some shared algorithm
      can loop over (implemented for Keyed(SharedObject)Containers and Masks (with integral types of values 0
      and 1)

    - The next Barrier step is some shared algorithm, taking the output of the merger (and in case of masks
      some other data)  as input

    - The scatterer takes the output of the shared algorithm + the gatherer output + merger output how to
      scatter the inputs to the different lines again

*/

template <typename T>
using VOC = Gaudi::Functional::vector_of_const_<T>;
template <typename T>
using KC = KeyedContainer<T, Containers::HashMap>;
template <typename T>
using SOC      = SharedObjectsContainer<T>;
using KeyValue = std::pair<std::string, std::string>;

// cType = container type that the gatherer gets as input
// vcType is the vector of those containers
template <typename cType, typename vcType = VOC<cType>>
struct BarrierGatherer;

// Input and output of the merger
template <typename In, typename Out = VOC<In>>
struct BarrierMerger;

// scatteredOut = type of output of the scatterer ( container of container of transformed things )
// transformedIn = output of the transformer ( container of transformed things )
// gatheredIn = output of Gatherer ( container of container of things )
template <typename scatteredOut, typename transformedIn, typename gatheredIn = scatteredOut>
struct BarrierScatterer;

template <typename cType>
struct BarrierGatherer<VOC<cType*>, VOC<cType*>> final
    : LHCb::Algorithm::MergingTransformer<VOC<cType*>( VOC<cType*> const& )> {

  BarrierGatherer( std::string const& name, ISvcLocator* pSvcLocator )
      : LHCb::Algorithm::MergingTransformer<VOC<cType*>( VOC<cType*> const& )>(
            name, pSvcLocator, { "InputLocations", { LHCb::TrackLocation::Velo } },
            { "OutputLocation", { "Rec/Track/GatheredTracks" } } ) {}

  VOC<cType*> operator()( VOC<cType*> const& vec ) const override { return vec; }
};

template <typename Type>
struct BarrierMerger<SOC<Type>, VOC<SOC<Type>*>> final
    : public LHCb::Algorithm::Transformer<SOC<Type>( VOC<SOC<Type>*> const& )> {

  BarrierMerger( std::string const& name, ISvcLocator* pSvcLocator )
      : LHCb::Algorithm::Transformer<SOC<Type>( VOC<SOC<Type>*> const& )>(
            name, pSvcLocator, { "InputLocation", "Rec/Track/GatheredTracks" },
            { "OutputLocation", "Rec/Track/MergedTracks" } ) {}

  SOC<Type> operator()( VOC<SOC<Type>*> const& vec ) const override {
    using std::begin;
    using std::end;
    SOC<Type> out;
    for ( SOC<Type> const* container : vec ) {
      if ( !container ) continue;
      for ( auto* thing : *container ) {
        if ( std::find( begin( out ), end( out ), thing ) == end( out ) ) { out.insert( thing ); }
      }
    }
    return out;
  }
};

template <typename OutType, typename InType, typename TransformedCType>
struct BarrierScatterer<std::vector<SOC<OutType>>, TransformedCType, VOC<SOC<InType>*>> final
    : public LHCb::Algorithm::SplittingTransformer<std::vector<SOC<OutType>>( TransformedCType const&,
                                                                              VOC<SOC<InType>*> const& )> {
  BarrierScatterer( std::string const& name, ISvcLocator* pSvcLocator )
      : LHCb::Algorithm::SplittingTransformer<std::vector<SOC<OutType>>( TransformedCType const&,
                                                                         VOC<SOC<InType>*> const& )>(
            name, pSvcLocator,
            { KeyValue{ "transformedInput", "Rec/Track/ForwardFastFitted" },
              KeyValue{ "gatheredInput", "Rec/Track/GatheredTracks" } },
            { "OutputLocations", { "" } } ) {}

  std::vector<SOC<OutType>> operator()( TransformedCType const&  transformedIn,
                                        VOC<SOC<InType>*> const& gatheredIn ) const override {
    std::vector<SOC<OutType>> Out{ gatheredIn.size() };
    for ( std::size_t i = 0; i < gatheredIn.size(); ++i ) {
      if ( !gatheredIn[i] ) { continue; }
      for ( auto* obj : *gatheredIn[i] ) { Out[i].insert( transformedIn( gatheredIn[i]->index( obj ) ) ); }
      if ( this->msgLevel( MSG::DEBUG ) ) { this->debug() << i << ": " << Out[i].size() << endmsg; }
    }
    return Out;
  }
};

// need this shit because the macro fucks up the type otherwise
using BarrierGatherer_LHCbSelection = BarrierGatherer<VOC<LHCb::Track::Selection*>, VOC<LHCb::Track::Selection*>>;
using BarrierMerger_LHCbSelection   = BarrierMerger<LHCb::Track::Selection, VOC<LHCb::Track::Selection*>>;
using BarrierScatterer_LHCbSelection =
    BarrierScatterer<std::vector<LHCb::Track::Selection>, LHCb::Tracks, VOC<LHCb::Track::Selection*>>;

DECLARE_COMPONENT_WITH_ID( BarrierGatherer_LHCbSelection, "TrackVectorGatherer" )
DECLARE_COMPONENT_WITH_ID( BarrierMerger_LHCbSelection, "TrackMergerTransformer" )
DECLARE_COMPONENT_WITH_ID( BarrierScatterer_LHCbSelection, "TrackVectorScatterer" )
