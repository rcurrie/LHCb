###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PRConfig.TestFileDB import test_file_db

from PyConf.Algorithms import CopyInputStream
from PyConf.application import (
    ApplicationOptions,
    configure,
    configure_input,
    create_or_reuse_rootIOAlg,
    input_from_root_file,
)
from PyConf.control_flow import CompositeNode

options = ApplicationOptions(_enabled=False)
options.set_input_and_conds_from_testfiledb("R08S14_smallfiles")
options.input_files = (
    test_file_db["R08S14_smallfiles"].filenames
    + test_file_db["2010_justFSR_EW"].filenames
    + test_file_db["R08S14_smallfiles"].filenames
)

options.output_type = "ROOT"
options.output_file = "tryRoot.dst"

config = configure_input(options)

reader = input_from_root_file("/Event/DAQ/RawEvent", forced_type="LHCb::RawEvent")
writer = CopyInputStream(
    name="CopyInputStream",
    InputFileLeavesLocation=create_or_reuse_rootIOAlg(options).InputLeavesLocation,
    Output="DATAFILE='{}' SVC='Gaudi::RootCnvSvc' OPT='RECREATE'".format(
        options.output_file
    ),
)

cf_node = CompositeNode("Seq", children=[reader, writer])
config.update(configure(options, cf_node))
