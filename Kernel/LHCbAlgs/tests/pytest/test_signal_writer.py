###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
from pathlib import Path

from LHCbTesting import LHCbExeTest


class Test(LHCbExeTest):
    command = ["gaudirun.py", "../options/signal_writer.py"]
    returncode = 1
    reference = {"messages_count": {"FATAL": 0, "ERROR": 0, "WARNING": 0}}

    def test_output_file(self, cwd: Path):
        expected_fn = cwd / "signal_0000269939_00000000000001450050.mdf"
        expected_size = 28032
        try:
            size = os.path.getsize(expected_fn)
        except OSError:
            raise OSError("Could not locate output MDF file.")

        assert size == expected_size, f"File size {size} != expected {expected_size}"
