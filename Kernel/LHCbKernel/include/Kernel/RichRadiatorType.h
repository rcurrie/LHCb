/*****************************************************************************\
* (c) Copyright 2000-20122 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//=================================================================================
/** @file RichRadiatorType.h
 *
 *  Header file for RICH particle ID enumeration : RichRadiatorType
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   08/07/2004
 */
//=================================================================================

#pragma once

// Detector
#include "Detector/Rich/Types.h"

// Gaudi
#include "GaudiKernel/SerializeSTL.h"

// STL
#include <cassert>
#include <iostream>
#include <string>

namespace Rich {

  /** Text conversion for RadiatorType enumeration
   *
   *  @param radiator Radiator type enumeration
   *  @return Radiator type as an std::string
   */
  std::string text( const RadiatorType radiator );

  /// Implement textual ostream << method for Rich::RadiatorType enumeration
  inline std::ostream& operator<<( std::ostream& os, const RadiatorType radiator ) {
    return os << Rich::text( radiator );
  }

  /// Print a vector of Radiator IDs
  inline std::ostream& operator<<( std::ostream& os, const Radiators& rads ) {
    return GaudiUtils::details::ostream_joiner( os << '[', rads, ", " ) << ']';
  }

  /// Print contents of a RadiatorArray templated type
  template <typename TYPE>
  inline std::ostream& operator<<( std::ostream& os, const RadiatorArray<TYPE>& rads ) {
    return GaudiUtils::details::ostream_joiner( os << '[', rads, ", " ) << ']';
  }

  /// Convert Radiator enum to RICH enum
  inline auto richType( const RadiatorType rad ) {
    // assert( Rich::Aerogel != rad ); // FixMe: Eventually enable this
    return ( Rich2Gas == rad ? Rich2 : Rich1 );
  }

} // namespace Rich
