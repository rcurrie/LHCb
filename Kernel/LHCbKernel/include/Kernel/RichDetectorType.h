/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file RichDetectorType.h
 *
 *  Header file for RICH particle ID enumeration : RichDetectorType
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   08/07/2004
 */
//-----------------------------------------------------------------------------

#pragma once

// Gaudi
#include "GaudiKernel/SerializeSTL.h"

// Detector
#include "Detector/Rich/Types.h"

// std include
#include <iostream>
#include <string>

//  General namespace for RICH specific definitions documented in RichSide.h
namespace Rich {

  /** Text conversion for DetectorType enumeration
   *
   *  @param detector RICH detector enumeration
   *  @return Detector type as a string
   */
  std::string text( const DetectorType detector );

  /// Implement textual ostream << method for Rich::DetectorType enumeration
  inline std::ostream& operator<<( std::ostream& os, const DetectorType& detector ) {
    return os << Rich::text( detector );
  }

  /// Print a vector of Detector IDs
  inline std::ostream& operator<<( std::ostream& os, const Detectors& dets ) {
    return GaudiUtils::details::ostream_joiner( os << '[', dets, ", " ) << ']';
  }

  /// Print contents of a DetectorArray templated type
  template <typename TYPE>
  inline std::ostream& operator<<( std::ostream& os, const DetectorArray<TYPE>& dets ) {
    return GaudiUtils::details::ostream_joiner( os << '[', dets, ", " ) << ']';
  }

  /// Convert RICH enum to Radiator enum
  inline auto radType( const DetectorType rich ) { return ( Rich1 == rich ? Rich1Gas : Rich2Gas ); }

} // namespace Rich
