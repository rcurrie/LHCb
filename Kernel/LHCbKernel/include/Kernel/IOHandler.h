/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "GaudiKernel/EventContext.h"
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/IIncidentSvc.h"

#include <future>
#include <memory>
#include <mutex>
#include <string>
#include <thread>
#include <tuple>
#include <type_traits>
#include <utility>
#include <vector>

namespace {
  // template code to decide whether a type is a tuple
  template <typename>
  struct is_tuple : std::false_type {};
  template <typename... T>
  struct is_tuple<std::tuple<T...>> : std::true_type {};

  /// template code to deduce IOHandler EventType from the Buffer one
  // By default just create a tuple {type, buffer}
  template <typename Buffer, typename BufferEventType>
  struct DeduceEventType {
    using type = std::tuple<BufferEventType, std::shared_ptr<Buffer>>;
  };
  // but in case of tuple for the buffer event type, concatenate
  template <typename Buffer, typename... T>
  struct DeduceEventType<Buffer, std::tuple<T...>> {
    using type = decltype( std::tuple_cat( std::declval<std::tuple<T...>>(),
                                           std::declval<std::tuple<std::shared_ptr<Buffer>>>() ) );
  };
  template <typename Buffer, typename BufferEventType>
  using DeduceEventType_t = typename DeduceEventType<Buffer, BufferEventType>::type;
  // get a tuple of n times the given Type, or directly the type for n = 1
  template <typename Type, unsigned int ND>
  struct GetTuple;
  template <typename Type, unsigned int ND>
  using GetTuple_t = typename GetTuple<Type, ND>::type;
  template <typename Type, unsigned int ND>
  struct GetTuple {
    using type =
        decltype( std::tuple_cat( std::declval<std::tuple<Type>>(), std::declval<GetTuple_t<Type, ND - 1>>() ) );
  };
  template <typename Type>
  struct GetTuple<Type, 1> {
    using type = std::tuple<Type>;
  };
} // namespace

namespace LHCb::IO {
  /// StatusCode extention for task output. Mainly allowing ENDOFINPUT
  enum class TaskOutput : StatusCode::code_t { SUCCESS = 0, FAILURE = 1, ENDOFINPUT = 301 };
} // namespace LHCb::IO

STATUSCODE_ENUM_DECL( LHCb::IO::TaskOutput )

namespace LHCb::IO {

  /// ENDOFINPUT should not be seen as an error of the algorithm which produces it
  struct TaskCategory : StatusCode::Category {
    const char* name() const override { return "Task"; }
    bool        isSuccess( StatusCode::code_t code ) const override {
      const TaskOutput e = static_cast<TaskOutput>( code );
      return e == TaskOutput::SUCCESS || e == TaskOutput::ENDOFINPUT;
    }
    std::string message( StatusCode::code_t code ) const override {
      switch ( static_cast<TaskOutput>( code ) ) {
      case TaskOutput::ENDOFINPUT:
        return "ENDOFINPUT";
      default:
        return StatusCode::default_category().message( code );
      }
    }
  };

  /// class used to throw exception when input is consumed
  struct EndOfInput : GaudiException {
    EndOfInput()
        : GaudiException( "No more data available", "IOHandler::EndOfInput", StatusCode( TaskOutput::ENDOFINPUT ) ) {}
  };

  /**
   * This class allows to read events from a set of files.
   * It's actually the core implementation of the IOAlgBase class allowing
   * to create algorithms to read data from files.
   *
   * The next() method is reentrant and allows to dispatch
   * events between multiple threads
   *
   * The class is templated by 2 parameters :
   *    - Buffer : the type of Buffer used. The Buffer class must define a
   *      EventType alias and have a get method with signature
   *        std::optional<EventType> get( EventContext const& evtCtx );
   *      This method needs to be thread safe
   *    - InputHandler : the class actually dealing with the data handling
   *      It has to implement 4 methods :
   *        InputHandler( Owner&, std::vector<std::string> const& inputFileNames )
   *        void skip( Owner&, unsigned int nbEvts )
   *        std::shared_ptr<Buffer> prefetchEvents( Owner&, unsigned int nbEvts )
   *        void rewind()
   *      In all this Owner is either an Algorithm or a Tool providing service,
   *      error and info methods
   *      None of these methods need to be thread safe
   * FIXME : when C++20 is available, use concepts here
   */
  template <typename Buffer, typename InputHandler>
  class IOHandler {

  public:
    // concatenate tuples when EventType is tuple, otherwise just create a tuple of 2 elements
    using EventType           = DeduceEventType_t<Buffer, typename Buffer::EventType>;
    using TESEventType        = DeduceEventType_t<Buffer, typename Buffer::TESEventType>;
    using KeyValue            = std::pair<std::string, std::string>;
    using ReturnKeyValuesType = GetTuple_t<KeyValue, std::tuple_size_v<EventType>>;

    // FIXME : drop incidentSvc part when FSRSink does not require it anymore
    template <typename Owner>
    IOHandler( Owner& owner, std::vector<std::string> const& input ) : m_inputHandler{ owner, input } {}

    virtual ~IOHandler() = default;
    template <typename Owner, typename... InputHandlerArgs>
    void initialize( Owner&, IIncidentSvc* incidentSvc, unsigned int bufferNbEvents, unsigned int nbSkippedEvents,
                     InputHandlerArgs&&... args );
    void finalize();
    template <typename Owner>
    void rewind( Owner& );

    /**
     * get next event from input
     * This method is reentrant
     * @return a tuple Event, shared_ptr<Buffer> where the second one
     * owns the data pointed by the first one
     * @throws EndOfInput
     */
    template <typename Owner>
    EventType next( Owner&, EventContext const& );

  private:
    /**
     * preloads data into the next buffer
     * Practically it first creates a task that will do the job,
     * then fills m_nextBuffer with the future of this task,
     * then releases the lock hold by the guard to release other
     * threads running on current buffer and finally does the job
     */
    template <typename Owner>
    void preloadNextBuffer( Owner&, std::unique_lock<std::mutex>& );

    /**
     * initializes buffers
     * Fetches data into the current buffer synchronously and then
     * creates a task for filling next buffer.
     * Note that the inputHandler needs to be initialized before calling this
     */
    template <typename Owner>
    void initializeBuffers( Owner& );

    // approximate size of the buffer used to prefetch rawbanks in terms of number of events
    unsigned int m_bufferNbEvents{ 1000 };
    // First event to process
    unsigned int m_nbSkippedEvents{ 0 };
    /// current Buffer to events
    std::shared_ptr<Buffer> m_curBuffer{ nullptr };
    /// std::future holding the next buffer to use
    std::future<std::shared_ptr<Buffer>> m_nextBuffer;
    /// lock for handling the change of buffer
    std::mutex m_changeBufferLock;
    /// local instance of class InputHandler
    InputHandler m_inputHandler;
    // pointer to the incident service, only used for backward compatibility
    // with EvntLoopMgr. FIXME should be removed
    IIncidentSvc* m_incidentSvc{ nullptr };
  };
} // namespace LHCb::IO

template <typename Buffer, typename InputHandler>
template <typename Owner>
void LHCb::IO::IOHandler<Buffer, InputHandler>::initializeBuffers( Owner& owner ) {
  m_inputHandler.skip( owner, m_nbSkippedEvents );
  // create an empty buffer as the current buffer
  m_curBuffer = m_inputHandler.prefetchEvents( owner, 0 );
  // and prefetch data into the next buffer. Will allow to prefecth concurrently to
  // the rest of initialization
  m_nextBuffer = std::async(
      std::launch::deferred,
      [&owner]( InputHandler& input, unsigned int nbEvents ) -> std::shared_ptr<Buffer> {
        return input.prefetchEvents( owner, nbEvents );
      },
      std::ref( m_inputHandler ), m_bufferNbEvents );
}

template <typename Buffer, typename InputHandler>
template <typename Owner, typename... InputHandlerArgs>
void LHCb::IO::IOHandler<Buffer, InputHandler>::initialize( Owner& owner, IIncidentSvc* incidentSvc,
                                                            unsigned int bufferNbEvents, unsigned int nbSkippedEvents,
                                                            InputHandlerArgs&&... args ) {
  m_bufferNbEvents  = bufferNbEvents;
  m_nbSkippedEvents = nbSkippedEvents;
  m_incidentSvc     = incidentSvc;
  try {
    // connect to first input
    m_inputHandler.initialize( owner, incidentSvc, args... );
    initializeBuffers( owner );
  } catch ( EndOfInput const& e ) {
    // ignore empty input at initialize. It will be reported on first
    // attempt to read an event
  }
}

template <typename Buffer, typename InputHandler>
void LHCb::IO::IOHandler<Buffer, InputHandler>::finalize() {
  // release current buffer
  m_curBuffer.reset();
  // join the thread that may be running in the task, trying to prefetch more data
  // and reset the release buffer
  if ( m_nextBuffer.valid() ) {
    try {
      m_nextBuffer.get().reset();
    } catch ( EndOfInput& ) {}
  }
  // finalize underlying inputHandler
  m_inputHandler.finalize();
}

template <typename Buffer, typename InputHandler>
template <typename Owner>
void LHCb::IO::IOHandler<Buffer, InputHandler>::rewind( Owner& owner ) {
  // drop and recreate buffers
  m_curBuffer.reset();
  if ( m_nextBuffer.valid() ) {
    try {
      m_nextBuffer.get().reset();
    } catch ( EndOfInput& ) {}
  }
  // rewind underlying inputhandler
  m_inputHandler.rewind( owner );
  // and recreate new buffers
  initializeBuffers( owner );
}

template <typename Buffer, typename InputHandler>
template <typename Owner>
typename LHCb::IO::IOHandler<Buffer, InputHandler>::EventType
LHCb::IO::IOHandler<Buffer, InputHandler>::next( Owner& owner, EventContext const& evtCtx ) {
  // get hold of current buffer, by copying the shared_ptr
  auto buffer = m_curBuffer;
  // if no current buffer, we got an EndOfFile at initalization, raise it again now
  if ( !buffer ) throw EndOfInput();
  // pick an event in it atomically
  auto event = buffer->get( evtCtx );
  if ( event.has_value() ) {
    if constexpr ( is_tuple<typename Buffer::EventType>::value ) {
      return std::tuple_cat( std::move( event.value() ), std::tie( buffer ) );
    } else {
      return { std::move( event.value() ), std::move( buffer ) };
    }
  } else {
    // No events remaining in current buffer, we need to renew the buffer
    while ( true ) {
      // new scope with serialized access so that only on thread deal with the renewal
      std::unique_lock guard( m_changeBufferLock );
      // We got the lock, but maybe the buffer was renewed while we waited. So double check
      // get hold of current buffer, by copying the shared_ptr
      buffer = m_curBuffer;
      // pick an event in it atomically
      auto event = buffer->get( evtCtx );
      if ( event.has_value() ) {
        if constexpr ( is_tuple<typename Buffer::EventType>::value ) {
          return std::tuple_cat( std::move( event.value() ), std::tie( buffer ) );
        } else {
          return { std::move( event.value() ), std::move( buffer ) };
        }
      } else {
        // ok, buffers still need to be renewed, or needs it again. Anyway we are in charge now
        // let's just use the "ready to use" nextBuffer. Note that in case it's not yet
        // fully ready, the "get" call will be waiting for it to be ready
        if ( m_nextBuffer.valid() ) {
          try {
            m_curBuffer = m_nextBuffer.get();
          } catch ( EndOfInput& ) {
            // End of input
            // for backward compatibility with Gaudi schedulers, an AbortEventIncident is sent
            // before throwing an exception. In case the HLTControlFlowManager is used, this is
            // not needed. But with the Gaudi EventloopManager, it is
            if ( m_incidentSvc ) { m_incidentSvc->fireIncident( Incident( owner.name(), IncidentType::AbortEvent ) ); }
            throw;
          }
        } else {
          // only happens at the end of input, when recreating the nextBuffer failed
          // but we still have scheduled some more events due to multi-threading
          throw EndOfInput();
        }
        // check whether we reached the end of input, only continue if no
        if ( m_curBuffer->size() != 0 ) {
          // Now launch the preload of next buffer. Note that the lock will be released
          // before the actual preloading but after m_nextBuffer has been filled with a new future
          // This ensures that other threads will wait on that future if they exhaust the current
          // buffer before the preloading is over
          preloadNextBuffer( owner, guard );
        }
      }
    } // guard is released here, if not already released by the preload
  }
}

template <typename Buffer, typename InputHandler>
template <typename Owner>
void LHCb::IO::IOHandler<Buffer, InputHandler>::preloadNextBuffer( Owner& owner, std::unique_lock<std::mutex>& guard ) {
  // use a task, and associate its future to next buffer
  std::packaged_task<std::shared_ptr<Buffer>( InputHandler&, unsigned int )> task(
      [&owner]( InputHandler& input, unsigned int nbEvents ) { return input.prefetchEvents( owner, nbEvents ); } );
  m_nextBuffer = task.get_future();
  // now that next buffer is set, we can unlock to let other theads consume the current buffer
  // while we are preloading the next one
  guard.unlock();
  // and preload data into the next buffer by running the task
  task( std::ref( this->m_inputHandler ), this->m_bufferNbEvents );
}
