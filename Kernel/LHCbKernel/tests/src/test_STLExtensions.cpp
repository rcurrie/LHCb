/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE test_PlatformInfo
#include <boost/test/unit_test.hpp>

#include "Kernel/STLExtensions.h"
#include <numeric>
#include <type_traits>

template <typename ElementType, auto N>
auto sum( LHCb::span<ElementType, N> s ) {
  using std::begin;
  using std::end;
  return std::accumulate( begin( s ), end( s ), ElementType{ 0 }, std::plus{} );
}

BOOST_AUTO_TEST_CASE( span ) {
  {
    std::array<int, 3> ai{ 9, 10, 11 };
    BOOST_CHECK( sum( LHCb::make_span( ai ) ) == 30 );
    std::vector<int> vi{ 9, 10, 11 };
    BOOST_CHECK( sum( LHCb::make_span( vi ) ) == 30 );

    const std::vector<int> cvi{ 9, 10, 11 };
    BOOST_CHECK( sum( LHCb::make_span( cvi ) ) == 30 );

    // check that we can make a span from iterators over vector elements ...
    BOOST_CHECK( sum( LHCb::make_span( vi.begin(), vi.begin() + 2 ) ) == 19 );

    BOOST_CHECK( sum( LHCb::make_span( cvi.begin(), cvi.begin() + 2 ) ) == 19 );

    // check that we can make a span from iterators over span elements ...
    auto si = LHCb::make_span( vi );
    BOOST_CHECK( sum( LHCb::make_span( si.begin(), si.begin() + 2 ) ) == 19 );

    auto csi = LHCb::make_span( cvi );
    BOOST_CHECK( sum( LHCb::make_span( csi.begin(), csi.begin() + 2 ) ) == 19 );
  }
}

BOOST_AUTO_TEST_CASE( range_single ) {
  {
    auto single = LHCb::range::single{ 4.0 };
    auto span   = LHCb::make_span( single );
    static_assert( span.extent == gsl::dynamic_extent ); // FIXME -- annoying that this isn't 1
    BOOST_CHECK_CLOSE( sum( span ), single.value(), 1e-4 );
    BOOST_CHECK( single.size() == 1 );
  }

  {
    // check conversion single<T> -> span<T>
    auto               single = LHCb::range::single{ 4.0 };
    LHCb::span<double> d      = single;
    static_assert( d.extent == gsl::dynamic_extent ); // FIXME annoying, this has dynamic extent...
    auto e = LHCb::span<double, 1>{ single };
    static_assert( e.extent == 1 );
    auto f = static_cast<LHCb::span<double, 1>>( single );
    BOOST_CHECK( &f[0] == &single.value() );
    auto g = static_cast<LHCb::span<double>>( single );
    BOOST_CHECK( &g[0] == &single.value() );
  }

  // check two ways of moving out of single...

  {
    auto single_uptr = LHCb::range::single{ std::make_unique<int>( 99 ) };
    BOOST_CHECK( static_cast<bool>( single_uptr.value() ) );

    auto uptr = std::move( single_uptr.value() );

    BOOST_CHECK( !static_cast<bool>( single_uptr.value() ) );
    BOOST_CHECK( *uptr == 99 );
  }

  {
    auto single_uptr = LHCb::range::single{ std::make_unique<int>( 99 ) };
    BOOST_CHECK( static_cast<bool>( single_uptr.value() ) );

    auto uptr = std::move( single_uptr ).value();

    BOOST_CHECK( !static_cast<bool>( single_uptr.value() ) );
    BOOST_CHECK( *uptr == 99 );
  }

  // check in place construction
  {
    struct S {
      constexpr S( double, int, const char* ) {}
    };
    constexpr auto s1 = LHCb::range::single<S>{ std::in_place, 2.5, 3, "HelloGoodbye" };
    static_assert( s1.size() == 1 );
    static_assert( !s1.empty() );
  }
}

BOOST_AUTO_TEST_CASE( range_chunker ) {

  std::vector<int> vi( 16, 0 );
  std::iota( vi.begin(), vi.end(), 0 );
  std::string result;
  for ( auto c : LHCb::range::chunk( vi, 4 ) ) {
    for ( auto i : c ) result += std::to_string( i );
    result += '\n';
  }
  std::string expected = "0123\n4567\n891011\n12131415\n";
  BOOST_CHECK( result == expected );

  std::string s = "aaabbbcccdddee";
  auto        e = std::array{ "aaa", "bbb", "ccc", "ddd", "ee" };
  int         i = 0;
  for ( auto c : LHCb::range::chunk( s, 3 ) ) {
    std::string r;
    for ( auto cc : c ) r.push_back( cc );
    BOOST_CHECK( r == e[i] );
    ++i;
  }
}

BOOST_AUTO_TEST_CASE( enumerate ) {
  char x[] = "Hello";
  for ( auto [i, c] :
        LHCb::range::enumerate( x ) ) { // note: enumerate returns a tuple of (possible const) _references_
                                        // - and thus i and c are bound to those _references_
    // ++i; // should not, and does not, compile
    if ( i == 3 ) c = 'X'; // this really modifies x[3]
  }
  BOOST_CHECK( std::string_view{ "HelXo" } == x );

  for ( auto [i, s] : LHCb::range::enumerate( std::array{ 0, 1, 4, 9, 16, 25, 36 } ) ) {
    BOOST_CHECK( i * i == static_cast<size_t>( s ) );
    // s+=1;    does not, and should not, compile
  }

  int j = 9;
  for ( auto [i, s] : LHCb::range::enumerate( std::array{ 99, 98, 97 }, j ) ) {
    BOOST_CHECK( ( i == 9 && s == 99 ) || ( i == 10 && s == 98 ) || ( i == 11 && s == 97 ) );
  }
  BOOST_CHECK( j == 9 );
  for ( auto [i, s] : LHCb::range::enumerate( std::array{ 99, 98, 97 }, std::ref( j ) ) ) {
    BOOST_CHECK( ( i == 9 && s == 99 ) || ( i == 10 && s == 98 ) || ( i == 11 && s == 97 ) );
  }
  BOOST_CHECK( j == 12 );
}
