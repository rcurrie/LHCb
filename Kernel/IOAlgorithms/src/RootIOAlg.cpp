/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include <IOAlgorithms/RootIOAlgBase.h>
#include <IOAlgorithms/RootIOHandler.h>

#include <optional>

namespace LHCb::IO {

  /**
   * Algorithms dealing with Root IO and reading a set of files one by one
   *
   * Implementation is ostly part of RootIOAlgBase, so see documentation there,
   * in particular why it's not as nicely functional as it seems and how to
   * configure it.
   */
  template <bool ReadAllBranches = false>
  class RootIOAlg : public RootIOAlgBase<ReadAllBranches> {
  public:
    RootIOAlg( const std::string& name, ISvcLocator* pSvcLocator )
        : RootIOAlgBase<ReadAllBranches>( name, pSvcLocator ) {}

    StatusCode initialize() override;
    StatusCode finalize() override {
      m_ioHandler->finalize();
      return RootIOAlgBase<ReadAllBranches>::finalize();
    };
    StatusCode start() override {
      auto hadStopped = this->m_hasStopped;
      return RootIOAlgBase<ReadAllBranches>::start().andThen( [&] {
        if ( hadStopped ) { m_ioHandler->rewind( *this ); }
      } );
    };

    typename RootIOHandler<ReadAllBranches>::EventType nextEvent( EventContext const& evtCtx ) const override;

  private:
    Gaudi::Property<std::vector<std::string>> m_input{ this, "Input", {}, "List of inputs" };
    // Note that IOHandler::next is thread safe, so the usage of mutable is safe
    mutable std::optional<RootIOHandler<ReadAllBranches>> m_ioHandler{};
  };

} // namespace LHCb::IO

template <bool ReadAllBranches>
StatusCode LHCb::IO::RootIOAlg<ReadAllBranches>::initialize() {
  return RootIOAlgBase<ReadAllBranches>::initialize().andThen( [&]() {
    m_ioHandler.emplace( *this, m_input.value() );
    // initialize IOHandler
    m_ioHandler->initialize( *this, &*this->m_incidentSvc, this->m_bufferNbEvents.value(),
                             this->m_nbSkippedEvents.value(), this->m_eventTreeName.value(),
                             this->m_eventBranches.value(), this->m_allowMissingInput.value(),
                             this->m_storeOldFSRs.value(), this->m_evtPrintFrequency.value() );
  } );
}

template <bool ReadAllBranches>
typename LHCb::IO::RootIOHandler<ReadAllBranches>::EventType
LHCb::IO::RootIOAlg<ReadAllBranches>::nextEvent( EventContext const& evtCtx ) const {
  return m_ioHandler->next( *this, evtCtx );
}

DECLARE_COMPONENT_WITH_ID( LHCb::IO::RootIOAlg<false>, "RootIOAlg" )
DECLARE_COMPONENT_WITH_ID( LHCb::IO::RootIOAlg<true>, "RootIOAlgExt" )
