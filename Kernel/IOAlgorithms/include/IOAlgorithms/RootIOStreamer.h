/*****************************************************************************\
* (c) Copyright 2000-2024 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <GaudiKernel/MsgStream.h>

namespace LHCb::IO {

  /**
   * Singleton object allowing to easily patch the ROOT streamer
   * so that SmartRefs and ContainedObjects are supported by the
   * ROOT I/O machinery
   *
   * Note that this implementation is not thread safe, or ate least
   * the streamer may be patched several times in case this is used
   * in a threaded context.
   */
  struct RootIOStreamer {
    /// whether the streamer was already patched or not
    static bool patched;
    /// constructor, patching if not already done
    RootIOStreamer( MsgStream& s );
  };

} // namespace LHCb::IO
