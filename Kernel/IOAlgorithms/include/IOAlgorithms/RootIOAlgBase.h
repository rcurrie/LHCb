/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "RootIOHandler.h"
#include "RootIOStreamer.h"

#include <LHCbAlgs/Transformer.h>

#include <Gaudi/Parsers/Factory.h>
#include <GaudiKernel/DataObjectHandle.h>

#include <string>

namespace LHCb::IO {

  /**
   * Base Algorithm dealing with Root IO
   *
   * Although this algorithm looks nicely functional, it actually only declares
   * its buffer as output while the data are dynamically put in the TES outside
   * of the functional framework, using DataHandles created on the fly.
   * This allows to avoid a compile time dependency on the number of outputs.
   *
   * Configuration of this algorithms involves setting the EventBranches
   * Property holding a list of the branches to be loaded from the ROOT file(s),
   * under the form of a vector of string pairs : Property Name and path for
   * each branch. The property name is used to create dynamically a property
   * of the algorithm so that the TES output location can be accessed from
   * python. Each branch in the Root file(s) should contain an object inheriting
   * from DataObject and this object will be loaded into the transient store
   * in a path matching the branch name.
   *
   * The template paramater ReadAllBranches allows to read all branches from
   * the input file and fill the TES with them. However only branches present
   * in m_eventBranches will be outputs of the algorithm and have DataHandles
   * associated.
   * This should only be used in conjunction with CopyInputStream and should be
   * dropped when CopyInputStream is replaced. FIXME
   *
   * This is a Base algorithm, as it actually does not open any file and does not
   * read any event. It must be subclassed for this and method nextEvent must be
   * implemented. See RootIOAlg and RootIOMixer classes for practical examples
   */
  template <bool ReadAllBranches = false>
  class RootIOAlgBase : public Algorithm::Transformer<std::shared_ptr<IO::details::Buffer>( EventContext const& )> {
  public:
    RootIOAlgBase( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator, { { "EventBufferLocation", "" } } ), m_streamer( info() ) {}

    StatusCode initialize() override;
    StatusCode stop() override {
      m_hasStopped = true;
      return Transformer::stop();
    };
    StatusCode start() override {
      return Transformer::start().andThen( [&] {
        if ( m_hasStopped ) { m_hasStopped = false; }
      } );
    };

    std::shared_ptr<IO::details::Buffer> operator()( EventContext const& evtCtx ) const override;

    /**
     * pure virtual method to be implemented for a concrete RootIOAlg. It should return the next
     * event to handle. See RootIOAlg and RootIOMixer classes for practical examples
     */
    virtual typename RootIOHandler<ReadAllBranches>::EventType nextEvent( EventContext const& evtCtx ) const = 0;

  protected:
    Gaudi::Property<unsigned int> m_bufferNbEvents{
        this, "BufferNbEvents", 1000,
        "approximate size of the buffer used to prefetch rawbanks in terms of number of events. Default is 1000" };
    Gaudi::Property<unsigned int>             m_nbSkippedEvents{ this, "NSkip", 0, "First event to process" };
    Gaudi::Property<std::string>              m_eventTreeName{ this, "EventTreeName", "Event",
                                                  "Name of the tree containing Events in the Root files" };
    Gaudi::Property<std::vector<std::string>> m_eventBranches{
        this,
        "EventBranches",
        {},
        "Algorithm property name and path for each branch to be retrieved from the Root files" };
    Gaudi::Property<bool> m_allowMissingInput{
        this, "AllowMissingInput", false,
        "Allows to ignore missing inputs. Should never be used and should even be dropped, but was needed for the "
        "transition period form FetchDataFromFile to IOAlg" };
    // only used if ReadAllBranches is True. Drop when removing the template FIXME
    DataObjectWriteHandle<std::vector<DataObject*>> m_inputLeavesLocation{
        this, "InputLeavesLocation", "/Event/InputFileLeaves",
        "Place where to put list of input leaves when reading all branches. Used by CopyInputStream" };
    // only used if ReadAllBranches is True. Drop when removing the template FIXME
    Gaudi::Property<std::vector<std::string>> m_ignorePaths{
        this, "IgnorePaths", {}, "Set of paths to ignore when copying everything from Input file" };
    // should we deal with old FSRs ? FIXME should be removed when old FSRs are gone
    Gaudi::Property<bool> m_storeOldFSRs{
        this, "StoreOldFSRs", false,
        "Will store the old FSRs in a dedicated transient store using the FileRecordDataSvc" };
    // Frequency for printing info on events processed
    Gaudi::Property<int> m_evtPrintFrequency{ this, "PrintFreq", 1000, "printout frequency" };

    // vector of datahandles created dynamically in initialize, one per requested branch
    std::vector<DataObjectHandle<DataObject>> m_dataHandles;

    // incident service to be used. FIXME : drop whenever FSRSink does not need this anymore
    ServiceHandle<IIncidentSvc> m_incidentSvc{ this, "IncidentSvc", "IncidentSvc", "the incident service" };

    // remembers if this algorithm has just been called on stop(), so that we rewind in case of restart
    bool m_hasStopped{ false };

    // allows to patch the streamer of ROOT to support SmartRefs and ContainedObjects at the ROOT level
    RootIOStreamer m_streamer;
  };

} // namespace LHCb::IO

template <bool ReadAllBranches>
StatusCode LHCb::IO::RootIOAlgBase<ReadAllBranches>::initialize() {
  return Transformer::initialize().andThen( [&]() {
    // Create dynamically DataHandles for each branch requested
    for ( auto& branch : m_eventBranches ) {
      // add a new DataHandle for this branch
      m_dataHandles.emplace_back( branch, Gaudi::DataHandle::Writer, this );
    }
  } );
}

template <bool ReadAllBranches>
std::shared_ptr<LHCb::IO::details::Buffer>
LHCb::IO::RootIOAlgBase<ReadAllBranches>::operator()( EventContext const& evtCtx ) const {
  auto&& [eventData, buffer] = nextEvent( evtCtx );
  std::vector<DataObject*> allLeaves;
  // fill TES
  if constexpr ( ReadAllBranches ) {
    // has to come first as we are also having "directory" branches here which may be
    // auto created by filling the TES with m_dataHandles. In such case doing this
    // after the other would fail for these cases as they already exist
    buffer->fillExtraData( *evtSvc(), eventData.second, allLeaves, m_ignorePaths );
  }
  for ( auto&& [handle, data] : ranges::views::zip( m_dataHandles, eventData.first ) ) {
    handle.put( std::unique_ptr<DataObject>( data ) );
    if ( std::find( begin( m_ignorePaths ), end( m_ignorePaths ), handle.fullKey().key() ) == end( m_ignorePaths ) ) {
      allLeaves.push_back( data );
    }
  }
  // fill the special InputFileLeavesLocation with the set of branches to save
  // this replaces FetchLeavesFromFile for the RootIOAlg case
  m_inputLeavesLocation.put( std::move( allLeaves ) );
  // return shared pointer to buffer, which will be stored in TES via functional machinary
  return buffer;
}
