/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/IOFSR.h"
#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/DataStoreItem.h"
#include "GaudiKernel/IDataManagerSvc.h"
#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiKernel/IOpaqueAddress.h"
#include "GaudiKernel/IRegistry.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/SmartDataPtr.h"
#include "GaudiKernel/SmartIF.h"
#include "IIOFSRSvc.h"
#include "LHCbRecordStream.h"

/** @class LHCbFSRStream LHCbFSRStream.h
 * Extension of RecordStream to add IOFsr and automatically clean up the TES
 *
 *
 * @author  R.Lambert
 * @version 1.0
 */
class LHCbFSRStream : public LHCbRecordStream {
public:
  /// Standard algorithm Constructor
  LHCbFSRStream( const std::string&, ISvcLocator* );
  /// Algorithm overload: initialization
  StatusCode initialize() override;
  /// Algorithm overload: finalization
  StatusCode finalize() override;
  /// Runrecords do not get written for each event: Event processing hence dummy....
  StatusCode execute() override { return StatusCode::SUCCESS; }

protected:
  /// clean up empty nodes before writing
  void cleanNodes( DataObject* obj, const std::string& location, unsigned int nRecCount = 0 );

private:
  SmartIF<IIOFSRSvc> m_ioFsrSvc;         ///< reference to IOFSR service
  bool               m_doIOFsr   = true; // set to false to skip all the IOFSR stuff
  bool               m_cleanTree = true; // set to false to skip cleaning the FSR tree
  std::string        m_cleanRoot;        // clean from where in the FSR tree
};

// Define the algorithm factory for the standard output data writer
DECLARE_COMPONENT( LHCbFSRStream )

// Standard Constructor
LHCbFSRStream::LHCbFSRStream( const std::string& name, ISvcLocator* pSvcLocator )
    : LHCbRecordStream( name, pSvcLocator ) {
  declareProperty( "FSRCleaningDirectory", m_cleanRoot = "/FileRecords" );
  declareProperty( "CleanTree", m_cleanTree = true );
  declareProperty( "AddIOFSR", m_doIOFsr = true );
}

StatusCode LHCbFSRStream::initialize() {
  StatusCode sc = LHCbRecordStream::initialize();
  if ( !sc.isSuccess() ) return sc;
  if ( !m_doIOFsr ) return sc;

  m_ioFsrSvc = serviceLocator()->service( "IOFSRSvc" );
  if ( !m_ioFsrSvc ) {
    error() << "Error retrieving IOFSRSvc." << endmsg;
    return StatusCode::FAILURE;
  }

  return sc;
}

StatusCode LHCbFSRStream::finalize() {
  if ( m_doIOFsr ) {

    // clean any existing top-level FSR
    StatusCode sc = m_ioFsrSvc->cleanTopFSR();
    // it probably isn't there...
    sc.ignore();

    // add the IOFSR to the TES
    sc = m_ioFsrSvc->storeIOFSR( m_outputName );
    if ( !sc.isSuccess() ) { error() << "Error storing IOFSR." << endmsg; }
  }

  // TODO cleanup the existing store, removing all empty directories
  if ( m_cleanTree ) {
    // Try and load the root DataObject for the configured stream
    SmartDataPtr<DataObject> root( m_pDataProvider, m_cleanRoot );

    // if found, recursively clean
    if ( root ) cleanNodes( root, m_cleanRoot );
  }

  m_ioFsrSvc.reset();
  return LHCbRecordStream::finalize();
}

void LHCbFSRStream::cleanNodes( DataObject* obj, const std::string& location, unsigned int nRecCount ) {
  // protect against infinite recursion
  if ( ++nRecCount > 99999 ) {
    error() << "Maximum recursion limit reached...." << endmsg;
    return;
  }

  SmartIF<IDataManagerSvc> mgr( m_pDataManager );

  typedef std::vector<IRegistry*> Leaves;
  Leaves                          leaves;

  // Load the leaves
  StatusCode sc = mgr->objectLeaves( obj, leaves );
  if ( sc ) {

    if ( !leaves.empty() ) {
      for ( Leaves::const_iterator iL = leaves.begin(); leaves.end() != iL; ++iL ) {
        const std::string& id = ( *iL )->identifier();
        DataObject*        tmp( NULL );
        sc = m_pDataProvider->findObject( id, tmp );
        if ( sc && tmp ) {
          if ( CLID_DataObject == tmp->clID() ) { cleanNodes( tmp, id, nRecCount ); }
        }
      }
      // Load again, after cleaning, to see what remains
      sc = mgr->objectLeaves( obj, leaves );
    }

    if ( sc && leaves.empty() ) {
      debug() << "Removing node " << location << endmsg;
      sc = m_pDataProvider->unlinkObject( location );
    }
  }
}
