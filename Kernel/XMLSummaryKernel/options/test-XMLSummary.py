###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Set of tests around XMLSummarySvc
# will read and write a Dst file
# Will fill some counters, and output the summary.xml

import os

from PyConf.Algorithms import LegacyEventCountAlg, RecordStream
from PyConf.application import (
    ApplicationOptions,
    configure,
    configure_input,
    create_or_reuse_rootIOAlg,
    root_writer,
)
from PyConf.control_flow import CompositeNode


def runTest(
    options, algs=[LegacyEventCountAlg(name="TestCounter")], insertWrongFile=False
):
    options.set_input_and_conds_from_testfiledb("TestXMLSummary")
    if insertWrongFile:
        options.input_files.insert(0, "YouCannotOpenMe.dst")
    options.output_file = os.environ.get("XMLSUMM_TEST_OUT", "RootDst.root")
    options.output_type = "ROOT"
    options.evt_max = 10
    options.xml_summary_svc = "CounterSummarySvc"
    options.root_ioalg_opts = {"StoreOldFSRs": True}
    config = configure_input(options)

    writer = root_writer(options.output_file, ["/Event/#1"])
    FSRWriter = RecordStream(
        name="FRWriter",
        Output=options.output_file,
        ItemList=["/FileRecords#999"],
        EvtDataSvc="FileRecordDataSvc",
        EvtConversionSvc="FileRecordPersistencySvc",
    )

    cf_node = CompositeNode(
        "Sequence",
        children=[create_or_reuse_rootIOAlg(options)] + algs + [writer, FSRWriter],
    )

    extra_config = configure(options, cf_node)

    # amend the XMLSummarySvc and set OutputLevel and UpdateFreq
    summarySvc = extra_config["XMLSummarySvc/CounterSummarySvc"]
    summarySvc.UpdateFreq = 1
    summarySvc.OutputLevel = 1
    config.update(extra_config)


def plain():
    """test plain configuration"""
    options = ApplicationOptions(_enabled=False)
    options.xml_summary_file = "summary.xml"
    runTest(options)


def exception():
    """test behavior when throwing an exception after 3 events"""
    from PyConf.Algorithms import ThrowingCounter

    options = ApplicationOptions(_enabled=False)
    options.xml_summary_file = "summary-exception.xml"
    runTest(options, [ThrowingCounter(name="Counter")])


def exit():
    """test behavior when exiting"""
    from PyConf.Algorithms import ExitingCounter

    options = ApplicationOptions(_enabled=False)
    options.xml_summary_file = "summary-exit.xml"
    runTest(options, [ExitingCounter(name="Counter")])


def readError():
    """test behavior in case of read error"""
    options = ApplicationOptions(_enabled=False)
    options.xml_summary_file = "summary-ReadError.xml"
    runTest(options, insertWrongFile=True)


def writeError():
    """test behavior in case of write error"""
    options = ApplicationOptions(_enabled=False)
    options.xml_summary_file = "summary-WriteError.xml"
    runTest(options)


def _checkMemory(_, new):
    stat = new.find("stat")
    assert stat is not None, f"XMLSummary content error: missing child stat in usage."
    assert "useOf" in stat.keys() and stat.get("useOf") == "MemoryMaximum", (
        f"XMLSummary content error: expected useOf='MemoryMaximum' should be present in entry usage/stat."
    )


def _checkOutput(ref, new):
    fref = ref.find("file")
    fnew = new.find("file")
    output_file = os.environ.get("XMLSUMM_TEST_OUT", "RootDst.root")
    assert fnew is not None, (
        f"XMLSummary content error: missing child file: {fnew} in the output."
    )
    assert output_file in fnew.get("name"), (
        f"XMLSummary content error: wrong output file {fnew.get('name')}, expected RootDst.root."
    )
    assert fnew.text == fref.text, (
        f"XMLSummary content error: difference in output file usage : {fnew.text}, expected {fref.text}"
    )


def _checkCounters(ref, new):
    def getTable(item):
        t = {}
        for child in item:
            t[child.get("name")] = child.text
        return t

    tref = getTable(ref)
    tnew = getTable(new)
    assert tref == tnew, (
        f"XMLSummary encountered different set of counters: got {tnew}, expected {tref}"
    )


def _checkFiles(ref, new):
    refFiles = {refChild.get("UUID"): refChild for refChild in ref}
    newFiles = {newChild.get("UUID"): newChild for newChild in new}
    for uuid in refFiles:
        assert uuid in newFiles, f"Input file with UUID {uuid} is missing"

        _check(refFiles[uuid], newFiles[uuid])

    for uuid in newFiles:
        assert uuid in refFiles, f"Got extra input file with UUID {uuid}"


def _check(ref, new):
    handlers = {
        "usage": _checkMemory,
        "output": _checkOutput,
        "input": _check,
        "counters": _checkCounters,
    }
    # check attributes are the same
    for attr in ref.keys():
        refValue = ref.get(attr)
        value = new.get(attr)
        assert value is not None, (
            f"XMLSummary: Attribute {attr} missing in tag {ref.tag}"
        )
        assert value == refValue, (
            f"Attribute {attr} has unexpected value {value}, expected {refValue}"
        )
    # check content is the same
    assert ref.text == new.text, (
        f"XMLSummary content error: In entry {ref.tag}, expected {ref.text}, got {new.text}"
    )
    # check children are ok
    hasFile = False
    for refChild in ref:
        tag = refChild.tag
        newChild = new.find(tag)
        assert newChild is not None, f"XMLSummary: Entry {tag} missing."

        if tag == "file":
            _checkFiles(ref, new)
        elif tag in handlers:
            handlers[tag](refChild, newChild)
        else:
            assert refChild.text == newChild.text, (
                f"XMLSummary content error: In entry {tag}, expected {refChild.text}, got {newChild.text}"
            )


def check(refFile, outputFile):
    import xml.etree.ElementTree as ET

    ref = ET.parse(refFile).getroot()
    try:
        new = ET.parse(outputFile).getroot()
    except FileNotFoundError:
        raise FileNotFoundError("Expected output file (XMLSummary) missing.")
    _check(ref, new)
