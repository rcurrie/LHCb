###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import json
import os
from multiprocessing import Process
from pathlib import Path

from LHCbTesting import LHCbExeTest

FILENAMEFSR = "test_periodic_flush.fsr.json"
FILENAME = "test_periodic_flush.root"
FILENAMEJSON = "test_periodic_flush.json"


class Test(LHCbExeTest):
    command = ["gaudirun.py", f"{__file__}:config"]

    def test_files_exist(self, cwd: Path):
        for name in [FILENAME, FILENAMEFSR, FILENAMEJSON]:
            assert os.path.exists(cwd / name)

    def test_files_content(self, cwd: Path):
        # we expect 6 copies of the FSR JSON dump, with increasing counts
        data = "FSR not found"
        for i in range(6):
            data = json.load(open(cwd / f"{FILENAMEFSR}.~{i}~"))
            assert data, "JSON.load of {FILENAMEFSR}.~{i}~ failed."
            expected = i
            found = data.get("EvtCounter.count", {}).get("nEntries")
            assert found == expected, (
                f"Invalid partial count. Expected {expected}, but found {found}."
            )


def keep_intermediate_files(filename):
    print("===> starting thread to copy FSR dumps", flush=True)
    from time import sleep, time

    wait_time = 0.01
    max_time = time() + 300
    counter = -1
    # when to stop looking for new FSR dumps after event 5
    # or if we have been running for too long
    while counter < 5 and time() < max_time:
        try:
            data = open(filename).read()
            parsed_data = json.loads(data)  # check that the file can be parser
            # we make a copy of the FSR file for each event, only if the output file guid is filled
            if not parsed_data.get("guid"):
                continue
            counter = parsed_data.get("EvtCounter.count", {}).get("nEntries", -1)
            if counter >= 0 and not os.path.exists(f"{filename}.~{counter}~"):
                with open(f"{filename}.~{counter}~", "w") as f:
                    f.write(data)
                    print("=== FSR begin ===")
                    print(data.rstrip())
                    print("=== FSR  end  ===")
                    print(f"---> wrote {filename}.~{counter}~", flush=True)
        except FileNotFoundError:
            pass
        except json.JSONDecodeError:
            continue  # the file is corrupted (it might be being written)

        sleep(wait_time)

    print("===> stopping thread to copy FSR dumps", flush=True)


def config():
    from Configurables import ApplicationMgr

    from PyConf.Algorithms import (
        EventCountAlg,
        Gaudi__Examples__IntDataProducer,
        GaudiTesting__SleepyAlg,
    )
    from PyConf.application import (
        ApplicationOptions,
        configure,
        configure_input,
        root_writer,
    )
    from PyConf.components import setup_component
    from PyConf.control_flow import CompositeNode

    options = ApplicationOptions(_enabled=False)
    # No data from the input is used, but something should be there for the configuration
    options.input_files = ["dummy_input_file_name.dst"]
    options.input_type = "ROOT"
    options.output_file = FILENAME
    options.output_type = "ROOT"
    options.data_type = "Upgrade"
    options.dddb_tag = "upgrade/dddb-20220705"
    options.conddb_tag = "upgrade/sim-20220705-vc-mu100"
    options.geometry_version = "run3/trunk"
    options.conditions_version = "master"
    options.simulation = True
    options.evt_max = 5
    options.monitoring_file = FILENAMEJSON
    # options.output_level = 2

    config = configure_input(options)

    app = ApplicationMgr()
    app.EvtSel = "NONE"  # ignore input configuration
    app.ExtSvc.append(
        config.add(
            setup_component(
                "LHCb__FSR__Sink",
                instance_name="FileSummaryRecord",
                AcceptRegex=r"^EvtCounter\.count$",
                OutputFile=FILENAMEFSR,
                AutoFlushPeriod=0.3,  # seconds
            )
        )
    )

    producer = Gaudi__Examples__IntDataProducer()

    # the ExtraOutputs options for SleepyAlg are not used, but there to please PyConf
    # (it does not allow two algorithm instances that differ only by the name)
    cf = CompositeNode(
        "test",
        [
            GaudiTesting__SleepyAlg(
                name="SleepBefore", SleepTime=1, ExtraOutputs=["Stamp1"]
            ),
            EventCountAlg(name="EvtCounter"),
            GaudiTesting__SleepyAlg(
                name="SleepAfter", SleepTime=1, ExtraOutputs=["Stamp2"]
            ),
            producer,
            root_writer(options.output_file, [producer.OutputLocation]),
        ],
    )
    app.OutStream.clear()
    config.update(configure(options, cf))

    # make sure the histogram file is not already there
    for name in [FILENAME, FILENAMEFSR, FILENAMEJSON] + [
        f"{FILENAMEFSR}.~{i}~" for i in range(10)
    ]:
        if os.path.exists(name):
            os.remove(name)

    # start a subprocess that tracks changes to FILENAMEFSR and keeps
    # back up versions of it
    Process(target=keep_intermediate_files, args=(FILENAMEFSR,)).start()
