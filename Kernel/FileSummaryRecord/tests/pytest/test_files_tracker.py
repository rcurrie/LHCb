###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import json
import os
from pathlib import Path

from LHCbTesting import LHCbExeTest
from PRConfig.TestFileDB import test_file_db

FILENAMEFSR = "test_files_tracker.fsr.json"
FILENAME = "test_files_tracker.root"
FILENAMEJSON = "test_files_tracker.json"


class Test(LHCbExeTest):
    command = ["gaudirun.py", f"{__file__}:config"]

    def test_files_exist(self, cwd: Path):
        for name in [FILENAME, FILENAMEFSR, FILENAMEJSON]:
            assert os.path.exists(cwd / name)

    def test_files_content(self, cwd: Path):
        inputFileName = test_file_db["2018_raw_full"].filenames[0]
        expected = {
            "EvtCounter.count": {
                "empty": False,
                "nEntries": 5,
                "type": "counter:Counter:m",
            },
            "FilesTracker.FileEvents": [
                {
                    "filename": inputFileName,
                    "id": inputFileName,
                    "type": "CONNECTED_INPUT",
                }
            ],
            "FilesTracker.OutputFileStats": {
                "test_files_tracker.root": {"status": "full", "writes": 5}
            },
        }

        import ROOT

        fsr_dump = json.load(open(cwd / FILENAMEFSR))
        f = ROOT.TFile.Open(str(cwd / FILENAME))
        fsr_root = json.loads(str(f.FileSummaryRecord))

        guid = fsr_dump.get("guid")
        assert guid, "Missing or invalid GUID in FSR dump."

        expected["guid"] = guid  # GUID is random

        assert fsr_dump == expected
        assert fsr_root == expected


def config():
    from Configurables import ApplicationMgr

    from PyConf.Algorithms import EventCountAlg, Gaudi__Examples__IntDataProducer
    from PyConf.application import (
        ApplicationOptions,
        configure,
        configure_input,
        create_or_reuse_mdfIOAlg,
        root_writer,
    )
    from PyConf.components import setup_component
    from PyConf.control_flow import CompositeNode

    options = ApplicationOptions(_enabled=False)
    options.set_input_from_testfiledb("2018_raw_full")
    options.output_file = FILENAME
    options.output_type = "ROOT"
    options.data_type = "Upgrade"
    options.dddb_tag = "upgrade/dddb-20220705"
    options.conddb_tag = "upgrade/sim-20220705-vc-mu100"
    options.geometry_version = "run3/trunk"
    options.conditions_version = "master"
    options.simulation = True
    options.evt_max = 5
    options.monitoring_file = FILENAMEJSON
    # options.output_level = 2

    config = configure_input(options)

    app = ApplicationMgr()
    app.ExtSvc.append(
        config.add(
            setup_component(
                "LHCb__FSR__Sink",
                instance_name="FileSummaryRecord",
                AcceptRegex=r"^(EvtCounter\.count|FilesTracker\..*)$",
                OutputFile=FILENAMEFSR,
            )
        )
    )
    app.ExtSvc.append(
        config.add(
            setup_component(
                "LHCb__FSR__OpenFilesTracker",
                instance_name="FilesTracker",
            )
        )
    )

    producer = Gaudi__Examples__IntDataProducer(name="IntDataProducer")
    # force IOAlg as the test expects it to be ran even if none of its output is used !
    ioAlg = create_or_reuse_mdfIOAlg("/Event/Raw/DAQ", options)

    cf = CompositeNode(
        "test",
        [
            ioAlg,
            EventCountAlg(name="EvtCounter"),
            producer,
            root_writer(options.output_file, [producer.OutputLocation]),
        ],
    )
    app.OutStream.clear()
    config.update(configure(options, cf))

    # make sure the histogram file is not already there
    for name in [FILENAME, FILENAMEFSR, FILENAMEJSON]:
        if os.path.exists(name):
            os.remove(name)
