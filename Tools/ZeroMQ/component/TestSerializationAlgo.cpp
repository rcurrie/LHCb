/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include <LHCbAlgs/Consumer.h>

#include <boost/functional/hash.hpp>

#include <ZeroMQ/IZeroMQSvc.h>
#include <ZeroMQ/SerializeSize.h>

#include <array>
#include <map>
#include <string>
#include <tuple>
#include <unordered_set>
#include <vector>

/**
 *  @author Roel Aaij
 *  @date   2016-06-01
 */
class TestSerializationAlgo : public LHCb::Algorithm::Consumer<void()> {
public:
  using Consumer::Consumer;
  void operator()() const override;

  SmartIF<IZeroMQSvc>& zmq() const {
    if ( !m_zmqSvc ) { m_zmqSvc = service( "ZeroMQSvc" ).as<IZeroMQSvc>(); }
    return m_zmqSvc;
  }

private:
  mutable SmartIF<IZeroMQSvc> m_zmqSvc;

  template <class T, class Comp = std::equal_to<T>>
  void testEncodeDecode( const T& test, Comp comp = Comp{} ) const {
    auto msg = zmq()->encode( test );
    auto tmp = zmq()->decode<T>( msg );
    auto tn  = System::typeinfoName( typeid( T ) );
    if ( comp( test, tmp ) ) {
      info() << "Message size: " << msg.size() << " for " << tn << endmsg;
    } else {
      error() << "Encode decode test failed for " << tn << endmsg;
    }
  }
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( TestSerializationAlgo )

void TestSerializationAlgo::operator()() const {
  testEncodeDecode( 25ul );
  testEncodeDecode( 25. );
  testEncodeDecode<std::string>( "test_string" );
  testEncodeDecode<std::vector<int>>( { 1, 2, 3, 6 } );
  testEncodeDecode<std::array<int, 4>>( { 1, 2, 3, 6 } );
  testEncodeDecode<std::vector<std::tuple<int, double, std::string>>>(
      { { 1, 2., "one" }, { 2, 3., "two" }, { 3, 8., "three" }, { 5, 6., "four" } } );
  testEncodeDecode<std::map<int, std::string>>( { { 1, "one" }, { 2, "two" } } );
  testEncodeDecode<std::unordered_set<std::string>>( { "one", "five", "twelve", "twenty" } );
  using lspair = std::pair<unsigned long, std::string>;
  testEncodeDecode<std::unordered_set<lspair, boost::hash<lspair>>>(
      { { 1, "one" }, { 5, "five" }, { 12, "twelve" }, { 20, "twenty" } } );
}
