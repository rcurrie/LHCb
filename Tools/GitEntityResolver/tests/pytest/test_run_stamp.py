###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import re
from textwrap import dedent

import pytest
from LHCbTesting import LHCbExeTest


@pytest.mark.ctest_fixture_required("gitentityresolver.prepare")
@pytest.mark.shared_cwd("GitEntityResolver")
class Test(LHCbExeTest):
    command = ["gaudirun.py", "-v"]

    def options(self):
        from Configurables import (
            ApplicationMgr,
            DDDBConf,
            EventClockSvc,
            FakeEventTime,
            RunStampCheck,
        )
        from Gaudi.Configuration import DEBUG, allConfigurables, appendPostConfigAction
        from GitCondDB.TestOptions import setup

        setup(tag="HEAD")

        def ts(*args):
            from datetime import datetime

            epoch = datetime(1970, 1, 1)
            return int((datetime(*args) - epoch).total_seconds() * 1000000000)

        ecs = EventClockSvc(InitialTime=ts(2015, 6, 10, 12, 00))
        ecs.addTool(FakeEventTime, "EventTimeDecoder")
        ecs.EventTimeDecoder.StartTime = ts(2015, 6, 9, 12, 00)
        ecs.EventTimeDecoder.TimeStep = 24 * 60 * 60 * 1000000000  # 1 day
        ecs.EventTimeDecoder.StartRun = 100
        ecs.EventTimeDecoder.EventsPerRun = 1

        DDDBConf(EnableRunStampCheck=True)

        RunStampCheck(OutputLevel=DEBUG, ValidRunsList="git://valid_runs.txt")

        ApplicationMgr(EvtSel="NONE", EvtMax=4)

        # MessageSvc(OutputLevel = 1)

        def tweakVFS():
            from Gaudi.Configuration import VFSSvc

            vfs = VFSSvc()
            vfs.FileAccessTools = [
                vfs.FileAccessTools[0],
                allConfigurables["ToolSvc.GitDDDB"],
            ]

        appendPostConfigAction(tweakVFS)

    returncode = 4

    def test_expected_lines(self, stdout: bytes):
        ## Check that we find the expected lines in the right order
        expected = dedent("""\
        RunStampCheck       DEBUG opening git://valid_runs.txt
        RunStampCheck       DEBUG loaded 12 valid run numbers
        RunStampCheck       ERROR Database not up-to-date. No valid data for run 102 at 2015-06-11 12:00:00.0 UTC
        EventLoopMgr      SUCCESS Terminating event processing loop due to a stop scheduled by an incident listener
        EventLoopMgr      SUCCESS Terminating event processing loop due to scheduled stop
        """).splitlines()

        exp = re.compile(
            r"^(RunStampCheck.*(opening|loaded|Found|Database)|.*Terminating event processing)"
        )
        lines = [
            line.rstrip() for line in stdout.decode().splitlines() if exp.match(line)
        ]

        assert lines == expected, "Order of RunStampCheck lines not correct."
