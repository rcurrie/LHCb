###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import pytest
from conftest import GERTestHelper
from GaudiTesting import NO_ERROR_MESSAGES


@pytest.mark.ctest_fixture_required("gitentityresolver.prepare")
@pytest.mark.shared_cwd("GitEntityResolver")
class Test(GERTestHelper):
    command = ["gaudirun.py", "-v"]

    def options(self):
        from DetDescChecks.Options import LoadDDDBTest

        LoadDDDBTest("2016")

        from GitCondDB.TestOptions import setup

        setup(tag="v0", conditions=["/dd/Changing"])

        from Configurables import ApplicationMgr, EventClockSvc, FakeEventTime

        ecs = EventClockSvc()
        ecs.addTool(FakeEventTime, "EventTimeDecoder")
        # tuned from the content of /dd/Changing for tag v0
        ecs.EventTimeDecoder.StartTime = 1442403000000000000
        ecs.EventTimeDecoder.TimeStep = 18399600000000000

        ApplicationMgr(EvtMax=3)

    reference = {
        "messages_count": NO_ERROR_MESSAGES,
    }
    tag = "v0"
    condition_values = [
        ("Changing", "0.0 -> 1451602800.0", "(int) parameter = 0"),
        ("Changing", "1451602800.0 -> 1470002400.0", "(int) parameter = 1"),
        ("Changing", "1470002400.0 -> 9223372036.854775807", "(int) parameter = 0"),
    ]
