/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
// #include "Detector/Muon/TileID.h"
#include "Kernel/EventLocalAllocator.h"
#include "Kernel/STLExtensions.h"

#include "GaudiKernel/Range.h"

/** @class CommonMuonHit CommonMuonHit.h
 * A container for a muon hit that holds a reference to the corresponding tile
 *as well as information on x, y, z and corresponding errors.
 */
class MuonTell40PciInfo final {
public:
  MuonTell40PciInfo() = default;

  MuonTell40PciInfo( unsigned int tell40, unsigned int pci, unsigned int max_link, unsigned int connected,
                     unsigned int enabled, unsigned int BankType, unsigned int LinkInError )
      : m_tell40{ tell40 }
      , m_pci{ pci }
      , m_maxLinks{ max_link }
      , m_connectedLinks{ connected }
      , m_enabledLinks{ enabled }
      , m_BankType{ BankType }
      , m_LinkInError{ LinkInError } {}

  unsigned int Tell40Number() const { return m_tell40; }
  unsigned int PCINumber() const { return m_pci; }
  unsigned int maxLinks() const { return m_maxLinks; }
  bool         isConnected( unsigned int pos ) const { return ( m_connectedLinks >> pos ) & 0x1; }
  bool         isEnabled( unsigned int pos ) const { return ( m_enabledLinks >> pos ) & 0x1; }
  bool         isInError( unsigned int pos ) const { return ( m_LinkInError >> pos ) & 0x1; }
  unsigned int BankType() const { return m_BankType; }

private:
  unsigned int m_tell40         = 0;
  unsigned int m_pci            = 0;
  unsigned int m_maxLinks       = 0;
  unsigned int m_connectedLinks = 0;
  unsigned int m_enabledLinks   = 0;
  unsigned int m_BankType       = 0;
  unsigned int m_LinkInError    = 0;
};

using MuonTell40PciInfos = std::vector<MuonTell40PciInfo, LHCb::Allocators::EventLocal<MuonTell40PciInfo>>;
typedef std::vector<const MuonTell40PciInfo*>        ConstMuonTell40PciInfos;
typedef const Gaudi::Range_<MuonTell40PciInfos>      MuonTell40PciInfosRange;
typedef const Gaudi::Range_<ConstMuonTell40PciInfos> ConstMuonTell40PciInfosRange;
