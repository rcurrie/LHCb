###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from DDDB.CheckDD4Hep import UseDD4Hep

from PyConf.Algorithms import MuonRawInUpgradeToHits
from PyConf.application import (
    ApplicationOptions,
    configure,
    configure_input,
    default_raw_banks,
)
from PyConf.control_flow import CompositeNode

options = ApplicationOptions(_enabled=False)
options.set_input_and_conds_from_testfiledb("upgrade_Sept2022_minbias_0fb_md_xdigi")
options.evt_max = 100
config = configure_input(options)

if UseDD4Hep:
    dd4hepsvc = config["LHCb::Det::LbDD4hep::DD4hepSvc/LHCb::Det::LbDD4hep::DD4hepSvc"]
    dd4hepsvc.DetectorList = ["/world", "Muon"]

algs = [
    MuonRawInUpgradeToHits(
        name="MuonRawInUpgradeToHits",
        RawBanks=default_raw_banks("Muon"),
        ErrorRawBanks=default_raw_banks("MuonError"),
    )
]

config.update(configure(options, CompositeNode("muon_decoding", algs)))
