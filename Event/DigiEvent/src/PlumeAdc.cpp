/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/PlumeAdc.h"

#include <fmt/core.h>

std::ostream& LHCb::PlumeAdc::fillStream( std::ostream& os ) const {
  os << fmt::format( "Channel={:10}, ADC={:4}, OverTh={}", channelID().toString(), m_adc, m_overThreshold );
  return os;
}
