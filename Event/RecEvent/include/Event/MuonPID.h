/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
#include "Event/Track.h"
#include "GaudiKernel/KeyedContainer.h"
#include "GaudiKernel/KeyedObject.h"
#include "GaudiKernel/SharedObjectsContainer.h"
#include "GaudiKernel/SmartRef.h"
#include <ostream>
#include <vector>

// Forward declarations

namespace LHCb {

  // Forward declarations

  // Class ID definition
  static const CLID CLID_MuonPID = 11050;

  // Namespace for locations in TDS
  namespace MuonPIDLocation {
    inline const std::string Default = "Rec/Muon/MuonPID";
    inline const std::string Offline = "Rec/Muon/MuonPID";
    inline const std::string Hlt     = "Hlt/Muon/MuonPID";
  } // namespace MuonPIDLocation

  /** @class MuonPID MuonPID.h
   *
   * Stores the output of the Muon Identification in the muon system
   *
   * @author M. Gandelman, E. Polycarpo
   *
   */

  class MuonPID final : public KeyedObject<int> {
  public:
    /// typedef for std::vector of MuonPID
    using Vector      = std::vector<MuonPID*>;
    using ConstVector = std::vector<const MuonPID*>;

    /// typedef for KeyedContainer of MuonPID
    using Container = KeyedContainer<MuonPID, Containers::HashMap>;

    using Selection = SharedObjectsContainer<MuonPID>;
    using Range     = Gaudi::Range_<ConstVector>;

    /// Copy constructor. Creates a new MuonPID object with the same pid information
    MuonPID( const LHCb::MuonPID& lhs )
        : KeyedObject<int>()
        , m_MuonLLMu( lhs.m_MuonLLMu )
        , m_MuonLLBg( lhs.m_MuonLLBg )
        , m_NShared( lhs.m_NShared )
        , m_Status( lhs.m_Status )
        , m_chi2Corr( lhs.m_chi2Corr )
        , m_muonMVA1( lhs.m_muonMVA1 )
        , m_muonMVA2( lhs.m_muonMVA2 )
        , m_muonMVA3( lhs.m_muonMVA3 )
        , m_muonMVA4( lhs.m_muonMVA4 )
        , m_IDTrack( lhs.m_IDTrack )
        , m_muonTrack( lhs.m_muonTrack ) {}

    /// Default Constructor
    MuonPID() = default;

    // Retrieve pointer to class definition structure
    const CLID&        clID() const override { return LHCb::MuonPID::classID(); }
    static const CLID& classID() { return CLID_MuonPID; }

    /// Fill the ASCII output stream
    std::ostream& fillStream( std::ostream& s ) const override;

    /// Retrieve const  Muon Likelihood for muons
    double MuonLLMu() const { return m_MuonLLMu; }

    /// Update  Muon Likelihood for muons
    MuonPID& setMuonLLMu( double value ) {
      m_MuonLLMu = value;
      return *this;
    }

    /// Retrieve const  Muon likelihood for non muons
    double MuonLLBg() const { return m_MuonLLBg; }

    /// Update  Muon likelihood for non muons
    MuonPID& setMuonLLBg( double value ) {
      m_MuonLLBg = value;
      return *this;
    }

    /// Retrieve const  Number of tracks which share hits
    int nShared() const { return m_NShared; }

    /// Update  Number of tracks which share hits
    MuonPID& setNShared( int value ) {
      m_NShared = value;
      return *this;
    }

    /// Retrieve const  Status of MuonPID
    unsigned int Status() const { return m_Status; }

    /// Update  Status of MuonPID
    MuonPID& setStatus( unsigned int value ) {
      m_Status = value;
      return *this;
    }

    /// Retrieve Boolean: Is track a muon ?
    bool IsMuon() const { return 0 != ( ( m_Status & IsMuonMask ) >> IsMuonBits ); }

    /// Update Boolean: Is track a muon ?
    void setIsMuon( bool value );

    /// Retrieve Boolean: True if track extraoplation is in the Muon acceptance
    bool InAcceptance() const;

    /// Update Boolean: True if track extraoplation is in the Muon acceptance
    void setInAcceptance( bool value );

    /// Retrieve Boolean: True if track has minimal momentum
    bool PreSelMomentum() const;

    /// Update Boolean: True if track has minimal momentum
    void setPreSelMomentum( bool value );

    /// Retrieve Boolean: Is track a muon (looser defintion) ?
    bool IsMuonLoose() const;

    /// Update Boolean: Is track a muon (looser defintion) ?
    void setIsMuonLoose( bool value );

    /// Retrieve Boolean: Is track a muon (with x,y crossing requirement for muon hits) ?
    bool IsMuonTight() const;

    /// Update Boolean: Is track a muon (with x,y crossing requirement for muon hits) ?
    void setIsMuonTight( bool value );

    /// Retrieve const  chi2 using correlations of the hits
    float chi2Corr() const { return m_chi2Corr; }

    /// Update  chi2 using correlations of the hits
    MuonPID& setChi2Corr( float value ) {
      m_chi2Corr = value;
      return *this;
    }

    /// Retrieve const  multi variate algorithm for MuonID with tune1
    float muonMVA1() const { return m_muonMVA1; }

    /// Update  multi variate algorithm for MuonID with tune1
    MuonPID& setMuonMVA1( float value ) {
      m_muonMVA1 = value;
      return *this;
    }

    /// Retrieve const  multi variate algorithm for MuonID with tune2
    float muonMVA2() const { return m_muonMVA2; }

    /// Update  multi variate algorithm for MuonID with tune2
    MuonPID& setMuonMVA2( float value ) {
      m_muonMVA2 = value;
      return *this;
    }

    /// Retrieve const  multi variate algorithm for MuonID with tune3
    float muonMVA3() const { return m_muonMVA3; }

    /// Update  multi variate algorithm for MuonID with tune3
    MuonPID& setMuonMVA3( float value ) {
      m_muonMVA3 = value;
      return *this;
    }

    /// Retrieve const  multi variate algorithm for MuonID with tune4
    float muonMVA4() const { return m_muonMVA4; }

    /// Update  multi variate algorithm for MuonID with tune4
    MuonPID& setMuonMVA4( float value ) {
      m_muonMVA4 = value;
      return *this;
    }

    /// Retrieve (const)  The track that has been IDed by the Muon system
    const LHCb::Track* idTrack() const { return m_IDTrack; }

    /// Update  The track that has been IDed by the Muon system
    MuonPID& setIDTrack( SmartRef<LHCb::Track> value ) {
      m_IDTrack = std::move( value );
      return *this;
    }

    /// Retrieve (const)  The track segment as reconstructed in the Muon system
    const LHCb::Track* muonTrack() const { return m_muonTrack; }

    /// Update  The track segment as reconstructed in the Muon system
    MuonPID& setMuonTrack( SmartRef<LHCb::Track> value ) {
      m_muonTrack = std::move( value );
      return *this;
    }

    friend std::ostream& operator<<( std::ostream& str, const MuonPID& obj ) { return obj.fillStream( str ); }

  private:
    /// Offsets of bitfield Status
    enum StatusBits {
      IsMuonBits         = 0,
      InAcceptanceBits   = 1,
      PreSelMomentumBits = 2,
      IsMuonLooseBits    = 3,
      IsMuonTightBits    = 4
    };

    /// Bitmasks for bitfield Status
    enum StatusMasks {
      IsMuonMask         = 0x1L,
      InAcceptanceMask   = 0x2L,
      PreSelMomentumMask = 0x4L,
      IsMuonLooseMask    = 0x8L,
      IsMuonTightMask    = 0x10L
    };

    double                m_MuonLLMu{ -11.5 };  ///< Muon Likelihood for muons
    double                m_MuonLLBg{ 0.0 };    ///< Muon likelihood for non muons
    int                   m_NShared{ 0 };       ///< Number of tracks which share hits
    unsigned int          m_Status{ 0 };        ///< Status of MuonPID
    float                 m_chi2Corr{ 0.0 };    ///< chi2 using correlations of the hits
    float                 m_muonMVA1{ -999.0 }; ///< multi variate algorithm for MuonID with tune1
    float                 m_muonMVA2{ -999.0 }; ///< multi variate algorithm for MuonID with tune2
    float                 m_muonMVA3{ -999.0 }; ///< multi variate algorithm for MuonID with tune3
    float                 m_muonMVA4{ -999.0 }; ///< multi variate algorithm for MuonID with tune4
    SmartRef<LHCb::Track> m_IDTrack;            ///< The track that has been IDed by the Muon system
    SmartRef<LHCb::Track> m_muonTrack;          ///< The track segment as reconstructed in the Muon system

  }; // class MuonPID

  /// Definition of Keyed Container for MuonPID
  using MuonPIDs = MuonPID::Container;

} // namespace LHCb

// -----------------------------------------------------------------------------
// end of class
// -----------------------------------------------------------------------------

// Including forward declarations

inline std::ostream& LHCb::MuonPID::fillStream( std::ostream& s ) const {
  s << "{ "
    << "MuonLLMu :	" << (float)m_MuonLLMu << std::endl
    << "MuonLLBg :	" << (float)m_MuonLLBg << std::endl
    << "NShared :	" << m_NShared << std::endl
    << "Status :	" << m_Status << std::endl
    << "chi2Corr :	" << m_chi2Corr << std::endl
    << "muonMVA1 :	" << m_muonMVA1 << std::endl
    << "muonMVA2 :	" << m_muonMVA2 << std::endl
    << "muonMVA3 :	" << m_muonMVA3 << std::endl
    << "muonMVA4 :	" << m_muonMVA4 << std::endl
    << " }";
  return s;
}

inline void LHCb::MuonPID::setIsMuon( bool value ) {
  auto val = (unsigned int)value;
  m_Status &= ~IsMuonMask;
  m_Status |= ( ( ( (unsigned int)val ) << IsMuonBits ) & IsMuonMask );
}

inline bool LHCb::MuonPID::InAcceptance() const { return 0 != ( ( m_Status & InAcceptanceMask ) >> InAcceptanceBits ); }

inline void LHCb::MuonPID::setInAcceptance( bool value ) {
  auto val = (unsigned int)value;
  m_Status &= ~InAcceptanceMask;
  m_Status |= ( ( ( (unsigned int)val ) << InAcceptanceBits ) & InAcceptanceMask );
}

inline bool LHCb::MuonPID::PreSelMomentum() const {
  return 0 != ( ( m_Status & PreSelMomentumMask ) >> PreSelMomentumBits );
}

inline void LHCb::MuonPID::setPreSelMomentum( bool value ) {
  auto val = (unsigned int)value;
  m_Status &= ~PreSelMomentumMask;
  m_Status |= ( ( ( (unsigned int)val ) << PreSelMomentumBits ) & PreSelMomentumMask );
}

inline bool LHCb::MuonPID::IsMuonLoose() const { return 0 != ( ( m_Status & IsMuonLooseMask ) >> IsMuonLooseBits ); }

inline void LHCb::MuonPID::setIsMuonLoose( bool value ) {
  auto val = (unsigned int)value;
  m_Status &= ~IsMuonLooseMask;
  m_Status |= ( ( ( (unsigned int)val ) << IsMuonLooseBits ) & IsMuonLooseMask );
}

inline bool LHCb::MuonPID::IsMuonTight() const { return 0 != ( ( m_Status & IsMuonTightMask ) >> IsMuonTightBits ); }

inline void LHCb::MuonPID::setIsMuonTight( bool value ) {
  auto val = (unsigned int)value;
  m_Status &= ~IsMuonTightMask;
  m_Status |= ( ( ( (unsigned int)val ) << IsMuonTightBits ) & IsMuonTightMask );
}
