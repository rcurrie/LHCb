/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/Track.h"
#include "GaudiKernel/KeyedContainer.h"
#include "GaudiKernel/KeyedObject.h"
#include "GaudiKernel/SharedObjectsContainer.h"
#include "GaudiKernel/SmartRef.h"
#include <vector>

namespace LHCb {

  static const CLID CLID_GlobalChargedPID = 2007;

  namespace GlobalChargedPIDLocation {
    inline const std::string Default = "";
  }

  class GlobalChargedPID : public KeyedObject<int> {
  public:
    using Vector      = std::vector<GlobalChargedPID*>;
    using ConstVector = std::vector<const GlobalChargedPID*>;
    using Container   = KeyedContainer<GlobalChargedPID, Containers::HashMap>;
    using Selection   = SharedObjectsContainer<GlobalChargedPID>;
    using Range       = Gaudi::Range_<ConstVector>;

    enum ProbNN { Electron, Muon, Pion, Kaon, Proton, Deuteron, Ghost };

    // constructors
    GlobalChargedPID( GlobalChargedPID const& pid )
        : KeyedObject<int>()
        , m_elDLL( pid.m_elDLL )
        , m_muDLL( pid.m_muDLL )
        , m_kaDLL( pid.m_kaDLL )
        , m_prDLL( pid.m_prDLL )
        , m_deDLL( pid.m_deDLL )
        , m_probnne( pid.m_probnne )
        , m_probnnmu( pid.m_probnnmu )
        , m_probnnpi( pid.m_probnnpi )
        , m_probnnk( pid.m_probnnk )
        , m_probnnp( pid.m_probnnp )
        , m_probnnd( pid.m_probnnd )
        , m_probnnghost( pid.m_probnnghost )
        , m_IDTrack( pid.m_IDTrack ) {}

    GlobalChargedPID() = default;

    // Retrieve pointer to class definition structure
    const CLID&        clID() const override { return classID(); }
    static const CLID& classID() { return CLID_GlobalChargedPID; }

    const LHCb::Track* idTrack() const { return m_IDTrack; }
    GlobalChargedPID&  setIDTrack( SmartRef<LHCb::Track> value ) {
      m_IDTrack = std::move( value );
      return *this;
    }

    // getters
    auto CombDLLe() const { return m_elDLL; }
    auto CombDLLmu() const { return m_muDLL; }
    auto CombDLLpi() const { return 0.f; } // 0 by definition, also can't set
    auto CombDLLk() const { return m_kaDLL; }
    auto CombDLLp() const { return m_prDLL; }
    auto CombDLLd() const { return m_deDLL; }

    auto ProbNNe() const { return m_probnne; }
    auto ProbNNmu() const { return m_probnnmu; }
    auto ProbNNpi() const { return m_probnnpi; }
    auto ProbNNk() const { return m_probnnk; }
    auto ProbNNp() const { return m_probnnp; }
    auto ProbNNd() const { return m_probnnd; }
    auto ProbNNghost() const { return m_probnnghost; }

    // setters
    GlobalChargedPID& setCombDLLe( float value ) {
      m_elDLL = value;
      return *this;
    }
    GlobalChargedPID& setCombDLLmu( float value ) {
      m_muDLL = value;
      return *this;
    }
    GlobalChargedPID& setCombDLLk( float value ) {
      m_kaDLL = value;
      return *this;
    }
    GlobalChargedPID& setCombDLLp( float value ) {
      m_prDLL = value;
      return *this;
    }
    GlobalChargedPID& setCombDLLd( float value ) {
      m_deDLL = value;
      return *this;
    }

    GlobalChargedPID& setProbNNe( float value ) {
      m_probnne = value;
      return *this;
    }
    GlobalChargedPID& setProbNNmu( float value ) {
      m_probnnmu = value;
      return *this;
    }
    GlobalChargedPID& setProbNNpi( float value ) {
      m_probnnpi = value;
      return *this;
    }
    GlobalChargedPID& setProbNNk( float value ) {
      m_probnnk = value;
      return *this;
    }
    GlobalChargedPID& setProbNNp( float value ) {
      m_probnnp = value;
      return *this;
    }
    GlobalChargedPID& setProbNNd( float value ) {
      m_probnnd = value;
      return *this;
    }
    GlobalChargedPID& setProbNNghost( float value ) {
      m_probnnghost = value;
      return *this;
    }

    template <ProbNN type>
    GlobalChargedPID& setProbNN( float value ) {
      if constexpr ( type == ProbNN::Electron ) return setProbNNe( value );
      if constexpr ( type == ProbNN::Muon ) return setProbNNmu( value );
      if constexpr ( type == ProbNN::Pion ) return setProbNNpi( value );
      if constexpr ( type == ProbNN::Kaon ) return setProbNNk( value );
      if constexpr ( type == ProbNN::Proton ) return setProbNNp( value );
      if constexpr ( type == ProbNN::Deuteron ) return setProbNNd( value );
      if constexpr ( type == ProbNN::Ghost ) return setProbNNghost( value );
    }

  private:
    // Combined (summed) Log Likelihoods
    float m_elDLL{ DefaultCombDLL }; ///< Electron Log Likelihood
    float m_muDLL{ DefaultCombDLL }; ///< Muon Log Likelihood
    float m_kaDLL{ DefaultCombDLL }; ///< Kaon Log Likelihood
    float m_prDLL{ DefaultCombDLL }; ///< Proton Log Likelihood
    float m_deDLL{ DefaultCombDLL }; ///< Deuteron Log Likelihood
    // ProbNNs (ML-based classifier)
    float m_probnne{ DefaultProbNN };
    float m_probnnmu{ DefaultProbNN };
    float m_probnnpi{ DefaultProbNN };
    float m_probnnk{ DefaultProbNN };
    float m_probnnp{ DefaultProbNN };
    float m_probnnd{ DefaultProbNN };
    float m_probnnghost{ DefaultProbNN };
    // track the classifiers refers to
    SmartRef<LHCb::Track> m_IDTrack;

  public:
    static constexpr float DefaultCombDLL = 0.f;
    static constexpr float DefaultProbNN  = -1.f;
  };

  using GlobalChargedPIDs = GlobalChargedPID::Container;

} // namespace LHCb

/* hash builders used to create a unique identifier for storing underlying PID objects in
 * an anonymous but uniquely identifiable way in the TES for different ProtoParticle containers
 */
namespace GlobalPIDObjectHash {
  template <auto N>
  std::size_t combine( const std::array<std::size_t, N>& hashes ) {
    return std::accumulate( hashes.begin(), hashes.end(), std::size_t{ 0 }, []( std::size_t i, std::size_t h ) {
      return i ^ ( h + 0x9e3779b9 + ( i << 6 ) + ( i >> 2 ) );
    } );
  }
} // namespace GlobalPIDObjectHash

template <>
struct std::hash<LHCb::GlobalChargedPID> {
  std::size_t operator()( const LHCb::GlobalChargedPID& pid ) const noexcept {
    return GlobalPIDObjectHash::combine( std::array{
        std::hash<float>{}( pid.CombDLLe() ), std::hash<float>{}( pid.CombDLLmu() ),
        std::hash<float>{}( pid.CombDLLk() ), std::hash<float>{}( pid.CombDLLp() ),
        std::hash<float>{}( pid.CombDLLd() ), std::hash<float>{}( pid.ProbNNe() ), std::hash<float>{}( pid.ProbNNmu() ),
        std::hash<float>{}( pid.ProbNNpi() ), std::hash<float>{}( pid.ProbNNk() ), std::hash<float>{}( pid.ProbNNp() ),
        std::hash<float>{}( pid.ProbNNd() ), std::hash<float>{}( pid.ProbNNghost() ) } );
  }
};
