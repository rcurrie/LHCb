/*****************************************************************************\
* (c) Copyright 2020-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <tuple>
#include <type_traits>
#include <utility>

namespace LHCb::Event {

  namespace details {
    template <typename T, typename simd_t, typename Collection, typename = typename simd_t::mask_v>
    struct hasIsValid : std::false_type {};
    template <typename T, typename simd_t, typename Collection>
    struct hasIsValid<T, simd_t, Collection,
                      decltype( std::declval<T>().template isValid<simd_t, Collection>(
                          std::declval<Collection>(), std::declval<std::size_t>() ) )> : std::true_type {};
  } // namespace details

  // An SOAPath represents a path in a SOADataTree
  // a path is a sequence of steps, each step can be either a tag or an index
  // indices can be known at runtime or compiletime
  template <typename T, typename... Ts>
  struct SOAPath {
    using Tag = T; // the type of the first step in the path

    template <typename... ExtraTs>
    using extend_t = SOAPath<T, Ts..., ExtraTs...>;

    SOAPath( std::tuple<T, Ts...> t ) : m_indices{ t } {}

    [[nodiscard, gnu::always_inline]] inline auto next() const { // return the next steps in the path
      if constexpr ( sizeof...( Ts ) < 1 ) {
        return *this;
      } else {
        return next_impl( std::make_index_sequence<sizeof...( Ts )>() );
      }
    }

    [[nodiscard, gnu::always_inline]] inline std::size_t index() const { // get the value of the first step in the
                                                                         // path, usually an index
      return static_cast<std::size_t>( std::get<0>( m_indices ) );
    }

    // Check if a path is valid given a collection and some contiguous elements, returns a mask
    template <typename simd_t, typename Collection>
    [[nodiscard, gnu::always_inline]] inline auto isValid( const Collection& collection, std::size_t element ) {
      auto valid = simd_t::mask_true();
      if constexpr ( details::hasIsValid<T, simd_t, Collection>::value ) {
        valid = std::get<0>( m_indices ).template isValid<simd_t>( collection, element );
      }
      if constexpr ( sizeof...( Ts ) < 1 ) {
        return valid;
      } else {
        return valid && next().template isValid<simd_t>( collection, element );
      }
    }

    template <typename... IdxTs>
    [[nodiscard, gnu::always_inline]] inline auto append( IdxTs... Is ) const { // append some indices to the path
#pragma GCC diagnostic push
#ifndef __clang__
// hide gcc 12 warning (false positive?)
#  pragma GCC diagnostic ignored "-Wmaybe-uninitialized"
#endif
      return SOAPath<T, Ts..., IdxTs...>{ std::tuple_cat( m_indices, std::make_tuple( Is... ) ) };
#pragma GCC diagnostic pop
    }

    template <typename Tag, typename... IdxTs>
    [[nodiscard, gnu::always_inline]] inline auto append( IdxTs... Is ) const { // append a tag and some indices to the
                                                                                // path
#pragma GCC diagnostic push
#ifndef __clang__
// hide gcc 12 warning (false positive?)
#  pragma GCC diagnostic ignored "-Wmaybe-uninitialized"
#endif
      return SOAPath<T, Ts..., Tag, IdxTs...>{ std::tuple_cat( m_indices, std::make_tuple( Tag{}, Is... ) ) };
#pragma GCC diagnostic pop
    }

  private:
    template <std::size_t... Is>
    constexpr auto next_impl( std::index_sequence<Is...> ) const {
      return SOAPath<Ts...>{ std::make_tuple( std::get<1 + Is>( m_indices )... ) };
    }
    const std::tuple<T, Ts...> m_indices;
  };

  // The packing algorithm assumes that the size will always be packed before the elements
  // TODO: add a compile time check to ensure this
  template <typename PathToSize>
  struct SOAPathIfSmallerThan {
    SOAPathIfSmallerThan( std::size_t i, const PathToSize path ) : index( i ), path( path ) {}

    explicit operator std::size_t() const { return index; }

    template <typename simd_t, typename Collection>
    [[nodiscard, gnu::always_inline]] inline auto isValid( const Collection& collection,
                                                           std::size_t       offset ) const noexcept {
      using I = typename simd_t::int_v;
      return I{ static_cast<int>( index ) } < I{ &collection.data( path )[offset] };
    }

    std::size_t      index;
    const PathToSize path;
  };
} // namespace LHCb::Event
