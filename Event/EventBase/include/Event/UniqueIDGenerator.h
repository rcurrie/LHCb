/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include <atomic>
#include <limits>
#include <stdexcept>
#include <string>

#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>

#include "GaudiKernel/Kernel.h"
#include "LHCbMath/SIMDWrapper.h"

namespace LHCb {

  // Namespace for locations in TES
  namespace UniqueIDGeneratorLocation {
    inline const std::string Default = "Rec/UniqueIDGenerator";
  }

  /** @class UniqueIDGenerator UniqueIDGenerator.h
   *
   * Unique object ID information. This object is put in the TES in
   * order to have an unique identifier (thread-safe, index-based) of any
   * reconstructed object. For each event being processed an object of
   * this type should be put, with the starting index at zero. There is
   * currently no way of avoiding having two of these instances in the TES
   * living at different locations. The processing sequences must ensure
   * that this object is unique even across different executions.
   *
   * Examples of when the IDs might not be valid:
   *
   * - Two instances of this generator are created at different locations
   * in the same execution unit. e.g. running the reconstruction where
   * Long and Downstream tracks are created using different instances of
   * the ID generator, which will make them share some IDs.
   *
   * - Instances of ID generator in subsequent runs. e.g. running the
   * reconstruction twice replacing the instance of the ID generator
   * present in the TES by a new one, or placing it somewhere else and
   * using that.
   *
   * @author Miguel Ramos Pernas
   *
   */
  class UniqueIDGenerator final {
  public:
    /// Must be a type compatible with SIMD integral objects
    using count_type = int;

    /// Typed ID
    template <class IntTypeSIMD>
    class ID final {

    public:
      using underlying_type = IntTypeSIMD;

      ID( underlying_type id, boost::uuids::uuid generator_tag ) : m_id{ id }, m_generator_tag{ generator_tag } {}
      ID( ID&& )                 = default;
      ID& operator=( ID&& )      = default;
      ID( ID const& )            = default;
      ID& operator=( ID const& ) = default;

      /// The actual value of the ID
      auto const& value() const { return m_id; }
      /// The ID of the generator, obtained from the hash of its TES location
      auto const& generator_tag() const { return m_generator_tag; }
      /// Comparison operator
      auto operator==( ID const& other ) const {
        if ( other.m_generator_tag != m_generator_tag )
          throw std::runtime_error( "Comparing IDs of two different generators" );
        return m_id == other.m_id;
      }
      /// Comparison operator
      auto operator!=( ID const& other ) const { return !( ( *this ) == other ); }

    protected:
      /// Value of the ID
      underlying_type m_id;
      /// ID of the generator
      boost::uuids::uuid m_generator_tag;
    };

    /// The default constructor initializes the object with an unique ID
    UniqueIDGenerator() : m_tag{ boost::uuids::random_generator()() } {}

    // The atomic types do not implement move constructors
    UniqueIDGenerator( UniqueIDGenerator&& other ) : m_tag{ other.m_tag }, m_value{ other.m_value.load() } {};

    // Allow move elision
    UniqueIDGenerator& operator=( UniqueIDGenerator&& other ) {
      m_value = other.m_value.load();
      m_tag   = other.m_tag;
      return *this;
    }

    /** Generate and ID of the SIMD type of the instruction set
     *
     * Every time this function is called, the internal index is incremented
     * depending on the size of the SIMD type on a thread-safe manner (using
     * @ref std::atomic).
     */
    template <class IntTypeSIMD>
    ID<IntTypeSIMD> generate() const {

      if constexpr ( std::is_same_v<IntTypeSIMD, int> ) {

        auto id = shift<0>();

        if ( id == std::numeric_limits<count_type>::max() )
          throw std::overflow_error{ "too many IDs requested in this event" };

        return ID{ id, m_tag };
      } else {

        auto id = generate_with_shift<IntTypeSIMD>( std::make_index_sequence<IntTypeSIMD::size()>() );

        // use !none instead of any, see https://github.com/root-project/root/issues/8370
        if ( !none( id == IntTypeSIMD{ std::numeric_limits<count_type>::max() } ) )
          throw std::overflow_error{ "too many IDs requested in this event" };

        return ID{ id, m_tag };
      }
    }

    /// Unique ID of the object
    boost::uuids::uuid const& tag() const { return m_tag; }

  private:
    /// Unique identifier of the generator
    boost::uuids::uuid m_tag;
    /// Internal global ID
    mutable std::atomic<count_type> m_value{ 0 };

    /// Shift the internal count by the size of the SIMD type
    template <std::size_t I>
    count_type shift() const {
      return ++m_value;
    }

    /// Implementation of the function to generate identifiers
    template <class T, std::size_t... I>
    T generate_with_shift( std::index_sequence<I...> ) const {
      count_type values[sizeof...( I )] = { shift<I>()... };
      return T{ values };
    }
  };
} // namespace LHCb
