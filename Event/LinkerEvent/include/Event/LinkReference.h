/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef LinkerEvent_LinkReference_H
#define LinkerEvent_LinkReference_H 1

// Include files
#include <ostream>

// Forward declarations

namespace LHCb {

  // Forward declarations

  /** @class LinkReference LinkReference.h
   *
   * Entry to get a reference to a keyed object
   *
   * @author Olivier Callot
   * created Thu Mar 28 08:35:26 2019
   *
   */

  class LinkReference final {
  public:
    /// Default Constructor
    LinkReference() = default;

    /// Constructor with arguments
    LinkReference( int srcLinkID, int linkID, int key, int nextIndex, float weight )
        : m_srcLinkID{ short( srcLinkID ) }
        , m_linkID{ short( linkID ) }
        , m_objectKey{ key }
        , m_nextIndex{ nextIndex }
        , m_weight{ weight } {}

    /// Fill the ASCII output stream
    std::ostream& fillStream( std::ostream& s ) const {
      return s << "{ srcLinkID :	" << m_srcLinkID << "\nlinkID :	" << m_linkID << "\nobjectKey :	" << m_objectKey
               << "\nnextIndex :	" << m_nextIndex << "\nweight :	" << m_weight << "\n }";
    }

    /// Retrieve const  index of the source sontainer in the LinkManager
    short srcLinkID() const { return m_srcLinkID; }

    /// Update  index of the source sontainer in the LinkManager
    void setSrcLinkID( short value ) { m_srcLinkID = value; }

    /// Retrieve const  index of the target container in the LinkManager
    short linkID() const { return m_linkID; }

    /// Update  index of the target container in the LinkManager
    void setLinkID( short value ) { m_linkID = value; }

    /// Retrieve const  key of the object in the container
    int objectKey() const { return m_objectKey; }

    /// Update  key of the object in the container
    void setObjectKey( int value ) { m_objectKey = value; }

    /// Retrieve const  index of the next entry
    int nextIndex() const { return m_nextIndex; }

    /// Update  index of the next entry
    void setNextIndex( int value ) { m_nextIndex = value; }

    /// Retrieve const  weight of the reference
    float weight() const { return m_weight; }

    /// Update  weight of the reference
    void setWeight( float value ) { m_weight = value; }

    friend std::ostream& operator<<( std::ostream& str, const LinkReference& obj ) { return obj.fillStream( str ); }

  private:
    short m_srcLinkID = 0; ///< index of the source sontainer in the LinkManager
    short m_linkID    = 0; ///< index of the target container in the LinkManager
    int   m_objectKey = 0; ///< key of the object in the container
    int   m_nextIndex = 0; ///< index of the next entry
    float m_weight    = 0; ///< weight of the reference

  }; // class LinkReference

} // namespace LHCb

#endif /// LinkerEvent_LinkReference_H
