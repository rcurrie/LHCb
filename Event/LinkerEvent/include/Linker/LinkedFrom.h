/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef LINKER_LINKEDFROM_H
#define LINKER_LINKEDFROM_H 1

#include "Event/LinksByKey.h"
#include "GaudiKernel/ContainedObject.h"
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/IRegistry.h"
#include "GaudiKernel/LinkManager.h"
#include "GaudiKernel/ObjectContainerBase.h"
#include "GaudiKernel/SmartDataPtr.h"
#include "fmt/format.h"

/** @class LinkedFrom LinkedFrom.h Linker/LinkedFrom.h
 *
 *
 *  @author Olivier Callot
 *  @date   2004-01-06
 */

template <class SOURCE>
class LinkedFrom final {
  enum struct Weighted { yes, no };
  template <Weighted weighted>
  class FromRange {
    friend LinkedFrom;
    struct Sentinel {};
    class Iterator {
      friend FromRange;
      SOURCE const*                                    m_current = nullptr;
      LHCb::LinksByKey const*                          m_links   = nullptr;
      LHCb::LinkReference                              m_curReference{ 0, 0, 0, -1, 0 };
      std::vector<std::pair<int, int>>::const_iterator m_srcIterator;
      long                                             m_wantedKey = -1;

      SOURCE const* source( int index ) {
        assert( m_links );
        LinkManager::Link const* link = m_links->linkMgr()->link( m_curReference.srcLinkID() );
        if ( !link->object() ) {
          SmartDataPtr<DataObject> tmp( m_links->registry()->dataSvc(), link->path() );
          const_cast<LinkManager::Link*>( link )->setObject( tmp );
        }
        auto* container = dynamic_cast<ObjectContainerBase const*>( link->object() );
        return container ? static_cast<SOURCE const*>( container->containedObject( index ) ) : nullptr;
      }
      Iterator( LHCb::LinksByKey const* links, ContainedObject const* target )
          : m_links{ links }, m_wantedKey{ target ? target->index() : -1 } {
        if ( !m_links ) return;
        //== check that the target's container is known.
        const DataObject*        container = target->parent();
        LinkManager::Link const* link      = m_links->linkMgr()->link( container );
        if ( !link ) { // try with name, and store pointer if OK
          link = m_links->linkMgr()->link( container->registry()->identifier() );
          if ( link ) const_cast<LinkManager::Link*>( link )->setObject( container );
        }
        if ( !link ) return;

        //== Define the target's linkID and Index
        m_curReference.setLinkID( link->ID() );
        m_curReference.setObjectKey( m_wantedKey );
        int index = m_links->firstSource( m_curReference, m_srcIterator );
        m_current = ( m_wantedKey == m_curReference.objectKey() ? source( index ) : nullptr );
      }

    public:
      using difference_type = int;
      using value_type      = void;
      using pointer         = void;
      using reference = std::conditional_t<weighted == Weighted::yes, std::tuple<SOURCE const&, float>, SOURCE const&>;
      using iterator_category = std::forward_iterator_tag;
      bool      operator!=( Sentinel ) const { return m_current != nullptr; }
      bool      operator==( Sentinel ) const { return m_current == nullptr; }
      Iterator& operator++() {
        if ( m_current ) {
          int index = m_links->nextSource( m_curReference, m_srcIterator );
          m_current = ( m_wantedKey == m_curReference.objectKey() ? source( index ) : nullptr );
        }
        return *this;
      }
      reference operator*() const {
        if constexpr ( weighted == Weighted::yes ) {
          return { *m_current, m_curReference.weight() };
        } else {
          return *m_current;
        }
      }
    };

    Iterator m_begin;
    template <typename TARGET>
    FromRange( LHCb::LinksByKey const* links, TARGET const* target ) : m_begin{ links, target } {}

  public:
    Iterator begin() const { return m_begin; }
    Sentinel end() const { return {}; }
    int      size() const {
      int i = 0;
      for ( auto b = begin(); b.m_current; ++b ) ++i;
      return i;
    }
    bool          empty() const { return m_begin.m_current == nullptr; }
    SOURCE const& front() const { return *m_begin.m_current; }
    SOURCE const* try_front() const { return m_begin.m_current; }
  };

public:
  /// Standard constructor
  LinkedFrom( LHCb::LinksByKey const* links ) : m_links{ links } { check_requirements(); }

  operator bool() const { return m_links != nullptr; }

  /** returns a range for the specified target
   */
  template <typename TARGET>
  auto weightedRange( const TARGET* target ) const {
    check_target( TARGET::classID() );
    return FromRange<Weighted::yes>{ m_links, target };
  }
  template <typename TARGET>
  auto range( const TARGET* target ) const {
    check_target( TARGET::classID() );
    return FromRange<Weighted::no>{ m_links, target };
  }

private:
  void check_requirements() const {
    if ( !m_links ) return; // FIXME: this ought to be an error... but it is not for 'backwards compatibility'...
    if ( !m_links->registry() ) {
      throw GaudiException( "LinksByKey not registered in event store", "LinkedTo", StatusCode::FAILURE );
    }
    if ( !m_links->registry()->dataSvc() ) {
      throw GaudiException( "LinksByKey could not obtain dataSvc", "LinkedTo", StatusCode::FAILURE );
    }
    if ( m_links->sourceClassID() != SOURCE::classID() && CLID_ContainedObject != SOURCE::classID() ) {
      throw GaudiException(
          fmt::format( "Incompatible SOURCE type for location {} @ {} : Template classID is {} expected {}",
                       m_links->registry()->identifier(), (const void*)m_links, SOURCE::classID(),
                       m_links->sourceClassID() ),
          "LinkedFrom", StatusCode::FAILURE );
    }
  }
  void check_target( const CLID& target_classid ) const {
    if ( !m_links ) return; // FIXME: this ought to be an error... but it is not for 'backwards compatibility'...
    if ( m_links->targetClassID() != target_classid && CLID_ContainedObject != target_classid ) {
      throw GaudiException(
          fmt::format( "Incompatible TARGET type for location {} : Template classID is {} expected {}",
                       m_links->registry()->identifier(), target_classid, m_links->targetClassID() ),
          "LinkedFrom", StatusCode::FAILURE );
    }
  }
  LHCb::LinksByKey const* m_links;
};

#endif // LINKER_LINKEDFROM_H
