###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Event/TrackEvent
----------------
#]=======================================================================]

gaudi_add_library(TrackEvent
    SOURCES
        src/ChiSquare.cpp
        src/GenerateSOATracks.cpp
        src/State.cpp
        src/StateVector.cpp
        src/Track_v1.cpp
        src/Track_v2.cpp
        src/TrackEnums.cpp
        src/TrackVertexUtils.cpp
    LINK
        PUBLIC
            Gaudi::GaudiKernel
            GSL::gsl
            LHCb::EventBase
            LHCb::FTDAQLib
            LHCb::UTDAQLib
            LHCb::MuonDAQLib
            LHCb::LHCbKernel
            LHCb::LHCbMathLib
            Rangev3::rangev3
)

gaudi_add_dictionary(TrackEventDict
    HEADERFILES dict/dictionary.h
    SELECTION dict/selection.xml
    LINK LHCb::TrackEvent
)

foreach(test IN ITEMS test_deref test_track_v2 test_prhits)
    gaudi_add_executable(${test}
        SOURCES tests/src/${test}.cpp
        LINK
            Boost::unit_test_framework
            LHCb::TrackEvent
        TEST
    )
endforeach()
