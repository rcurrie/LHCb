/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//------------------------------------------------------------------------------
/** @file TrackTypes.h
 *
 *  Track based typedefs
 *
 *  @author M. Needham
 *  @date   2005-1-09
 */

#pragma once
#include "GaudiKernel/GenericMatrixTypes.h"
#include "GaudiKernel/GenericVectorTypes.h"
#include "GaudiKernel/SymmetricMatrixTypes.h"

namespace Gaudi {
  // NB : Please remember to give a simple doxygen comment for each tyedef
  using TrackVector             = Vector5;      ///< Vector type for Track
  using TrackSymMatrix          = SymMatrix5x5; ///< Symmetrix matrix type for Track
  using TrackMatrix             = Matrix5x5;    ///< Matrix type for Track
  using TrackProjectionMatrix1D = Matrix1x5;    ///< 1D Projection matrix type for Track
  // FIXME Matrix2x5 alias is in Gaudi but not in v36r0, so use SMatrix until we use newer release
  using TrackProjectionMatrix2D = ROOT::Math::SMatrix<double, 2, 5>; ///< 1D Projection matrix type for Track

} // namespace Gaudi
