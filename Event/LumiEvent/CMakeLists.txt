###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Event/LumiEvent
---------------
#]=======================================================================]

gaudi_add_header_only_library(LumiEventLib
    LINK
        Gaudi::GaudiKernel
        LHCb::LHCbKernel
        nlohmann_json::nlohmann_json
)

gaudi_add_dictionary(LumiEventDict
    HEADERFILES dict/dictionary.h
    SELECTION dict/selection.xml
    LINK LHCb::LumiEventLib
)

if(BUILD_TESTING)
    gaudi_add_executable(test_LumiEventCounter
        SOURCES
            tests/src/test_LumiEventCounter.cpp
        LINK
            Boost::unit_test_framework
            LumiEventLib
        TEST
    )
endif()
