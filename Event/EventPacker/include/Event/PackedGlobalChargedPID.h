/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/GlobalChargedPID.h"
#include "Event/PackerBase.h"
#include "Event/StandardPacker.h"
#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/StatusCode.h"

namespace LHCb {

  struct PackedGlobalChargedPID {
    std::int32_t elDLL;
    std::int32_t muDLL;
    std::int32_t kaDLL;
    std::int32_t prDLL;
    std::int32_t deDLL;
    std::int32_t probnne;
    std::int32_t probnnmu;
    std::int32_t probnnpi;
    std::int32_t probnnk;
    std::int32_t probnnp;
    std::int32_t probnnd;
    std::int32_t probnnghost;
    std::int64_t key{ -1 };

#ifndef __CLING__
    template <typename Buf>
    void save( Buf& buf ) const {
      Packer::io( buf, *this );
    }
    template <typename Buf>
    void load( Buf& buf, unsigned int /*version*/ ) {
      Packer::io( buf, *this );
    }
#endif
  };

  constexpr CLID CLID_PackedGlobalChargedPIDs = 1574;

  /// Namespace for locations in TDS
  namespace PackedGlobalChargedPIDLocation {
    inline const std::string Default = "";
  } // namespace PackedGlobalChargedPIDLocation

  class PackedGlobalChargedPIDs : public DataObject {

  public:
    /// Vector of packed objects
    typedef std::vector<LHCb::PackedGlobalChargedPID> Vector;

    /// Default Packing Version
    static char defaultPackingVersion() { return 0; }

    /// Class ID
    static const CLID& classID() { return CLID_PackedGlobalChargedPIDs; }

    /// Class ID
    const CLID& clID() const override { return PackedGlobalChargedPIDs::classID(); }

    /// Write access to the data vector
    Vector& data() { return m_vect; }

    /// Read access to the data vector
    const Vector& data() const { return m_vect; }

    /// Access the packing version
    [[nodiscard]] char packingVersion() const { return m_packingVersion; }

    /// Describe serialization of object
    template <typename T>
    void save( T& buf ) const {
      buf.save( static_cast<uint8_t>( m_packingVersion ) );
      buf.save( static_cast<uint8_t>( version() ) );
      buf.save( m_vect );
    }

    /// Describe de-serialization of object
    template <typename T>
    void load( T& buf ) {
      m_packingVersion = buf.template load<uint8_t>();
      setVersion( buf.template load<uint8_t>() );
      buf.load( m_vect, m_packingVersion );
    }

    // Perform unpacking
    friend StatusCode unpack( Gaudi::Algorithm const*, const PackedGlobalChargedPIDs&, GlobalChargedPIDs& );

  private:
    /// Data packing version
    char m_packingVersion{ defaultPackingVersion() };

    /// The packed data objects
    Vector m_vect;
  };

  class GlobalChargedPIDPacker : public PackerBase {
  public:
    // These are required by the templated algorithms
    typedef LHCb::GlobalChargedPID        Data;
    typedef LHCb::PackedGlobalChargedPID  PackedData;
    typedef LHCb::GlobalChargedPIDs       DataVector;
    typedef LHCb::PackedGlobalChargedPIDs PackedDataVector;
    static const std::string&             packedLocation() { return LHCb::PackedGlobalChargedPIDLocation::Default; }
    static const std::string&             unpackedLocation() { return LHCb::GlobalChargedPIDLocation::Default; }
    constexpr inline static auto          propertyName() { return "GlobalChargedPIDs"; }

    using PackerBase::PackerBase;

    /// Pack GlobalChargedPID
    template <typename GlobalChargedPIDsRange>
    void pack( const GlobalChargedPIDsRange& pids, PackedDataVector& ppids ) const {
      const auto ver = ppids.packingVersion();
      if ( !isSupportedVer( ver ) ) return;
      ppids.data().reserve( pids.size() );
      for ( const auto* pid : pids ) pack( *pid, ppids.data().emplace_back(), ppids );
    }

    /// Unpack a single GlobalChargedPID
    StatusCode unpack( const PackedData& ppid, Data& pid, const PackedDataVector& ppids ) const;

    /// Unpack GlobalChargedPIDs
    StatusCode unpack( const PackedDataVector& ppids, DataVector& pids ) const;

    /// Compare two GlobalChargedPIDs to check the packing -> unpacking performance
    StatusCode check( const Data* dataA, const Data* dataB ) const;

  private:
    /// Pack a GlobalChargedPID
    void pack( const Data& pid, PackedData& ppid, PackedDataVector& ppids ) const;

    /// Check if the given packing version is supported
    [[nodiscard]] static bool isSupportedVer( const char ver ) {
      const bool OK = ( 0 == ver );
      if ( !OK ) {
        throw GaudiException( fmt::format( "Unknown packed data version {}", (int)ver ), "GlobalChargedPIDPacker",
                              StatusCode::FAILURE );
      }
      return OK;
    }
  };

} // namespace LHCb
