/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/PackedGlobalChargedPID.h"
#include "Event/PackedEventChecks.h"
#include <memory>

using namespace LHCb;

void GlobalChargedPIDPacker::pack( const Data& pid, PackedData& ppid, PackedDataVector& ppids ) const {
  const auto ver = ppids.packingVersion();
  if ( !isSupportedVer( ver ) ) { return; }
  // save the key
  ppid.key = pid.key();
  // save data
  ppid.elDLL       = StandardPacker::fltPacked( pid.CombDLLe() );
  ppid.muDLL       = StandardPacker::fltPacked( pid.CombDLLmu() );
  ppid.kaDLL       = StandardPacker::fltPacked( pid.CombDLLk() );
  ppid.prDLL       = StandardPacker::fltPacked( pid.CombDLLp() );
  ppid.deDLL       = StandardPacker::fltPacked( pid.CombDLLd() );
  ppid.probnne     = StandardPacker::fltPacked( pid.ProbNNe() );
  ppid.probnnmu    = StandardPacker::fltPacked( pid.ProbNNmu() );
  ppid.probnnpi    = StandardPacker::fltPacked( pid.ProbNNpi() );
  ppid.probnnk     = StandardPacker::fltPacked( pid.ProbNNk() );
  ppid.probnnp     = StandardPacker::fltPacked( pid.ProbNNp() );
  ppid.probnnd     = StandardPacker::fltPacked( pid.ProbNNd() );
  ppid.probnnghost = StandardPacker::fltPacked( pid.ProbNNghost() );
}

StatusCode GlobalChargedPIDPacker::unpack( const PackedData& ppid, Data& pid, const PackedDataVector& ppids ) const {
  const auto ver = ppids.packingVersion();
  if ( !isSupportedVer( ver ) ) { return StatusCode::FAILURE; }
  // unpack the data
  pid.setCombDLLe( (float)StandardPacker::fltPacked( ppid.elDLL ) );
  pid.setCombDLLmu( (float)StandardPacker::fltPacked( ppid.muDLL ) );
  pid.setCombDLLk( (float)StandardPacker::fltPacked( ppid.kaDLL ) );
  pid.setCombDLLp( (float)StandardPacker::fltPacked( ppid.prDLL ) );
  pid.setCombDLLd( (float)StandardPacker::fltPacked( ppid.deDLL ) );
  pid.setProbNNe( (float)StandardPacker::fltPacked( ppid.probnne ) );
  pid.setProbNNmu( (float)StandardPacker::fltPacked( ppid.probnnmu ) );
  pid.setProbNNpi( (float)StandardPacker::fltPacked( ppid.probnnpi ) );
  pid.setProbNNk( (float)StandardPacker::fltPacked( ppid.probnnk ) );
  pid.setProbNNp( (float)StandardPacker::fltPacked( ppid.probnnp ) );
  pid.setProbNNd( (float)StandardPacker::fltPacked( ppid.probnnd ) );
  pid.setProbNNghost( (float)StandardPacker::fltPacked( ppid.probnnghost ) );
  return StatusCode::SUCCESS;
}

StatusCode GlobalChargedPIDPacker::unpack( const PackedDataVector& ppids, DataVector& pids ) const {
  const auto ver = ppids.packingVersion();
  if ( !isSupportedVer( ver ) ) { return StatusCode::FAILURE; }
  pids.reserve( ppids.data().size() );
  StatusCode sc = StatusCode::SUCCESS;
  for ( const auto& ppid : ppids.data() ) {
    // make new pid in container
    auto pid = std::make_unique<Data>();
    // Fill data from packed object
    auto sc2 = unpack( ppid, *pid, ppids ).andThen( [&] { pids.insert( pid.release(), ppid.key ); } );
    if ( sc.isSuccess() ) sc = sc2;
  }
  return sc;
}

StatusCode GlobalChargedPIDPacker::check( const Data* dataA, const Data* dataB ) const {
  // checker
  const DataPacking::DataChecks ch( parent() );

  // assume OK from the start
  bool ok = true;

  ok &= ch.compareInts( "Key", dataA->key(), dataB->key() );
  ok &= ch.compareFloats( "CombDLLe", dataA->CombDLLe(), dataB->CombDLLe() );
  ok &= ch.compareFloats( "CombDLLmu", dataA->CombDLLmu(), dataB->CombDLLmu() );
  ok &= ch.compareFloats( "CombDLLk", dataA->CombDLLk(), dataB->CombDLLk() );
  ok &= ch.compareFloats( "CombDLLp", dataA->CombDLLp(), dataB->CombDLLp() );
  ok &= ch.compareFloats( "CombDLLd", dataA->CombDLLd(), dataB->CombDLLd() );
  ok &= ch.compareFloats( "ProbNNe", dataA->ProbNNe(), dataB->ProbNNe() );
  ok &= ch.compareFloats( "ProbNNmu", dataA->ProbNNmu(), dataB->ProbNNmu() );
  ok &= ch.compareFloats( "ProbNNpi", dataA->ProbNNpi(), dataB->ProbNNpi() );
  ok &= ch.compareFloats( "ProbNNk", dataA->ProbNNk(), dataB->ProbNNk() );
  ok &= ch.compareFloats( "ProbNNp", dataA->ProbNNp(), dataB->ProbNNp() );
  ok &= ch.compareFloats( "ProbNNd", dataA->ProbNNd(), dataB->ProbNNd() );
  ok &= ch.compareFloats( "ProbNNghost", dataA->ProbNNghost(), dataB->ProbNNghost() );

  // If comparison not OK, print full information
  if ( !ok ) {
    const std::string loc =
        ( dataA->parent() && dataA->parent()->registry() ? dataA->parent()->registry()->identifier() : "Not in TES" );
    parent().warning() << "Problem with GlobalChargedPID data packing :-" << endmsg
                       << "  Original PID key=" << dataA->key() << " in '" << loc << "'" << endmsg << dataA << endmsg
                       << "  Unpacked PID" << endmsg << dataB << endmsg;
  }

  return ( ok ? StatusCode::SUCCESS : StatusCode::FAILURE );
}

namespace LHCb {
  StatusCode unpack( Gaudi::Algorithm const* parent, const GlobalChargedPIDPacker::PackedDataVector& in,
                     GlobalChargedPIDPacker::DataVector& out ) {
    return GlobalChargedPIDPacker{ parent }.unpack( in, out );
  }
} // namespace LHCb
