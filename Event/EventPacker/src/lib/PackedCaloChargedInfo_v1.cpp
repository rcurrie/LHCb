/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/PackedCaloChargedInfo_v1.h"
#include "Event/PackedEventChecks.h"
#include <memory>

using namespace LHCb;

void CaloChargedPIDPacker::pack( const Data& pid, PackedData& ppid, PackedDataVector& ppids ) const {
  const auto ver = ppids.packingVersion();
  if ( !isSupportedVer( ver ) ) { return; }
  // save the key
  ppid.key = pid.key();
  // save data
  ppid.InAcc             = pid.InEcal() * 0x1 + pid.InHcal() * 0x2;
  ppid.ClusterID         = StandardPacker::caloCellIDPacked( pid.ClusterID() );
  ppid.ClusterMatch      = StandardPacker::fltPacked( pid.ClusterMatch() );
  ppid.ElectronID        = StandardPacker::caloCellIDPacked( pid.ElectronID() );
  ppid.ElectronMatch     = StandardPacker::fltPacked( pid.ElectronMatch() );
  ppid.ElectronEnergy    = StandardPacker::energy( pid.ElectronEnergy() );
  ppid.ElectronShowerEoP = StandardPacker::fltPacked( pid.ElectronShowerEoP() );
  ppid.ElectronShowerDLL = StandardPacker::deltaLL( pid.ElectronShowerDLL() );
  ppid.EcalPIDe          = StandardPacker::deltaLL( pid.EcalPIDe() );
  ppid.EcalPIDmu         = StandardPacker::deltaLL( pid.EcalPIDmu() );
  ppid.HcalEoP           = StandardPacker::fltPacked( pid.HcalEoP() );
  ppid.HcalPIDe          = StandardPacker::deltaLL( pid.HcalPIDe() );
  ppid.HcalPIDmu         = StandardPacker::deltaLL( pid.HcalPIDmu() );
}

StatusCode CaloChargedPIDPacker::unpack( const PackedData& ppid, Data& pid, const PackedDataVector& ppids ) const {
  const auto ver = ppids.packingVersion();
  if ( !isSupportedVer( ver ) ) { return StatusCode::FAILURE; }
  // unpack the data
  pid.setInEcal( ppid.InAcc & 0x1 );
  pid.setClusterID( StandardPacker::caloCellIDPacked( ppid.ClusterID ) );
  pid.setClusterMatch( (float)StandardPacker::fltPacked( ppid.ClusterMatch ) );
  pid.setElectronID( StandardPacker::caloCellIDPacked( ppid.ElectronID ) );
  pid.setElectronMatch( (float)StandardPacker::fltPacked( ppid.ElectronMatch ) );
  pid.setElectronEnergy( (float)StandardPacker::energy( ppid.ElectronEnergy ) );
  pid.setElectronShowerEoP( (float)StandardPacker::fltPacked( ppid.ElectronShowerEoP ) );
  pid.setElectronShowerDLL( (float)StandardPacker::deltaLL( ppid.ElectronShowerDLL ) );
  pid.setEcalPIDe( (float)StandardPacker::deltaLL( ppid.EcalPIDe ) );
  pid.setEcalPIDmu( (float)StandardPacker::deltaLL( ppid.EcalPIDmu ) );
  pid.setInHcal( ( ppid.InAcc & 0x2 ) );
  pid.setHcalEoP( (float)StandardPacker::fltPacked( ppid.HcalEoP ) );
  pid.setHcalPIDe( (float)StandardPacker::deltaLL( ppid.HcalPIDe ) );
  pid.setHcalPIDmu( (float)StandardPacker::deltaLL( ppid.HcalPIDmu ) );

  return StatusCode::SUCCESS;
}

StatusCode CaloChargedPIDPacker::unpack( const PackedDataVector& ppids, DataVector& pids ) const {
  const auto ver = ppids.packingVersion();
  if ( !isSupportedVer( ver ) ) { return StatusCode::FAILURE; }
  pids.reserve( ppids.data().size() );
  StatusCode sc = StatusCode::SUCCESS;
  for ( const auto& ppid : ppids.data() ) {
    // make new pid in container
    auto pid = std::make_unique<Data>();
    // Fill data from packed object
    auto sc2 = unpack( ppid, *pid, ppids ).andThen( [&] { pids.insert( pid.release(), ppid.key ); } );
    if ( sc.isSuccess() ) sc = sc2;
  }
  return sc;
}

StatusCode CaloChargedPIDPacker::check( const Data* dataA, const Data* dataB ) const {
  // checker
  const DataPacking::DataChecks ch( parent() );

  // assume OK from the start
  bool ok = true;

  ok &= ch.compareInts( "Key", dataA->key(), dataB->key() );
  ok &= ch.compareInts( "InEcal", dataA->InEcal(), dataB->InEcal() );
  ok &= ch.compareInts( "ClusterID", dataA->ClusterID().all(), dataB->ClusterID().all() );
  ok &= ch.compareFloats( "ClusterMatch", dataA->ClusterMatch(), dataB->ClusterMatch() );
  ok &= ch.compareInts( "ElectronID", dataA->ElectronID().all(), dataB->ElectronID().all() );
  ok &= ch.compareFloats( "ElectronMatch", dataA->ElectronMatch(), dataB->ElectronMatch() );
  ok &= ch.compareEnergies( "ElectronEnergy", dataA->ElectronEnergy(), dataB->ElectronEnergy() );
  ok &= ch.compareFloats( "ElectronShowerEoP", dataA->ElectronShowerEoP(), dataB->ElectronShowerEoP() );
  ok &= ch.compareDeltaLLs( "ElectronShowerDLL", dataA->ElectronShowerDLL(), dataB->ElectronShowerDLL() );
  ok &= ch.compareDeltaLLs( "EcalPIDe", dataA->EcalPIDe(), dataB->EcalPIDe() );
  ok &= ch.compareDeltaLLs( "EcalPIDmu", dataA->EcalPIDmu(), dataB->EcalPIDmu() );
  ok &= ch.compareInts( "InHcal", dataA->InHcal(), dataB->InHcal() );
  ok &= ch.compareFloats( "HcalEoP", dataA->HcalEoP(), dataB->HcalEoP() );
  ok &= ch.compareDeltaLLs( "HcalPIDe", dataA->HcalPIDe(), dataB->HcalPIDe() );
  ok &= ch.compareDeltaLLs( "HcalPIDmu", dataA->HcalPIDmu(), dataB->HcalPIDmu() );

  // force printout for tests
  // ok = false;
  // If comparison not OK, print full information
  if ( !ok ) {
    const std::string loc =
        ( dataA->parent() && dataA->parent()->registry() ? dataA->parent()->registry()->identifier() : "Not in TES" );
    parent().warning() << "Problem with CaloChargedPID data packing :-" << endmsg
                       << "  Original PID key=" << dataA->key() << " in '" << loc << "'" << endmsg << dataA << endmsg
                       << "  Unpacked PID" << endmsg << dataB << endmsg;
  }

  return ( ok ? StatusCode::SUCCESS : StatusCode::FAILURE );
}

void BremInfoPacker::pack( const Data& pid, PackedData& ppid, PackedDataVector& ppids ) const {
  const auto ver = ppids.packingVersion();
  if ( !isSupportedVer( ver ) ) { return; }
  // save the key
  ppid.key = pid.key();
  // save data
  ppid.BremAcc               = pid.InBrem() * 0x1 + pid.HasBrem() * 0x2;
  ppid.BremHypoID            = StandardPacker::caloCellIDPacked( pid.BremHypoID() );
  ppid.BremHypoMatch         = StandardPacker::fltPacked( pid.BremHypoMatch() );
  ppid.BremHypoEnergy        = StandardPacker::energy( pid.BremHypoEnergy() );
  ppid.BremHypoDeltaX        = StandardPacker::deltaLL( pid.BremHypoDeltaX() );
  ppid.BremTrackBasedEnergy  = StandardPacker::energy( pid.BremTrackBasedEnergy() );
  ppid.BremBendingCorrection = StandardPacker::fraction( pid.BremBendingCorrection() );
  ppid.BremEnergy            = StandardPacker::energy( pid.BremEnergy() );
  ppid.BremPIDe              = StandardPacker::deltaLL( pid.BremPIDe() );
}

StatusCode BremInfoPacker::unpack( const PackedData& ppid, Data& pid, const PackedDataVector& ppids ) const {
  const auto ver = ppids.packingVersion();
  if ( !isSupportedVer( ver ) ) { return StatusCode::FAILURE; }
  // unpack the data
  pid.setInBrem( ppid.BremAcc & 0x1 );
  pid.setBremHypoID( Detector::Calo::CellID( bit_cast<unsigned>( ppid.BremHypoID ) ) );
  pid.setBremHypoMatch( (float)StandardPacker::fltPacked( ppid.BremHypoMatch ) );
  pid.setBremHypoEnergy( (float)StandardPacker::energy( ppid.BremHypoEnergy ) );
  pid.setBremHypoDeltaX( (float)StandardPacker::deltaLL( ppid.BremHypoDeltaX ) );
  pid.setBremTrackBasedEnergy( (float)StandardPacker::energy( ppid.BremTrackBasedEnergy ) );
  pid.setBremBendingCorrection( (float)StandardPacker::fraction( ( ppid.BremBendingCorrection ) ) );
  pid.setHasBrem( ppid.BremAcc & 0x2 );
  pid.setBremEnergy( (float)StandardPacker::energy( ppid.BremEnergy ) );
  pid.setBremPIDe( (float)StandardPacker::deltaLL( ppid.BremPIDe ) );

  return StatusCode::SUCCESS;
}

StatusCode BremInfoPacker::unpack( const PackedDataVector& ppids, DataVector& pids ) const {
  const auto ver = ppids.packingVersion();
  if ( !isSupportedVer( ver ) ) { return StatusCode::FAILURE; }
  pids.reserve( ppids.data().size() );
  StatusCode sc = StatusCode::SUCCESS;
  for ( const auto& ppid : ppids.data() ) {
    // make new pid in container
    auto pid = std::make_unique<Data>();
    // Fill data from packed object
    auto sc2 = unpack( ppid, *pid, ppids ).andThen( [&] { pids.insert( pid.release(), ppid.key ); } );
    if ( sc.isSuccess() ) sc = sc2;
  }
  return sc;
}

StatusCode BremInfoPacker::check( const Data* dataA, const Data* dataB ) const {
  // checker
  const DataPacking::DataChecks ch( parent() );

  // assume OK from the start
  bool ok = true;

  ok &= ch.compareInts( "Key", dataA->key(), dataB->key() );
  ok &= ch.compareInts( "InBrem", dataA->InBrem(), dataB->InBrem() );
  ok &= ch.compareInts( "BremHypoID", dataA->BremHypoID().all(), dataB->BremHypoID().all() );
  ok &= ch.compareFloats( "BremHypoMatch", dataA->BremHypoMatch(), dataB->BremHypoMatch() );
  ok &= ch.compareEnergies( "BremHypoEnergy", dataA->BremHypoEnergy(), dataB->BremHypoEnergy() );
  ok &= ch.compareDeltaLLs( "BremHypoDeltaX", dataA->BremHypoDeltaX(), dataB->BremHypoDeltaX() );
  ok &= ch.compareEnergies( "BremTrackBasedEnergy", dataA->BremTrackBasedEnergy(), dataB->BremTrackBasedEnergy() );
  ok &= ch.compareFractions( "BremBendingCorrection", dataA->BremBendingCorrection(), dataB->BremBendingCorrection() );
  ok &= ch.compareInts( "HasBrem", dataA->HasBrem(), dataB->HasBrem() );
  ok &= ch.compareEnergies( "BremEnergy", dataA->BremEnergy(), dataB->BremEnergy() );
  ok &= ch.compareDeltaLLs( "BremPIDe", dataA->BremPIDe(), dataB->BremPIDe() );

  // force printout for tests
  // ok = false;
  // If comparison not OK, print full information
  if ( !ok ) {
    const std::string loc =
        ( dataA->parent() && dataA->parent()->registry() ? dataA->parent()->registry()->identifier() : "Not in TES" );
    parent().warning() << "Problem with BremInfo data packing :-" << endmsg << "  Original PID key=" << dataA->key()
                       << " in '" << loc << "'" << endmsg << dataA << endmsg << "  Unpacked PID" << endmsg << dataB
                       << endmsg;
  }

  return ( ok ? StatusCode::SUCCESS : StatusCode::FAILURE );
}

namespace LHCb {
  StatusCode unpack( Gaudi::Algorithm const* parent, const CaloChargedPIDPacker::PackedDataVector& in,
                     CaloChargedPIDPacker::DataVector& out ) {
    return CaloChargedPIDPacker{ parent }.unpack( in, out );
  }
  StatusCode unpack( Gaudi::Algorithm const* parent, const BremInfoPacker::PackedDataVector& in,
                     BremInfoPacker::DataVector& out ) {
    return BremInfoPacker{ parent }.unpack( in, out );
  }
} // namespace LHCb
