/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/PackedParticle.h"
#include "Event/PackedEventChecks.h"

#include "fmt/format.h"
#include "fmt/ostream.h"

#if FMT_VERSION >= 90000
template <>
struct fmt::formatter<LHCb::Particle::additionalInfo> : ostream_formatter {};
#endif

namespace {
  template <typename TYPE>
  static auto sqrt_or_0( const TYPE x ) {
    return ( x > TYPE( 0 ) ? std::sqrt( x ) : TYPE( 0 ) );
  }

  // ParticlePacker converts the XPE matrix to XMP representation, because that better reproduces the error on the mass.
  // However, if the mass is too small (like for a photon), then the conversion does not work. Therefore, we apply a
  // threshold. As long as the same mass is used in packer and unpacker, it does not matter. The latter is guaranteed by
  // using in the packer the 'unpacked p4' for the conversion.
  template <typename PELorentzVector>
  double packingmass( const PELorentzVector& p4 ) {
    constexpr double threshold  = 1.0 / LHCb::Packer::ENERGY_SCALE;
    constexpr double threshold2 = threshold * threshold;
    const double     m2         = p4.M2();
    return m2 < threshold2 ? 0.0 : std::sqrt( m2 );
  }

  /// transform a covariance matrix in the position-momentum-energy basis to the position-momentum-mass basis
  template <typename SymMatrix4x4, typename Matrix4x4, typename PELorentzVector>
  void convertToXPM( SymMatrix4x4& momcov, Matrix4x4& momposcov, const PELorentzVector& p4 ) {
    // This is a formulation that perhaps anyone can understand:
    //   Gaudi::Matrix4x4 jacobian ; // (P,E) --> (P,M)
    //   jacobian(0,0) = jacobian(1,1) = jacobian(2,2) = 1 ;
    //   jacobian(3,0) = -p4.Px() / mass ;
    //   jacobian(3,1) = -p4.Py() / mass ;
    //   jacobian(3,2) = -p4.Pz() / mass ;
    //   jacobian(3,3) =  p4.E() / mass ;
    //   // Sim(H,C) = H * C * H^T
    //   momcov = ROOT::Math::Similarity( jacobian, momcov ) ;
    //   momposcov = jacobian * momposcov;
    // But this one computes 3x less elements by exploiting the 0s and 1s:
    using Matrix1x4 = ROOT::Math::SMatrix<double, 1, 4>;
    Matrix1x4 jacobian{}; // only the M-row of the jacobian for (P,E) --> (P,M)
    auto      mass = packingmass( p4 );
    if ( mass > 0 ) {
      jacobian( 0, 0 ) = -p4.Px() / mass;
      jacobian( 0, 1 ) = -p4.Py() / mass;
      jacobian( 0, 2 ) = -p4.Pz() / mass;
      jacobian( 0, 3 ) = p4.E() / mass;
      Matrix1x4 JC     = jacobian * momcov;
      momcov( 3, 3 )   = ( JC * ROOT::Math::Transpose( jacobian ) )( 0, 0 );
      // If the masscov is so small that it would pack to zero, make sure all correlation elements are zero as well.
      constexpr auto masscovscale = LHCb::Packer::ENERGY_SCALE * LHCb::Packer::ENERGY_SCALE;
      if ( momcov( 3, 3 ) * masscovscale > 1 ) {
        for ( int i = 0; i < 3; ++i ) momcov( 3, i ) = JC( 0, i );
        momposcov.Place_at( jacobian * momposcov, 3, 0 );
      } else {
        for ( int i = 0; i < 4; ++i ) momcov( 3, i ) = 0;
        momposcov.Place_at( ROOT::Math::SMatrix<double, 1, 3>{}, 3, 0 );
      }
    } else {
      for ( int i = 0; i < 4; ++i ) momcov( 3, i ) = 0;
      momposcov.Place_at( ROOT::Math::SMatrix<double, 1, 3>{}, 3, 0 );
    }
  }

  /// transform a covariance matrix in the position-momentum-mass basis to the position-momentum-energy basis
  template <typename SymMatrix4x4, typename Matrix4x4, typename PELorentzVector>
  void convertToXPE( SymMatrix4x4& momcov, Matrix4x4& momposcov, const PELorentzVector& p4 ) {
    using Matrix1x4 = ROOT::Math::SMatrix<double, 1, 4>;
    Matrix1x4  jacobian{}; // only the E-row of the jacobian (P,M) --> (P,E)
    const auto mass  = packingmass( p4 );
    jacobian( 0, 0 ) = p4.Px() / p4.E();
    jacobian( 0, 1 ) = p4.Py() / p4.E();
    jacobian( 0, 2 ) = p4.Pz() / p4.E();
    jacobian( 0, 3 ) = mass / p4.E();
    Matrix1x4 JC     = jacobian * momcov;
    momcov( 3, 3 )   = ( JC * ROOT::Math::Transpose( jacobian ) )( 0, 0 );
    for ( int i = 0; i < 3; ++i ) momcov( 3, i ) = JC( 0, i );
    momposcov.Place_at( jacobian * momposcov, 3, 0 );
  }

  /// Convert packed momentum back to lorentzvector. Turned this into a standalone function such that it can also be
  /// used in the packer, as described above.
  auto unpackmomentum( const LHCb::ParticlePacker::PackedData& ppart, bool isVZero = false ) {
    // Lorentz momentum vector (force double precision to compute E)
    const double pz   = StandardPacker::energy( ppart.lv_pz );
    const double px   = ( isVZero ? StandardPacker::slope( ppart.lv_px ) * pz : StandardPacker::energy( ppart.lv_px ) );
    const double py   = ( isVZero ? StandardPacker::slope( ppart.lv_py ) * pz : StandardPacker::energy( ppart.lv_py ) );
    const double mass = ppart.lv_mass;
    const double E    = std::sqrt( ( px * px ) + ( py * py ) + ( pz * pz ) + ( mass * mass ) );
    return Gaudi::LorentzVector{ px, py, pz, E };
  }
} // namespace

using namespace LHCb;

void ParticlePacker::pack( const Data& part, PackedData& ppart, PackedDataVector& pparts ) const {
  // Only support version 1 for packing, as 0 is buggy.
  if ( 2 != pparts.packingVersion() ) {
    throw GaudiException( fmt::format( "Unknown packed data version {}", (int)pparts.packingVersion() ),
                          "ParticlePacker", StatusCode::FAILURE );
  }

  // fill ppart key from part
  ppart.key = StandardPacker::reference64( &pparts, &part );

  // Particle ID
  ppart.particleID = part.particleID().pid();

  // Mass and error
  ppart.measMass    = StandardPacker::mass( part.measuredMass() );
  ppart.measMassErr = StandardPacker::mass( part.measuredMassErr() );

  // Lorentz vector
  ppart.lv_px       = StandardPacker::energy( part.momentum().px() );
  ppart.lv_py       = StandardPacker::energy( part.momentum().py() );
  ppart.lv_pz       = StandardPacker::energy( part.momentum().pz() );
  const double mass = sqrt_or_0( part.momentum().M2() );
  ppart.lv_mass     = static_cast<float>( mass );

  // reference point
  ppart.refx = StandardPacker::position( part.referencePoint().x() );
  ppart.refy = StandardPacker::position( part.referencePoint().y() );
  ppart.refz = StandardPacker::position( part.referencePoint().z() );

  // Rotate cov matrices from (X,P,E) to (X,P,M) space. To make sure that the rotation is correctly inverted
  // in the unpacking, we rotate with the 'unpacked' momentum.
  auto momcov    = part.momCovMatrix();
  auto momposcov = part.posMomCovMatrix(); // actually momposcov!
  convertToXPM( momcov, momposcov, unpackmomentum( ppart ) );

  // Mom Cov
  const auto merr00 = sqrt_or_0( momcov( 0, 0 ) );
  const auto merr11 = sqrt_or_0( momcov( 1, 1 ) );
  const auto merr22 = sqrt_or_0( momcov( 2, 2 ) );
  const auto merr33 = sqrt_or_0( momcov( 3, 3 ) );
  ppart.momCov00    = StandardPacker::energy( merr00 );
  ppart.momCov11    = StandardPacker::energy( merr11 );
  ppart.momCov22    = StandardPacker::energy( merr22 );
  ppart.momCov33    = StandardPacker::energy( merr33 );
  ppart.momCov10    = StandardPacker::fraction( momcov( 1, 0 ), ( merr11 * merr00 ) );
  ppart.momCov20    = StandardPacker::fraction( momcov( 2, 0 ), ( merr22 * merr00 ) );
  ppart.momCov21    = StandardPacker::fraction( momcov( 2, 1 ), ( merr22 * merr11 ) );
  ppart.momCov30    = StandardPacker::fraction( momcov( 3, 0 ), ( merr33 * merr00 ) );
  ppart.momCov31    = StandardPacker::fraction( momcov( 3, 1 ), ( merr33 * merr11 ) );
  ppart.momCov32    = StandardPacker::fraction( momcov( 3, 2 ), ( merr33 * merr22 ) );

  // Pos Cov
  const auto perr00 = sqrt_or_0( part.posCovMatrix()( 0, 0 ) );
  const auto perr11 = sqrt_or_0( part.posCovMatrix()( 1, 1 ) );
  const auto perr22 = sqrt_or_0( part.posCovMatrix()( 2, 2 ) );
  ppart.posCov00    = StandardPacker::position( perr00 );
  ppart.posCov11    = StandardPacker::position( perr11 );
  ppart.posCov22    = StandardPacker::position( perr22 );
  ppart.posCov10    = StandardPacker::fraction( part.posCovMatrix()( 1, 0 ), ( perr11 * perr00 ) );
  ppart.posCov20    = StandardPacker::fraction( part.posCovMatrix()( 2, 0 ), ( perr22 * perr00 ) );
  ppart.posCov21    = StandardPacker::fraction( part.posCovMatrix()( 2, 1 ), ( perr22 * perr11 ) );

  // PosMom Cov
  ppart.pmCov00 = StandardPacker::fltPacked( momposcov( 0, 0 ) );
  ppart.pmCov01 = StandardPacker::fltPacked( momposcov( 0, 1 ) );
  ppart.pmCov02 = StandardPacker::fltPacked( momposcov( 0, 2 ) );
  ppart.pmCov10 = StandardPacker::fltPacked( momposcov( 1, 0 ) );
  ppart.pmCov11 = StandardPacker::fltPacked( momposcov( 1, 1 ) );
  ppart.pmCov12 = StandardPacker::fltPacked( momposcov( 1, 2 ) );
  ppart.pmCov20 = StandardPacker::fltPacked( momposcov( 2, 0 ) );
  ppart.pmCov21 = StandardPacker::fltPacked( momposcov( 2, 1 ) );
  ppart.pmCov22 = StandardPacker::fltPacked( momposcov( 2, 2 ) );
  ppart.pmCov30 = StandardPacker::fltPacked( momposcov( 3, 0 ) );
  ppart.pmCov31 = StandardPacker::fltPacked( momposcov( 3, 1 ) );
  ppart.pmCov32 = StandardPacker::fltPacked( momposcov( 3, 2 ) );

  // extra info
  ppart.firstExtra = pparts.extra().size();
  for ( const auto& [k, v] : part.extraInfo() ) {
    pparts.extra().emplace_back( k, StandardPacker::fltPacked( v ) );
    if ( parent().msgLevel( MSG::DEBUG ) ) parent().debug() << "extra info " << k << "  " << v << endmsg;
  }
  ppart.lastExtra = pparts.extra().size();

  // end vertex
  if ( part.endVertex() ) {
    ppart.vertex = StandardPacker::reference64( &pparts, part.endVertex() );
    if ( parent().msgLevel( MSG::DEBUG ) ) parent().debug() << "endvertex " << part.endVertex()->key() << endmsg;
  }

  // protoparticle
  if ( part.proto() ) {
    ppart.proto = StandardPacker::reference64( &pparts, part.proto() );
    if ( parent().msgLevel( MSG::DEBUG ) ) parent().debug() << "proto " << part.proto()->key() << endmsg;
  }

  // daughters
  ppart.firstDaughter = pparts.daughters().size();
  for ( const auto& P : part.daughters() ) {
    if ( P.target() ) {
      pparts.daughters().push_back( StandardPacker::reference64( &pparts, P ) );
      if ( parent().msgLevel( MSG::DEBUG ) ) parent().debug() << "daughter " << P->key() << endmsg;
    }
  }
  ppart.lastDaughter = pparts.daughters().size();

  if ( part.pv() ) {
    ppart.pv = StandardPacker::reference64( &pparts, part.pv() );
    if ( parent().msgLevel( MSG::DEBUG ) ) parent().debug() << "pv " << part.pv()->key() << endmsg;
  }
}

StatusCode ParticlePacker::unpack( const PackedData& ppart, Data& part, const PackedDataVector& pparts,
                                   DataVector& parts, LHCb::Packer::Carry& with_carry ) const {

  bool ok = true;

  if ( 0 != pparts.packingVersion() && 1 != pparts.packingVersion() && 2 != pparts.packingVersion() ) {
    throw GaudiException( fmt::format( "Unknown packed data version {}", (int)pparts.packingVersion() ),
                          "ParticlePacker", StatusCode::FAILURE );
  }

  const bool isVZero = ( 0 == pparts.packingVersion() );

  // particle ID
  part.setParticleID( LHCb::ParticleID( ppart.particleID ) );

  // Mass and error
  part.setMeasuredMass( StandardPacker::mass( ppart.measMass ) );
  part.setMeasuredMassErr( StandardPacker::mass( ppart.measMassErr ) );

  // Lorentz momentum vector
  part.setMomentum( unpackmomentum( ppart, isVZero ) );

  // reference point
  part.setReferencePoint( Gaudi::XYZPoint( StandardPacker::position( ppart.refx ),
                                           StandardPacker::position( ppart.refy ),
                                           StandardPacker::position( ppart.refz ) ) );

  // Mom Cov
  Gaudi::SymMatrix4x4 momCov{};

  const auto merr00 = ( isVZero ? StandardPacker::slope( ppart.momCov00 ) * part.momentum().Px()
                                : StandardPacker::energy( ppart.momCov00 ) );
  const auto merr11 = ( isVZero ? StandardPacker::slope( ppart.momCov11 ) * part.momentum().Py()
                                : StandardPacker::energy( ppart.momCov11 ) );
  const auto merr22 = StandardPacker::energy( ppart.momCov22 );
  const auto merr33 = StandardPacker::energy( ppart.momCov33 );
  momCov( 0, 0 )    = std::pow( merr00, 2 );
  momCov( 1, 1 )    = std::pow( merr11, 2 );
  momCov( 2, 2 )    = std::pow( merr22, 2 );
  momCov( 3, 3 )    = std::pow( merr33, 2 );
  momCov( 1, 0 )    = merr11 * merr00 * StandardPacker::fraction( ppart.momCov10 );
  momCov( 2, 0 )    = merr22 * merr00 * StandardPacker::fraction( ppart.momCov20 );
  momCov( 2, 1 )    = merr22 * merr11 * StandardPacker::fraction( ppart.momCov21 );
  momCov( 3, 0 )    = merr33 * merr00 * StandardPacker::fraction( ppart.momCov30 );
  momCov( 3, 1 )    = merr33 * merr11 * StandardPacker::fraction( ppart.momCov31 );
  momCov( 3, 2 )    = merr33 * merr22 * StandardPacker::fraction( ppart.momCov32 );

  // Pos Cov
  auto&      posCov = *( const_cast<Gaudi::SymMatrix3x3*>( &part.posCovMatrix() ) );
  const auto perr00 = StandardPacker::position( ppart.posCov00 );
  const auto perr11 = StandardPacker::position( ppart.posCov11 );
  const auto perr22 = StandardPacker::position( ppart.posCov22 );
  posCov( 0, 0 )    = std::pow( perr00, 2 );
  posCov( 1, 1 )    = std::pow( perr11, 2 );
  posCov( 2, 2 )    = std::pow( perr22, 2 );
  posCov( 1, 0 )    = perr11 * perr00 * StandardPacker::fraction( ppart.posCov10 );
  posCov( 2, 0 )    = perr22 * perr00 * StandardPacker::fraction( ppart.posCov20 );
  posCov( 2, 1 )    = perr22 * perr11 * StandardPacker::fraction( ppart.posCov21 );

  // Pos Mom Cov
  Gaudi::Matrix4x3 pmCov{};
  pmCov( 0, 0 ) = StandardPacker::fltPacked( ppart.pmCov00 );
  pmCov( 0, 1 ) = StandardPacker::fltPacked( ppart.pmCov01 );
  pmCov( 0, 2 ) = StandardPacker::fltPacked( ppart.pmCov02 );
  pmCov( 1, 0 ) = StandardPacker::fltPacked( ppart.pmCov10 );
  pmCov( 1, 1 ) = StandardPacker::fltPacked( ppart.pmCov11 );
  pmCov( 1, 2 ) = StandardPacker::fltPacked( ppart.pmCov12 );
  pmCov( 2, 0 ) = StandardPacker::fltPacked( ppart.pmCov20 );
  pmCov( 2, 1 ) = StandardPacker::fltPacked( ppart.pmCov21 );
  pmCov( 2, 2 ) = StandardPacker::fltPacked( ppart.pmCov22 );
  pmCov( 3, 0 ) = StandardPacker::fltPacked( ppart.pmCov30 );
  pmCov( 3, 1 ) = StandardPacker::fltPacked( ppart.pmCov31 );
  pmCov( 3, 2 ) = StandardPacker::fltPacked( ppart.pmCov32 );

  if ( pparts.packingVersion() >= 2 ) convertToXPE( momCov, pmCov, part.momentum() );
  part.setMomCovMatrix( momCov );
  part.setPosMomCovMatrix( pmCov );

  // extra info
  for ( const auto& [k, v] : with_carry( pparts.extra(), ppart.firstExtra, ppart.lastExtra ) ) {
    part.addInfo( k, StandardPacker::fltPacked( v ) );
  }

  // end vertex
  auto unpack_ref = StandardPacker::UnpackRef{ &pparts, &parts };
  if ( -1 != ppart.vertex ) {
    if ( auto v = unpack_ref( ppart.vertex ); v ) {
      part.setEndVertex( v );
      if ( parent().msgLevel( MSG::DEBUG ) ) parent().debug() << "endvertex " << ppart.vertex << endmsg;
    } else {
      parent().error() << "Corrupt Particle Vertex SmartRef found" << endmsg;
      ok = false;
    }
  }

  // protoparticle
  if ( -1 != ppart.proto ) {
    if ( auto p = unpack_ref( ppart.proto ); p ) {
      part.setProto( p );
      if ( parent().msgLevel( MSG::DEBUG ) ) parent().debug() << "proto " << ppart.proto << endmsg;
    } else {
      parent().error() << "Corrupt Particle ProtoParticle SmartRef found" << endmsg;
      ok = false;
    }
  }

  // daughters
  for ( auto& d : LHCb::Packer::subrange( pparts.daughters(), ppart.firstDaughter, ppart.lastDaughter ) ) {
    if ( auto p = unpack_ref( d ); p ) {
      part.addToDaughters( p );
      if ( parent().msgLevel( MSG::DEBUG ) ) parent().debug() << "daughter " << d << endmsg;
    } else {
      parent().error() << "Corrupt Particle Daughter Particle SmartRef found" << endmsg;
      ok = false;
    }
  }

  if ( pparts.packingVersion() >= 2 && -1 != ppart.pv ) {
    if ( auto p = unpack_ref( ppart.pv ); p ) {
      part.setPV( p );
      if ( parent().msgLevel( MSG::DEBUG ) ) parent().debug() << "pv " << ppart.pv << endmsg;
    } else {
      parent().error() << "Corrupt Particle PV SmartRef found" << endmsg;
      ok = false;
    }
  }

  return StatusCode{ ok };
}

StatusCode ParticlePacker::unpack( const PackedDataVector& pparts, DataVector& parts ) const {
  parts.reserve( pparts.data().size() );
  parts.setVersion( pparts.version() );
  LHCb::Packer::Carry carry{};
  bool                ok = true;
  for ( const auto& ppart : pparts.data() ) {
    // make and save new pid in container
    auto* part         = new Data();
    auto [linkID, key] = StandardPacker::indexAndKey64( ppart.key );
    parts.insert( part, key );
    // Fill data from packed object
    ok &= unpack( ppart, *part, pparts, parts, carry ).isSuccess();
  }
  if ( !ok ) { parts.clear(); }
  return StatusCode{ ok };
}

ParticlePacker::DataVector ParticlePacker::unpack( const PackedDataVector& pparts ) const {

  DataVector parts;
  parts.reserve( pparts.data().size() );
  parts.setVersion( pparts.version() );
  LHCb::Packer::Carry carry{};
  bool                ok = true;
  for ( const auto& ppart : pparts.data() ) {
    // make and save new pid in container
    auto* part         = new Data();
    auto [linkID, key] = StandardPacker::indexAndKey64( ppart.key );
    parts.insert( part, key );
    // Fill data from packed object
    ok &= unpack( ppart, *part, pparts, parts, carry ).isSuccess();
  }
  if ( !ok ) { parts.clear(); }
  return parts;
}

StatusCode ParticlePacker::check( const Data* dataA, const Data* dataB ) const {
  // assume OK from the start
  bool ok = true;

  // checker
  const DataPacking::DataChecks ch( parent() );

  // checks here

  // key
  ok &= ch.compareInts( "Key", dataA->key(), dataB->key() );

  // PID
  ok &= ch.compareInts( "PID", dataA->particleID(), dataB->particleID() );

  // Mass
  ok &= ch.compareMasses( "MeasuredMass", dataA->measuredMass(), dataB->measuredMass() );

  ok &= ch.compareMasses( "MeasuredMassError", dataA->measuredMassErr(), dataB->measuredMassErr() );

  // momentum
  ok &= ch.compareLorentzVectors( "Momentum", dataA->momentum(), dataB->momentum() );

  // reference position
  ok &= ch.comparePoints( "ReferencePoint", dataA->referencePoint(), dataB->referencePoint() );

  // Mom Cov
  const std::array<double, 4> tolDiagMomCov = {
      { Packer::ENERGY_TOL, Packer::ENERGY_TOL, Packer::ENERGY_TOL, 2 * Packer::ENERGY_TOL } };
  ok &= ch.compareCovMatrices<Gaudi::SymMatrix4x4, 4>( "MomCov", dataA->momCovMatrix(), dataB->momCovMatrix(),
                                                       tolDiagMomCov, 3 * Packer::FRACTION_TOL );

  // Pos Cov
  const std::array<double, 3> tolDiagPosCov = { { Packer::POSITION_TOL, Packer::POSITION_TOL, Packer::POSITION_TOL } };
  ok &= ch.compareCovMatrices<Gaudi::SymMatrix3x3, 3>( "PosCov", dataA->posCovMatrix(), dataB->posCovMatrix(),
                                                       tolDiagPosCov, Packer::FRACTION_TOL );

  // PosMom Cov
  ok &= ch.compareMatrices<Gaudi::Matrix4x3, 4, 3>( "PosMomCov", dataA->posMomCovMatrix(), dataB->posMomCovMatrix() );

  // Extra info
  const bool extraSizeOK = dataA->extraInfo().size() == dataB->extraInfo().size();
  ok &= extraSizeOK;
  if ( extraSizeOK ) {
    auto iEA = dataA->extraInfo().begin();
    auto iEB = dataB->extraInfo().begin();
    for ( ; iEA != dataA->extraInfo().end() && iEB != dataB->extraInfo().end(); ++iEA, ++iEB ) {
      auto       mess  = fmt::format( "ExtraInfo:{}", (LHCb::Particle::additionalInfo)iEA->first );
      const bool keyOK = iEA->first == iEB->first;
      if ( !keyOK ) parent().warning() << mess << " Different Keys" << endmsg;
      ok &= keyOK;
      const double relTol = 1.0e-3;
      double       tol    = relTol * std::abs( iEA->second );
      if ( tol < relTol ) tol = relTol;
      const bool valueOK = ch.compareDoubles( mess, iEA->second, iEB->second, tol );
      ok &= valueOK;
    }
  } else {
    parent().warning() << "ExtraInfo has different sizes" << endmsg;
  }

  // end vertex
  ok &= ch.comparePointers( "EndVertex", dataA->endVertex(), dataB->endVertex() );

  // proto particle
  ok &= ch.comparePointers( "ProtoParticle", dataA->proto(), dataB->proto() );

  // daughters
  const bool dauSizeOK = dataA->daughters().size() == dataB->daughters().size();
  ok &= dauSizeOK;
  if ( dauSizeOK ) {
    auto iDA = dataA->daughters().begin();
    auto iDB = dataB->daughters().begin();
    for ( ; iDA != dataA->daughters().end() && iDB != dataB->daughters().end(); ++iDA, ++iDB ) {
      ok &= ch.comparePointers( "Daughters", &**iDA, &**iDB );
    }
  } else {
    parent().warning() << "Daughters different sizes" << endmsg;
  }

  // pv
  ok &= ch.comparePointers( "PV", dataA->pv().target(), dataB->pv().target() );

  // force printout for tests
  // ok = false;
  // If comparison not OK, print full information
  if ( !ok ) {
    const std::string loc =
        ( dataA->parent() && dataA->parent()->registry() ? dataA->parent()->registry()->identifier() : "Not in TES" );
    parent().warning() << "Problem with Particle data packing :-" << endmsg
                       << "  Original Particle key=" << dataA->key() << " in '" << loc << "'" << endmsg << dataA
                       << endmsg << "  Unpacked Particle" << endmsg << dataB << endmsg;
  }

  return ( ok ? StatusCode::SUCCESS : StatusCode::FAILURE );
}

namespace LHCb {
  StatusCode unpack( Gaudi::Algorithm const* parent, const PackedParticles& in, Particles& out ) {
    return ParticlePacker{ parent }.unpack( in, out );
  }
} // namespace LHCb
