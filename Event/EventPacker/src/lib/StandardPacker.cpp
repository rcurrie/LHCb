/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/StandardPacker.h"

#include "Gaudi/Algorithm.h"
#include "GaudiKernel/GaudiException.h"

#include "fmt/format.h"

#include <atomic>

//-----------------------------------------------------------------------------
// Implementation file for class : StandardPacker
//
// 2015-03-07 : Chris Jones
//-----------------------------------------------------------------------------

bool StandardPacker::UnpackRef::hintAndKey32( const int data, int& hint, int& key ) const {
  // return status is bad by default
  bool OK = false;

  // Proceed if target and source are OK
  if ( m_target && m_source && m_target->linkMgr() && m_source->linkMgr() ) {
    // Extract the packed index and key from the data word
    int indx = data >> 28;
    key      = data & 0x0FFFFFFF;

    // Get the source link
    if ( const auto* sourceLink = m_source->linkMgr()->link( indx ); sourceLink ) {
      // If link is valid, saved to target and get the hint
      hint = m_target->linkMgr()->addLink( sourceLink->path(), sourceLink->object() );
      // finally return status is OK
      OK = true;
    }
  }

  // If failed to extract the data, reset values
  if ( !OK ) { hint = key = 0; }

  // return final status
  return OK;
}

bool StandardPacker::UnpackRef::hintAndKey64( const std::int64_t data, int& hint, int& key ) const {
  // return status is bad by default
  bool OK = false;

  // Proceed if target and source are OK
  if ( m_target && m_source && m_target->linkMgr() && m_source->linkMgr() ) {
    // Extract the packed index and key from the data word
    auto r = indexAndKey64( data );
    key    = r.key;

    // Get the source link
    if ( const auto* sourceLink = m_source->linkMgr()->link( r.id ); sourceLink ) {
      // If link is valid, saved to target and get the hint
      hint = m_target->linkMgr()->addLink( sourceLink->path(), sourceLink->object() );
      // finally return status is OK
      OK = true;
    }
  }

  // If failed to extract the data, reset values
  if ( !OK ) { hint = key = 0; }

  // return final status
  return OK;
}
