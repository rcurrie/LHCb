/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "UnpackerBaseAlg.h"

#include "Event/PackedCaloChargedInfo_v1.h"
#include "Event/PackedCaloHypo.h"
#include "Event/PackedGlobalChargedPID.h"
#include "Event/PackedMuonPID.h"
#include "Event/PackedNeutralPID.h"
#include "Event/PackedRichPID.h"
#include "Event/PackedTrack.h"
#include "Event/PackedVertex.h"
#include "Event/PackedWeightsVector.h"

#include "Event/PackedMCCaloHit.h"
#include "Event/PackedMCHit.h"
#include "Event/PackedMCRichDigitSummary.h"
#include "Event/PackedMCRichHit.h"
#include "Event/PackedMCRichOpticalPhoton.h"
#include "Event/PackedMCRichSegment.h"
#include "Event/PackedMCRichTrack.h"

namespace DataPacking {
  // MC Packers don't serialize data so we keep the ones taking packed objects
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::MCHitPacker>,
                             "UnpackMCHit" ) // note: different naming convention _on purpose_ -- this one type should
                                             // be used instead of all the MCXYZHitUnpacker eventually...
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::MCCaloHitPacker>, "UnpackMCCaloHit" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::MCRichHitPacker>, "MCRichHitUnpacker" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::MCRichOpticalPhotonPacker>, "MCRichOpticalPhotonUnpacker" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::MCRichSegmentPacker>, "MCRichSegmentUnpacker" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::MCRichTrackPacker>, "MCRichTrackUnpacker" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::MCPrsHitPacker>, "MCPrsHitUnpacker" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::MCSpdHitPacker>, "MCSpdHitUnpacker" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::MCEcalHitPacker>, "MCEcalHitUnpacker" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::MCHcalHitPacker>, "MCHcalHitUnpacker" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::MCVeloHitPacker>, "MCVeloHitUnpacker" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::MCVPHitPacker>, "MCVPHitUnpacker" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::MCTVHitPacker>, "MCTVHitUnpacker" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::MCPuVetoHitPacker>, "MCPuVetoHitUnpacker" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::MCTTHitPacker>, "MCTTHitUnpacker" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::MCUTHitPacker>, "MCUTHitUnpacker" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::MCUPHitPacker>, "MCUPHitUnpacker" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::MCITHitPacker>, "MCITHitUnpacker" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::MCOTHitPacker>, "MCOTHitUnpacker" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::MCMuonHitPacker>, "MCMuonHitUnpacker" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::MCRichDigitSummaryPacker>, "MCRichDigitSummaryUnpacker" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::MCFTHitPacker>, "MCFTHitUnpacker" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::MCMPHitPacker>, "MCMPHitUnpacker" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::MCSLHitPacker>, "MCSLHitUnpacker" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::MCHCHitPacker>, "MCHCHitUnpacker" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::MCBcmHitPacker>, "MCBcmHitUnpacker" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::MCBlsHitPacker>, "MCBlsHitUnpacker" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::MCPlumeHitPacker>, "MCPlumeHitUnpacker" )

  // These are needed for unpacking brunel output files, to be removed
  // Used in RecoConf/data_from_file.py and  DaVinci/data_from_file.py
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::RichPIDPacker>, "UnpackRichPIDs" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::MuonPIDPacker>, "UnpackMuonPIDs" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::NeutralPIDPacker>, "UnpackNeutralPIDs" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::CaloChargedPIDPacker>, "UnpackCaloChargedPIDs" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::GlobalChargedPIDPacker>, "UnpackGlobalChargedPIDs" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::BremInfoPacker>, "UnpackBremInfos" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::WeightsVectorPacker>, "UnpackWeightsVector" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::TrackPacker>, "UnpackTrack" )
  DECLARE_COMPONENT_WITH_ID( Unpack<LHCb::CaloHypoPacker>, "UnpackCaloHypo" )

} // namespace DataPacking
