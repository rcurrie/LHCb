/*****************************************************************************\
* (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE test_checksums

#include "Event/PackedDataChecksum.h"
#include "GaudiKernel/System.h"
#include <boost/pfr/core.hpp>
#include <boost/test/data/test_case.hpp>
#include <boost/test/unit_test.hpp>
#include <cstddef>
#include <cstdint>
#include <type_traits>
#include <vector>

template <typename A, typename B, typename C>
struct Test {
  A a{ 0 };
  B b{ 0 };
  C c{ 0 };
  template <typename T>
  void save( T& t ) const {
    t.save( *this );
  }
};

using TestA = Test<std::uint32_t, std::uint8_t, std::uint8_t>;
using TestB = Test<std::uint32_t, std::uint8_t, std::uint16_t>;
using TestC = Test<std::uint16_t, std::uint8_t, std::uint8_t>;
using TestD = Test<std::uint32_t, std::uint8_t, std::uint32_t>;
using TestE = Test<std::uint16_t, std::uint64_t, std::uint32_t>;

template <typename TEST>
auto test() {
  static_assert( std::is_aggregate_v<TEST> );
  const auto               test_name = System::typeinfoName( typeid( TEST ) );
  static const std::size_t nData     = 1024;
  std::vector<TEST>        test;
  test.reserve( nData );
  for ( std::size_t i = 0; i < nData; ++i ) {
    const std::uint8_t j = i % std::numeric_limits<std::uint8_t>::max();
    test.emplace_back( j, j, j );
  }
  LHCb::Hlt::PackedData::PackedDataChecksum pdCkSum;
  for ( auto& x : test ) { pdCkSum.processObject( x, test_name ); }
  const auto  cksum = pdCkSum.checksum( test_name );
  std::size_t payload_size{ 0 };
  boost::pfr::for_each_field( test[0], [&]( const auto& i ) { payload_size += sizeof( i ); } );
  const auto padding_size = sizeof( TEST ) - payload_size;
  std::cout << test_name << " Size=" << sizeof( TEST ) << " Padding=" << padding_size << " Checksum=" << cksum
            << std::endl;
  return cksum;
}

BOOST_AUTO_TEST_CASE( test_TestA ) { BOOST_CHECK( test<TestA>() == 4013466386 ); }
BOOST_AUTO_TEST_CASE( test_TestB ) { BOOST_CHECK( test<TestB>() == 1953834317 ); }
BOOST_AUTO_TEST_CASE( test_TestC ) { BOOST_CHECK( test<TestC>() == 2318790456 ); }
BOOST_AUTO_TEST_CASE( test_TestD ) { BOOST_CHECK( test<TestD>() == 491182948 ); }
BOOST_AUTO_TEST_CASE( test_TestE ) { BOOST_CHECK( test<TestE>() == 1180866782 ); }
