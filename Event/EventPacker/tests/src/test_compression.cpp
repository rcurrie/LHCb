/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE test_compression

#include "Event/PackedDataBuffer.h"
#include <bitset>
#include <cstddef>

namespace std {
  std::ostream& operator<<( std::ostream& os, std::byte b ) {
    return os << std::bitset<8>( std::to_integer<int>( b ) );
  }
} // namespace std

#include <boost/test/data/test_case.hpp>
#include <boost/test/unit_test.hpp>

using namespace LHCb::Hlt::PackedData;
namespace bdata = boost::unit_test::data;

struct incrementable_byte {
  std::byte b;
  auto&     operator++() {
    b = static_cast<std::byte>( std::to_integer<std::uint8_t>( b ) + (std::uint8_t)1 );
    return *this;
  }
  operator std::byte() const { return b; }
};

BOOST_DATA_TEST_CASE( test_compression,
                      // test all algos (NoCompression, ZLIB, LZMA, LZ4, ZSTD)
                      bdata::xrange( 5 ) *
                          // compression levels 1,5,9
                          bdata::xrange<int>( ( bdata::begin = 1, bdata::end = 10, bdata::step = 4 ) ) *
                          // and small or large buffers (10 and 20MB, limit in ROOT is around 16.7)
                          bdata::xrange<int>( ( bdata::begin = 10000000, bdata::end = 25000000,
                                                bdata::step = 10000000 ) ),
                      type, level, dataSize ) {
  // test big buffers only on ZSTD
  Compression compression{ type };
  if ( compression != Compression::ZSTD && dataSize > 0xffffff ) {
    std::cout << "Giving up with " << toString( compression ) << " and large buffer, it's not supported\n";
    return;
  }
  std::cout << "Testing " << toString( compression ) << ", level " << level << ", input data of size " << dataSize;
  // create tests data
  ByteBuffer::buffer_type data( dataSize );
  std::iota( begin( data ), end( data ), incrementable_byte{ std::byte{ 0 } } );
  // wrap it into a ByteBuffer with no compression
  ByteBuffer buffer;
  buffer.init( data, false );
  // compress it
  ByteBuffer::buffer_type zipData;
  BOOST_CHECK( buffer.compress( compression, level, zipData ) == true );
  std::cout << ", size went down to " << zipData.size() << "\n";
  // decompress back
  ByteBuffer outBuffer;
  outBuffer.init( zipData, true );
  // check we got back original data
  BOOST_CHECK_EQUAL_COLLECTIONS( begin( data ), end( data ), begin( outBuffer.buffer() ), end( outBuffer.buffer() ) );
}
