###############################################################################
# (c) Copyright 2000-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
cmake_minimum_required(VERSION 3.15)
option(CMAKE_EXPORT_COMPILE_COMMANDS "Enable/Disable output of compile_commands.json" ON)

project(LHCb VERSION 56.4
        LANGUAGES CXX)

# Enable testing with CTest/CDash
include(CTest)

list(PREPEND CMAKE_MODULE_PATH
    ${PROJECT_SOURCE_DIR}/cmake
    ${PROJECT_SOURCE_DIR}/Phys/LoKiCore/cmake
)

include(LHCbOptions)

set(WITH_LHCb_PRIVATE_DEPENDENCIES TRUE)
include(LHCbDependencies)

# ## Environment ##

# Make sure Gaudi runtime is handled correctly
# (needed because Gaudi uses a slightly different mechanism)
if(Gaudi_BINARY_DIR)
    lhcb_env(PREPEND PATH ${Gaudi_BINARY_DIR})
endif()
if(Gaudi_INCLUDE_DIR)
    lhcb_env(PREPEND ROOT_INCLUDE_PATH ${Gaudi_INCLUDE_DIR})
endif()
if(Gaudi_LIBRARY_DIR)
    lhcb_env(PREPEND LD_LIBRARY_PATH ${Gaudi_LIBRARY_DIR})
endif()
if(Gaudi_PLUGINS_DIR AND NOT Gaudi_PLUGINS_DIR STREQUAL Gaudi_PLUGINS_DIR)
    lhcb_env(PREPEND LD_LIBRARY_PATH ${Gaudi_PLUGINS_DIR})
endif()
if(Gaudi_PYTHON_DIR)
    lhcb_env(PREPEND PYTHONPATH ${Gaudi_PYTHON_DIR})
endif()
if(USE_TORCH)
    lhcb_env(PREPEND LD_LIBRARY_PATH ${torch_LIBRARIES_DIR})
endif()

# Turn off threading for a number of external components that otherwise
# would decide how many threads to use for themselves.
# - OpenBLAS (OPENBLAS_NUM_THREADS)
# - XGBoost (OMP_THREAD_LIMIT)
lhcb_env(
    SET OPENBLAS_NUM_THREADS 1
    SET OMP_THREAD_LIMIT 1
)

# GAUDI_QMTEST_CLASS overwrites the default python class used for LHCb tests
# Default is QMTTest, we use here an LHCb extension : LHCbTest
lhcb_env(
    SET STDOPTS ${CMAKE_CURRENT_SOURCE_DIR}/GaudiConf/options
    SET GAUDI_QMTEST_MODULE GaudiConf.QMTest.LHCbTest
    SET GAUDI_QMTEST_CLASS LHCbTest
)

# Silence an unavoidable warning from cling
# see https://gitlab.cern.ch/lhcb/LHCb/-/merge_requests/2912
if(ROOT_VERSION MATCHES "^6\\.2[2-4].*")
  set(LHCB_DICT_GEN_DEFAULT_OPTS  -Wno-unknown-attributes)
  message(STATUS "Generating dictionaries with additional flags: " ${LHCB_DICT_GEN_DEFAULT_OPTS})
endif()

lhcb_initialize_configuration()


if (NOT USE_DD4HEP)
    set(VarName DetDescOnlySubDirs)
else()
    set(VarName LHCB_IGNORE_SUBDIRS)
endif()
set(${VarName}
    Det/BcmDet
    Det/CaloDetXmlCnv
    Det/DetDescChecks
    Det/DetDescCnv
    Det/DetDescSvc
    Tools/GitEntityResolver
    Tools/XmlTools
)

# -- Subdirectories
lhcb_add_subdirectories(
    Associators/AssociatorsBase
    Associators/MCAssociators
    Calo/CaloKernel
    CaloFuture/CaloFutureDAQ
    CaloFuture/CaloFutureInterfaces
    CaloFuture/CaloFutureUtils
    DAQ/DAQSys
    DAQ/DAQUtils
    DAQ/MDF
    DAQ/MDF_ROOT
    DAQ/RawEventCompat
    Det/CaloDet
    Det/DDDB
    Det/DetCond
    Det/DetDesc
    Det/FTDet
    Det/LbDD4hep
    Det/Magnet
    Det/MuonDet
    Det/RichDet
    Det/UTDet
    Det/VPDet
    Det/LHCbDet
    ${DetDescOnlySubDirs}
    Event/DAQEvent
    Event/DigiEvent
    Event/DummyProducers
    Event/EventAssoc
    Event/EventBase
    Event/EventPacker
    Event/EventSys # candidate for removal
    Event/FSREvent
    Event/FTEvent
    Event/GenEvent
    Event/HltEvent
    Event/LinkerEvent
    Event/LinkerInstances
    Event/LumiEvent
    Event/MCEvent
    Event/MicroDst
    Event/PhysEvent
    Event/RecEvent
    Event/RecreatePIDTools
    Event/TrackEvent
    Ex/IOExample
    FT/FTDAQ
    GaudiAlg
    GaudiConf
    GaudiConfUtils
    GaudiGSL
    Hlt/HLTScheduler
    Hlt/HltDAQ
    Hlt/HltServices
    Kernel/AllocationTracker
    Kernel/FileSummaryRecord
    Kernel/FSRAlgs
    Kernel/HltInterfaces
    Kernel/IOAlgorithms
    Kernel/LHCbAlgs
    Kernel/LHCbKernel
    Kernel/LHCbMath
    Kernel/MCInterfaces
    Kernel/PartProp
    Kernel/PhysInterfaces
    Kernel/Relations
    Kernel/XMLSummaryBase
    Kernel/XMLSummaryKernel
    Muon/MuonDAQ
    Phys/LoKiCore
    Phys/LoKiHlt
    Phys/LoKiMC
    Phys/LoKiNumbers
    PyConf
    Rich/RichDetectors
    Rich/RichFutureDAQ
    Rich/RichFutureKernel
    Rich/RichFutureMCUtils
    Rich/RichFutureUtils
    Rich/RichInterfaces
    Rich/RichKernel
    Rich/RichUtils
    Si/SiDAQ
    Sim/SimComponents
    Tools/ZeroMQ
    Tr/LHCbTrackInterfaces
    UT/UTDAQ
    UT/UTKernel
    UT/UTTELL1Event
    VP/VPDAQ
    VP/VPKernel
    Vis/Phoenix
)

gaudi_install(CMAKE
    # helpers to find external projects
    cmake/FindHepMC.cmake
    cmake/FindRELAX.cmake
    cmake/FindVDT.cmake
    cmake/Findtorch.cmake

    # helpers to use in LHCb projects
    cmake/FileContentMetadataRepository.cmake
    cmake/FindDataPackage.cmake
    cmake/LHCbConfigUtils.cmake
    cmake/ProjectConfig.cmake.in

    # QMTest support
    cmake/qmtest_support.cmake
)
install(PROGRAMS cmake/extract_qmtest_metadata.py # used in gaudi_add_qmtest()
    DESTINATION "${GAUDI_INSTALL_CONFIGDIR}"
)

# Test scripts inside cmake/
gaudi_add_pytest(cmake/extract_qmtest_metadata.py
    PREFIX cmake.doctest.
    ROOT_DIR cmake
    OPTIONS --doctest-modules
)

lhcb_finalize_configuration()

if(TARGET LHCb::GaudiAlgLib)
    # if we built GaudiAlgLib (instead of taking it from Gaudi) we have to
    # create an alias for downstream projects to use.
    file(APPEND ${PROJECT_BINARY_DIR}/${PROJECT_NAME}Config.cmake
        "\nadd_library(Gaudi::GaudiAlgLib ALIAS LHCb::GaudiAlgLib)\n"
    )
endif()
