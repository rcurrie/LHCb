###############################################################################
# (c) Copyright 2000-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
GaudiConf
---------
#]=======================================================================]

gaudi_install(PYTHON)
gaudi_generate_confuserdb(
    GaudiConf.SimConf
    GaudiConf.DigiConf
    GaudiConf.CaloPackingConf
    GaudiConf.TurboConf
)
lhcb_add_confuser_dependencies(
    DAQ/RawEventCompat
    Kernel/LHCbKernel
    Packer/EventPacker:EventPacker
)

gaudi_install(SCRIPTS)

if(BUILD_TESTING)
    gaudi_add_tests(QMTest)

    gaudi_add_pytest(python OPTIONS --doctest-modules -v)
    gaudi_add_pytest(tests/pytest)
    add_dependencies(pytest-prefetch-GaudiConf-GaudiConf.pytest.python LHCb_MergeConfdb)

    # GAUDI-976: make GaudiTest.py more resilient towards unicode-ascii conversion failure
    # We inject unicode in the environment to make sure it's handles correctly
    # https://its.cern.ch/jira/browse/GAUDI-976
    set_tests_properties(GaudiConf.qmtest_support.jira_gaudi_976
        PROPERTIES
            ENVIRONMENT TESTENV=à
    )

    # the default stdout limit is 100MB, but for to test the check we can use a smaller limit (1KB)
    set_tests_properties(GaudiConf.qmtest_support.stdout_cutoff
        PROPERTIES
            ENVIRONMENT GAUDI_TEST_STDOUT_LIMIT=1024
            PASS_REGULAR_EXPRESSION "unexpected too big stdout"
    )
endif()
