###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PRConfig.TestFileDB import test_file_db

from GaudiConf.LbExec import Options
from PyConf.application import configure_input

test_options = {
    "simulation": True,
    "data_type": "Upgrade",
    "evt_max": 0,
    "output_file": "example-extra-yaml.dst",
    "dddb_tag": "upgrade/master",
    "conddb_tag": "upgrade/master",
}


def check_output_file(options: Options):
    assert options.output_file == "example-extra-yaml.dst", "Unexpected output_file"

    return configure_input(options)


def check_input_files(options: Options):
    assert options.input_files == test_file_db["expected_2024_minbias_xdigi"].filenames

    return configure_input(options)
