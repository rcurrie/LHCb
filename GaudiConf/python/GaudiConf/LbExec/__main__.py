#!/usr/bin/env python
###############################################################################
# (c) Copyright 2022-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import argparse
from multiprocessing import Pipe, Process, connection


def _parse_args_inner(args):
    from . import main
    from .cli_utils import FunctionLoader, OptionsLoader

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "function",
        type=FunctionLoader,
        help="Function to call with the options that will return the configuration. "
        "Given in the form 'my_module:function_name'.",
    )
    parser.add_argument(
        "options",
        help="YAML data to populate the Application.Options object with. "
        "Multiple files can merged using 'file1.yaml+file2.yaml'.",
    )
    parser.add_argument("extra_args", nargs="*")
    parser.add_argument(
        "--dry-run",
        action="store_true",
        help="Do not run the application, just generate the configuration.",
    )
    parser.add_argument(
        "--export",
        help='Write a file containing the full options (use "-" for stdout)',
    )
    parser.add_argument(
        "--with-defaults",
        action="store_true",
        help="Include options set to default values (for use with --export)",
    )
    parser.add_argument(
        "--override-option-class",
        help="Allows to override the option class specified in the function signature."
        "Given in the form 'my_module:ClassName'.",
    )
    parser.add_argument(
        "--app-type",
        default="Gaudi::Application",
        help=argparse.SUPPRESS,
        # "Name of the Gaudi application to use, primarily needed for Online."
    )
    kwargs = vars(parser.parse_args(args))
    kwargs["options"] = OptionsLoader(
        kwargs["function"], kwargs["options"], kwargs.pop("override_option_class")
    )
    return main(**kwargs)


def _parse_args_in_process(args, conn):
    """Wrapper for use with Process to call parse_args in a subprocess.

    Doing this means we can clean up any memory allocated while generating the
    configuration and can significantly reduce the memory usage of the main
    event processing.
    """
    conn.send(_parse_args_inner(args))
    conn.close()
    exit(0)


def parse_args() -> int:
    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument(
        "--without-process-isolation",
        action="store_true",
        help="Generate the Gaudi configuration in the same process as the main event loop. "
        "This will significantly increase the memory usage of the main event loop!",
    )
    args, unprocessed_args = parser.parse_known_args()

    if args.without_process_isolation:
        result = _parse_args_inner(unprocessed_args)
    else:
        parent_conn, child_conn = Pipe()
        p = Process(target=_parse_args_in_process, args=(unprocessed_args, child_conn))
        p.start()
        # Wait until either the pipe is ready or the process exits
        connection.wait([parent_conn, p.sentinel])
        if not parent_conn.poll():
            # If there is no data in the pipe something went wrong generating the configuration
            return p.exitcode or 1
        result = parent_conn.recv()
        parent_conn.close()
        p.join()
        if p.exitcode != 0:
            raise RuntimeError(
                f"Generating the Gaudi configuration failed with {p.exitcode}"
            )

    # Check if this was a dry-run
    if result is None:
        return 0
    gaudi_args, gaudi_kwargs = result

    import Gaudi

    app = Gaudi.Application(*gaudi_args, **gaudi_kwargs)
    # As the options dictionary can be very large, make sure to deallocate it
    # before starting the event loop
    del gaudi_args, gaudi_kwargs
    return app.run()


if __name__ == "__main__":
    exit(parse_args())
