###############################################################################
# (c) Copyright 2022-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__all__ = (
    "InputProcessTypes",
    "DataTypeEnum",
    "HltSourceID",
    "FileFormats",
    "EventStores",
    "Options",
    "TestOptions",
    "TestOptionsBase",
    "main",
)

import os
import sys

import click

from .configurables import config2opts, do_export
from .options import (
    DataTypeEnum,
    EventStores,
    FileFormats,
    HltSourceID,
    InputProcessTypes,
    Options,
    TestOptions,
    TestOptionsBase,
)


def main(
    function,
    options,
    extra_args,
    *,
    dry_run=False,
    with_defaults=False,
    export=None,
    app_type="Gaudi::Application",
):
    """Run an lbexec-style Gaudi job.

    Args:
        function (callable): A callable that will return the Gaudi configuration
        options (Options): An initialised APP.Options object
        extra_args (list of str): list of strings to add the the call to ``function``
        dry_run (bool): Only generate the configuration and don't actually start the job
        with_defaults (bool): Include options set to default values when writing them out
        export (str): Filename to write the options out (or ``'-'`` to write to stdout as ``.opts``)
        app_type (str): The ``Gaudi.Application`` ``appType`` to run

    Returns:
        args, kwargs: The arguments to pass to Gaudi.Application
    """

    config = function(options, *extra_args)
    opts = config2opts(config, with_defaults)

    if export:
        click.echo(
            click.style("INFO:", fg="green") + f" Writing configuration to {export}",
            err=True,
        )
        export_data = do_export(os.path.splitext(export)[-1] or ".opts", opts)
        if export == "-":
            sys.stdout.buffer.write(export_data)
        else:
            with open(export, "wb") as fh:
                fh.write(export_data)

    if dry_run:
        click.echo(
            click.style("INFO:", fg="green")
            + " Not starting the application as this is a dry-run.",
            err=True,
        )
        return None

    # Ensure that any printout that has been made by the user provided function
    # has been flushed. Without this, non-interactive jobs such as tests end up
    # showing the print out in the middle of the Gaudi application log
    sys.stdout.flush()
    sys.stderr.flush()

    # Run the actual job
    opts["ApplicationMgr.JobOptionsType"] = '"NONE"'
    return (opts,), {"appType": app_type}
