###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from typing import Dict

import pytest
from GaudiTesting.GaudiExeTest import GaudiExeTest

from LHCbTesting.preprocessors import preprocessor


class LHCbExeTest(GaudiExeTest):
    """
    An extension of GaudiExeTest tailored to the LHCb projects workflow.
    Includes counters testing.
    """

    preprocessor = preprocessor

    @pytest.mark.do_not_collect_source
    def test_counters(
        self,
        reference: Dict,
        counters: Dict,
    ) -> None:
        """
        Test the counters summaries against the reference.
        """
        if not reference or reference.get("counters") is None:
            pytest.skip()

        try:
            assert counters == reference["counters"]
        except AssertionError:
            reference["counters"] = counters
            raise
