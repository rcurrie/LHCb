###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import pytest
from LHCbTesting import LHCbExeTest
from LHCbTesting.preprocessors import RegexpReplacer


@pytest.mark.ctest_fixture_setup("gaudiconf.iohelper")
@pytest.mark.shared_cwd("GaudiConf")
class Test(LHCbExeTest):
    command = ["python", "../test_iohelper.py"]
    reference = "../refs/test_iohelper.yaml"

    if LHCbExeTest.preprocessor("[ 0x12345678 abc/test']") == "[test']":
        """
        It's an old Gaudi, so we suffer from the bug fixed in
        https://gitlab.cern.ch/gaudi/Gaudi/merge_requests/263 and the
        fix to https://its.cern.ch/jira/browse/GAUDI-1290, so
        we must artificially hide some differences
        """
        preprocessor = LHCbExeTest.preprocessor + RegexpReplacer(
            r"\[.*/([^/]*.*)\]", r"[\1]", "^\[Gaudi"
        )
