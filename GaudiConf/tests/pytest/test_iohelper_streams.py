###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import pytest
from LHCbTesting import LHCbExeTest
from LHCbTesting.preprocessors import RegexpReplacer


@pytest.mark.ctest_fixture_setup("gaudiconf.iohelper_streams")
@pytest.mark.shared_cwd("GaudiConf")
class Test(LHCbExeTest):
    command = ["python", "../test_iohelper_streams.py"]
    reference = "../refs/test_iohelper_streams.yaml"

    import Configurables

    if repr(Configurables.Gaudi__Examples__EvtColAlg("x")).startswith(
        "Gaudi::Examples::"
    ):
        """"
        It's an old Gaudi, so we miss the fix to https://its.cern.ch/jira/browse/GAUDI-1290
        we must artificially hide some differences
        """
        preprocessor = LHCbExeTest.preprocessor + RegexpReplacer(
            "__", "::", "SomethingWeird"
        )
