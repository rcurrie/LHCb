#!/usr/bin/env python
###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Script to convert data to MDF format.

Example usage:

    mdfwriter.py -h
    mdfwriter.py -n 10 -o output.mdf /path/to/file.xdigi

"""

import argparse
import os
import tempfile

from Configurables import Gaudi__MultiFileCatalog
from DDDB.CheckDD4Hep import UseDD4Hep
from Gaudi.Configuration import WARNING

from PyConf.Algorithms import LHCb__MDFWriter as MDFWriter
from PyConf.application import (
    ApplicationOptions,
    configure,
    configure_input,
    input_from_root_file,
)
from PyConf.control_flow import CompositeNode, NodeLogic

parser = argparse.ArgumentParser(description="Convert data to MDF format")
parser.add_argument(
    "files",
    nargs="+",
    help="Input filenames or single TestFileDB entry (requires --db-entry)",
)
parser.add_argument("-o", "--output", required=True, help="Output filename")
parser.add_argument(
    "-f", "--force", action="store_true", help="Overwrite output file if it exists"
)
parser.add_argument(
    "--db-entry",
    dest="db_entry",
    action="store_true",
    help="Interpret the file as a TestFileDB entry",
)
parser.add_argument("--start", type=int, default=0, help="First event to write")
group = parser.add_mutually_exclusive_group()
group.add_argument("-n", "--nevents", type=int, help="Number of events to write")
group.add_argument("--stop", type=int, help="Last event to write (exclusive)")
args = parser.parse_args()
if os.path.exists(args.output) and not args.force:
    parser.error(
        "output already exists (use --force to overwrite existing file): " + args.output
    )
if args.stop is not None:
    args.nevents = args.stop - args.start
if args.nevents is None:
    args.nevents = -1

# Deal with our software's quirks
if not UseDD4Hep:
    from Configurables import CondDB

    CondDB().EnableRunStampCheck = False
with tempfile.NamedTemporaryFile(prefix="catalog-", suffix=".xml") as f:
    Gaudi__MultiFileCatalog("FileCatalog").Catalogs = ["xmlcatalog_file:" + f.name]

options = ApplicationOptions(_enabled=False)
options.first_evt = args.start
options.evt_max = args.nevents

if args.db_entry:
    if len(args.files) > 1:
        parser.error("provide only one input with --db-entry")
    options.set_input_and_conds_from_testfiledb(args.files[0])
else:
    options.input_files = args.files
    options.input_type = "ROOT"

config = configure_input(options)

ioalg = input_from_root_file("DAQ/RawEvent", options)

mdf_writer = MDFWriter(
    Compress=0, ChecksumType=1, GenerateMD5=True, Connection="file://" + args.output
)

cf_node = CompositeNode("Seq", children=[ioalg, mdf_writer])
config.update(configure(options, cf_node))

from Gaudi.Main import gaudimain

c = gaudimain()
c.run(False)
