/*****************************************************************************\
* (c) Copyright 2000-2024 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MDF_RAWDATACONNECTION_H
#define MDF_RAWDATACONNECTION_H

// Framework include files
#include "GaudiUtils/IIODataManager.h" // for IDataConnection class definition
#include "MDF/StreamDescriptor.h"

#include <memory>
#include <zstd.h>

enum DataConnectionState {
  UNKNOWN         = 0,
  UNCOMPRESSED    = 1,
  COMPRESSED_ZSTD = 2,
};

/*
 *  LHCb namespace declaration
 */
namespace LHCb {

  /** @class RawDataConnection RawDataConnection.h MDF/RawDataConnection.h
   *
   *  @author  M.Frank
   *  @version 1.0
   *  @date    20/10/2007
   */
  class RawDataConnection : virtual public Gaudi::IDataConnection {
  protected:
    StreamDescriptor::Access m_bind;
    StreamDescriptor::Access m_access;

    DataConnectionState m_state;

    std::vector<char>                                         m_buffIn;
    std::vector<char>                                         m_buffOut;
    std::unique_ptr<ZSTD_DStream, decltype( &ZSTD_freeDCtx )> m_dstream{ nullptr, ZSTD_freeDCtx };
    ZSTD_inBuffer                                             m_input;
    ZSTD_outBuffer                                            m_output;
    size_t                                                    m_output_pos;

  public:
    /// Standard constructor
    RawDataConnection( const IInterface* own, const std::string& nam );
    /// Assignment operator
    RawDataConnection& operator=( RawDataConnection&& ) = default;
    /// Standard destructor
    virtual ~RawDataConnection();
    /// Check if connected to data source
    bool isConnected() const override { return m_access.ioDesc != 0; }
    /// Open data stream in read mode
    StatusCode connectRead() override;
    /// Open data stream in write mode
    StatusCode connectWrite( IoType typ ) override;
    /// Release data stream
    StatusCode disconnect() override;
    /**
     * Read raw byte buffer from input stream
     * if data is nullptr, then act as a seek
     */
    StatusCode read( void* const data, size_t len ) override;
    /// Write raw byte buffer to output stream
    StatusCode write( const void* data, int len ) override;
    /// Seek on the file described by ioDesc. Arguments as in ::seek()
    long long int seek( long long int where, int origin ) override;

  private:
    /// Helper to notify the framework that a read operation failed.
    void reportReadFailure( std::string_view msg ) const;
  };
} // namespace LHCb
#endif // MDF_RAWDATACONNECTION_H
