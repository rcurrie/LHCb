/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/RawEvent.h"
#include "MDF/MDFHeader.h"

#include "Kernel/STLExtensions.h"

#include <atomic>
#include <iostream>
#include <mutex>
#include <optional>
#include <vector>

namespace LHCb::MDF {

  /**
   * This class represents a single event in the event buffer
   * Upon creation, it gets a raw buffer, a size and a bool saying whether it's compressed.
   * Banks are then decoded from the buffer and added to a RawEvent when
   * calling get on the Buffer object
   * A special DAQ bank is held within this object, storing the content of the MDFHeader
   * Note that due to the implementation of the RawBank class, ulgy C code
   * has to be used here. To be fixed. Also this enforces move only semantic
   */
  class MDFEvent {
  public:
    MDFEvent() = default;
    MDFEvent( MDFHeader* header, LHCb::span<std::byte> data, bool compressed )
        : m_data( data ), m_isCompressed( compressed ) {
      auto headerSize = MDFHeader::sizeOf( header->headerVersion() );
      // ugly C way, due to the way RawBank is implemented
      void* space = malloc( sizeof( RawBank ) + headerSize );
      m_daqBank   = reinterpret_cast<RawBank*>( space );
      m_daqBank->setMagic();
      m_daqBank->setType( RawBank::DAQ );
      m_daqBank->setSize( headerSize );
      m_daqBank->setVersion( DAQ_STATUS_BANK );
      m_daqBank->setSourceID( 0 );
      ::memcpy( m_daqBank->data(), header, headerSize ); // Need to copy header to get checksum right
    }
    ~MDFEvent() { free( (void*)m_daqBank ); };
    MDFEvent( MDFEvent const& ) = delete;
    MDFEvent( MDFEvent&& other )
        : m_daqBank( other.m_daqBank ), m_data( other.m_data ), m_isCompressed( other.m_isCompressed ) {
      other.m_daqBank = nullptr;
    }
    MDFEvent& operator=( MDFEvent const& ) = delete;
    MDFEvent& operator=( MDFEvent&& other ) {
      m_daqBank       = other.m_daqBank;
      other.m_daqBank = nullptr;
      m_data          = other.m_data;
      m_isCompressed  = other.m_isCompressed;
      return *this;
    }

    std::byte*   data() const { return m_data.data(); }
    unsigned int size() const { return m_data.size(); }
    unsigned int isCompressed() const { return m_isCompressed; }
    RawBank*     daqBank() { return m_daqBank; }

  private:
    RawBank*        m_daqBank{ nullptr };
    span<std::byte> m_data;
    bool            m_isCompressed;
  };

  /**
   * Base class for an MDFEvent buffer. Handles a vector of MDFEvents
   * and gives thread safe access to them through the get method
   */
  class Buffer {
  public:
    /// constructor
    Buffer( std::vector<MDFEvent>&& events )
        : m_events( std::move( events ) ), m_nbAvailableEvents( m_events.size() ) {}
    /// virtual destructor
    virtual ~Buffer() = default;
    /// returns number of events in buffer
    unsigned int size() { return m_events.size(); }
    /// updates number of banks to reserve space for
    static void setReservedNumberOfBanks( unsigned int nbBanksReserve ) { s_nbBanksReserve = nbBanksReserve; }
    /// return type definition for usage in IOHandler. Note that several RawEvent may be returned in case of TAE
    using EventType = std::vector<LHCb::RawEvent>;
    /// return type definition for usage in TES. Only the main event goes to TES the normal way. TAE stuff is hacky...
    using TESEventType = LHCb::RawEvent;
    /**
     * get the next event in the Buffer. This method is thread safe
     * and guarantees to return every single event exactly once
     * @returns the event or nothing when the buffer is empty
     */
    std::optional<EventType> get( EventContext const& evtCtx );

  private:
    /**
     * decompresses compressed event, storing decompressed data into
     * m_unzipData and allocating that vector if we are first.
     * This method is thread safe.
     * @return returns pointer to uncompressed event data and size of uncompressed data
     * Note that the buffer containing the uncompressed data is owned by the Buffer object
     * and deallocated when the Buffer is released
     */
    std::pair<std::byte*, int> decompressEvent( std::byte* zipData, int zipSize, unsigned int evtIndex );

    /// vector of RawEvents, pointing to the Rawbanks in rawBuffer
    std::vector<MDFEvent> m_events;
    /// number of events still available in current buffer
    std::atomic<int> m_nbAvailableEvents;
    /// number of RawBanks to reserve space for in each RawEvent
    static unsigned int s_nbBanksReserve;
    /// space owned by the buffer for unpacking sipped events
    std::vector<std::unique_ptr<std::byte[]>> m_unzipData;
    /// mutex protecting allocation of m_unzipData
    std::mutex m_unzipDataMutex;
  };

  // Helper template magic to constrain OwningBuffer to actually be an owning buffer
  namespace details {
    template <typename T>
    struct is_owner_ptr_t : std::false_type {};
    template <typename T, typename D>
    struct is_owner_ptr_t<std::unique_ptr<T, D>> : std::true_type {};
    template <typename T>
    struct is_owner_ptr_t<std::shared_ptr<T>> : std::true_type {};
    template <typename T>
    inline constexpr bool is_owner_ptr_v = is_owner_ptr_t<T>::value;
  } // namespace details

  /**
   * Templated implementation of a Buffer owning its underlying RawBuffer
   * Accepted RawBuffer types are std::unique_ptr<byte[]> or std::shared_ptr<some file wrapper>
   * The key point being that the RawBufferPtr will be detroyed when the Buffer object is
   * destroyed
   */
  template <typename RawBuffer, typename = std::enable_if_t<details::is_owner_ptr_v<RawBuffer>>>
  class OwningBuffer : public Buffer {
  public:
    OwningBuffer( RawBuffer&& rawBuffer, std::vector<MDFEvent>&& events )
        : Buffer( std::move( events ) ), m_rawBuffer( std::move( rawBuffer ) ) {}

  private:
    /// raw buffer containing rawbanks
    RawBuffer m_rawBuffer;
  };

  struct free_deleter {
    template <typename T>
    void operator()( T* p ) const {
      std::free( const_cast<std::remove_const_t<T>*>( p ) );
    }
  };

  template <typename T>
  using unique_ptr_free = std::unique_ptr<T, free_deleter>;
  using ByteBuffer      = LHCb::MDF::OwningBuffer<unique_ptr_free<std::byte>>;

} // namespace LHCb::MDF
