/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/RawEvent.h"
#include "Kernel/IOHandler.h"
#include "MDF/Buffer.h"
#include "MDF/IOAlgBase.h"

#include "fmt/format.h"

namespace LHCb::MDF {

  template <typename IOHandler, typename InputType = std::vector<std::string>>
  class IOAlgFileReadBase : public IO::IOAlgBase<IOHandler, InputType> {
  public:
    IOAlgFileReadBase( const std::string& name, ISvcLocator* pSvcLocator )
        : LHCb::IO::IOAlgBase<IOHandler, InputType>( name, pSvcLocator,
                                                     { { "RawEventLocation", "" }, { "EventBufferLocation", "" } } ){};

    /// overide operator() to count banks
    std::tuple<RawEvent, std::shared_ptr<ByteBuffer>> operator()( EventContext const& evtCtx ) const override {
      // get returns std::tuple<std::vector<RawEvent>, std::shared_ptr<ByteBuffer>>
      auto [raw_events, buf] = this->m_ioHandler->next( *this, evtCtx );
      auto nEvts             = raw_events.size();
      auto main              = nEvts / 2;
      if ( nEvts > 1 ) {
        // case of a TAE
        auto& path = this->template outputLocation<0>();
        // compute relative path, supposing the main event in stored in /Event/...
        if ( path.substr( 0, 7 ) != "/Event/" ) {
          throw GaudiException(
              fmt::format(
                  "TAE events not supported if TES path of main event does ont start with '/Event' (here '{}')", path ),
              "IOAlgFileRead::operator()", StatusCode::FAILURE );
        }
        std::string relPath = path.substr( 7 );
        // we need to store the Prev and Next events of the TAE in the TES
        for ( size_t n = 0; n < main; n++ ) {
          auto bx = main - n;
          storeEvent( std::move( raw_events[n] ), fmt::format( "/Event/Prev{}/{}", bx, relPath ) );
          storeEvent( std::move( raw_events[main + bx] ), fmt::format( "/Event/Next{}/{}", bx, relPath ) );
        }
      }
      // regular event or central event of a TAE block
      m_numBanks += raw_events[main].size();
      return { std::move( raw_events[main] ), std::move( buf ) };
    }

  private:
    void storeEvent( RawEvent&& raw_event, std::string_view path ) const {
      auto obj = std::make_unique<RawEvent>( std::forward<RawEvent>( raw_event ) );
      if ( auto sc = this->evtSvc()->registerObject( path, obj.release() ); sc.isFailure() ) {
        throw GaudiException( fmt::format( "Error puting event in {}", path ), "IOAlgFileRead::storeevent", sc );
      }
    }

    Gaudi::Property<unsigned int> m_nbBanksReserve{
        this,
        "NBanksReserve",
        1200,
        [this]( auto& ) { MDF::Buffer::setReservedNumberOfBanks( this->m_nbBanksReserve ); },
        Gaudi::Details::Property::ImmediatelyInvokeHandler{ true },
        "Number of RawBanks to reserve space for in each Event" };
    mutable Gaudi::Accumulators::StatCounter<std::size_t> m_numBanks{ this, "#banks in raw event" };
  };

} // namespace LHCb::MDF
