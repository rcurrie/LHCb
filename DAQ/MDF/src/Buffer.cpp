/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "MDF/Buffer.h"
#include "MDF/RawEventPrintout.h"

#include "GaudiKernel/GaudiException.h"

#include "RZip.h"

namespace {

  /// tries to find the TAE Bank in a RawEvent starting at start
  LHCb::RawBank const* getTAEBank( std::byte const* start ) {
    // Get the first bank in the buffer
    LHCb::RawBank const* b = reinterpret_cast<LHCb::RawBank const*>( start );
    if ( b->type() == LHCb::RawBank::TAEHeader ) { // Is it the TAE bank?
      return b;
    }
    if ( b->type() == LHCb::RawBank::DAQ ) { // Is it the TAE bank?
      // If the first bank is a MDF (DAQ) bank,
      // then the second bank must be the TAE header
      b = reinterpret_cast<LHCb::RawBank const*>( start + b->totalSize() );
    }
    if ( b->type() == LHCb::RawBank::TAEHeader ) { // Is it the TAE bank?
      return b;
    }
    return nullptr;
  }

  /// fills given raw_event with banks found between start and end
  void extractBanks( LHCb::RawEvent& raw_event, std::byte* start, std::byte* end ) {
    while ( start < end ) {
      LHCb::RawBank const* bank = reinterpret_cast<LHCb::RawBank const*>( start );
      // check Bank sanity
      if ( bank->magic() != LHCb::RawBank::MagicPattern ) {
        throw GaudiException( "Bad magic pattern in Tell1 bank : " + LHCb::RawEventPrintout::bankHeader( bank ),
                              "MDF::Buffer", StatusCode::FAILURE );
      }
      if ( bank->type() >= LHCb::RawBank::LastType ) {
        throw GaudiException( "Unknown Bank type in Tell1 bank : " + LHCb::RawEventPrintout::bankHeader( bank ),
                              "MDF::Buffer", StatusCode::FAILURE );
      }
      raw_event.adoptBank( bank, false );
      start += bank->totalSize();
    }
  }

} // namespace

// This can be overriden from options, see IOHandler
unsigned int LHCb::MDF::Buffer::s_nbBanksReserve = 1200;

std::optional<LHCb::MDF::Buffer::EventType> LHCb::MDF::Buffer::get( EventContext const& evtCtx ) {
  /// Atomically returns a unique eventID or <= 0 number if no events remain
  int evtId = m_nbAvailableEvents--;
  /// no event remains
  if ( evtId <= 0 ) return {};
  /// get the event we've picked
  auto&      event    = m_events[size() - evtId];
  std::byte* data     = event.data();
  int        dataSize = event.size();
  // decompress it in case it's compressed
  if ( event.isCompressed() ) {
    auto buffer = decompressEvent( data, dataSize, size() - evtId );
    data        = buffer.first;
    dataSize    = buffer.second;
  }
  // Decode banks of the event, taking TAE ones into account
  RawBank const* b = getTAEBank( data );
  if ( b ) {
    // The TAE bank is a vector of triplets
    int nBlocks = b->size() / sizeof( int ) / 3;
    // create all RawEvents in the output vector
    std::vector<LHCb::RawEvent> raw_events;
    raw_events.reserve( nBlocks );
    for ( int i = 0; i < nBlocks; i++ ) { raw_events.emplace_back( LHCb::getMemResource( evtCtx ) ); }
    // DAQ bank
    raw_events[nBlocks / 2].adoptBank( event.daqBank(), false );
    // go through the blocks of RawBanks and fill the events
    const int* block = b->begin<int>();
    for ( int nbl = 0; nBlocks > nbl; ++nbl ) {
      // extract where the RawBanks are located for the current event
      int bx     = *( block++ );
      int offset = *( block++ );
      int length = *( block++ );
      // offset is from the end of the TAEHeader bank
      auto start = (std::byte*)b + b->totalSize() + offset;
      // extract RawBanks into the right event
      auto& raw_event = raw_events[bx + nBlocks / 2];
      raw_event.reserve( s_nbBanksReserve );
      extractBanks( raw_event, start, start + length );
    }
    return raw_events;
  } else {
    // non TAE event, extract single RawEvent
    std::vector<LHCb::RawEvent> raw_events;
    auto&                       raw_event = raw_events.emplace_back( LHCb::getMemResource( evtCtx ) );
    raw_event.reserve( s_nbBanksReserve );
    // DAQ bank
    raw_event.adoptBank( event.daqBank(), false );
    // all other banks
    extractBanks( raw_event, data, data + dataSize );
    return raw_events;
  }
}

std::pair<std::byte*, int> LHCb::MDF::Buffer::decompressEvent( std::byte* zipData, int zipSize,
                                                               unsigned int evtIndex ) {
  // check whether we need to allocate the vector of uncompressed data
  if ( m_unzipData.size() == 0 ) {
    // yes, allocate for all events via reserve, and make it in a thread safe way
    std::scoped_lock lock( m_unzipDataMutex );
    if ( m_unzipData.size() == 0 ) { m_unzipData.resize( m_events.size() ); }
  }
  // find out size of the uncompressed data
  int zipSizeInternal, dataSize;
  ::R__unzip_header( &zipSizeInternal, reinterpret_cast<unsigned char*>( zipData ), &dataSize );
  assert( zipSizeInternal == zipSize );
  // allocate space for unzipping event evtIndex
  m_unzipData[evtIndex] = std::make_unique<std::byte[]>( dataSize );
  std::byte* data       = m_unzipData[evtIndex].get();
  // decompress
  int actualDataSize;
  ::R__unzip( &zipSize, reinterpret_cast<unsigned char*>( zipData ), &dataSize,
              reinterpret_cast<unsigned char*>( data ), &actualDataSize );
  assert( actualDataSize == dataSize );
  return { data, dataSize };
}
